﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperModels
{
    public interface IDbObject
    {
        int Id { get; set; }
    }
}
