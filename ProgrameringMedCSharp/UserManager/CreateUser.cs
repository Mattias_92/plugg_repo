﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UserManager
{
    public class CreateUser
    {
        public static bool IsEmailValid(string userEmail)
        {
            int lastIndexOfDot = userEmail.LastIndexOf(".");
            if (userEmail.Contains("@"))
            {
                if (lastIndexOfDot == userEmail.Length - 4 ||
                    lastIndexOfDot == userEmail.Length - 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static string GetAPassword(string text)
        {
            var hashedPass = HashUserPassword(text);
            return ConvertPassHashToString(hashedPass);
        }
        internal static byte[] HashUserPassword(string text)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(text));
        }
        internal static string ConvertPassHashToString(byte[] vs)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < vs.Length; i++)
            {
                builder.Append(vs[i].ToString("x2"));
            }
            return builder.ToString();
        }

        public static UserAge GetUserAge(User user)
        {
            DateTime today = DateTime.Today;

            int userYears = today.Year - user.BirtDate.Year;
            int userMonths = today.Month - user.BirtDate.Month;

            if (today.Day < user.BirtDate.Day)
            {
                userMonths--;
            }
            if (userMonths < 0)
            {
                userYears--;
                userMonths += 12;
            }

            int userDays = (today - user.BirtDate.AddMonths(userYears * 12 + userMonths)).Days;
            UserAge userAge = new UserAge
            {
                UserYears = userYears,
                UserMonts = userMonths,
                UserDays = userDays
            };
            return userAge;
        }

        public static UserAge TimeTillNextBirtDay(User user)
        {
            DateTime today = DateTime.Today;
            DateTime userDate = user.BirtDate.Date;
            int userYears = today.Year - user.BirtDate.Year;
            int userMonths =  user.BirtDate.Month - today.Month;
            int days = today.Day - user.BirtDate.Day;
            if (today.Day > user.BirtDate.Day)
            {
                userMonths--;
            }
            if (userMonths < 0)
            {
                userYears--;
                userMonths += 12;
            }

            int userDays = (today - user.BirtDate.AddMonths(userYears * 12 + userMonths)).Days;


            UserAge userAge = new UserAge()
            {
                UserYears = userYears,
                UserMonts = userMonths,
                UserDays = userDays
            };
            return userAge;
        }
    }
}
