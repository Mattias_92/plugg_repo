﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager
{
    public static class ScoreFactory
    {
        public static UserScore CreateScore(int uId, string clicks, string elapsedTime)
        {
            UserScore score = new UserScore();

            score.UserId = uId;
            score.Clicks = int.Parse(clicks);
            score.ElapsedTime = TimeSpan.Parse(elapsedTime);
            score.Game = Games.Maze;
            return score;
        }
    }
}
