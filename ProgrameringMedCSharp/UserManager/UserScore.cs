﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager
{
    public class UserScore : HelperModels.IDbObject
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        //public User User { get; set; }
        public int Clicks { get; set; }
        public TimeSpan ElapsedTime { get; set; }
        public Games Game { get; set; }
        
    }
}
