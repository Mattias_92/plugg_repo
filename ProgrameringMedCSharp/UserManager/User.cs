﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager
{
    public class User : HelperModels.IDbObject
    {
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public string HashedPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public DateTime BirtDate { get; set; }
        public List<UserScore> Scores { get; set; }
        
    }
}
