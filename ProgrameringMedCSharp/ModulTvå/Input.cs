﻿using System;

namespace ModulTvå
{
    public static class Input
    {
        public static double GetDoubleFromUser()
        {
            double userNumber;
            while (!double.TryParse(Console.ReadLine(), out userNumber))
            {
                Console.Write("Enter a valid double number: ");
            }
            return userNumber;
        }
        public static DateTime GetDateFromUser()
        {
            DateTime userBirthDate;
            while (!DateTime.TryParse(Console.ReadLine(), out userBirthDate))
            {
                Console.Write("enter your birth date use yyyy/mm/dd format: ");
            }
            return userBirthDate;
        }

        internal static int GetIntFromUser()
        {
            int userNumber;
            while (!int.TryParse(Console.ReadLine(), out userNumber))
            {
                Console.Write("Enter a valid integer number: ");
            }
            return userNumber;
        }


    }
}
