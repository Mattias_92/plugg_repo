﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ModulTvå
{
    partial class Program
    {
        static void Main(string[] args)
        {
            DateTime today = DateTime.Today;
            DateTime[] dates = new DateTime[2];

            for (int i = 0; i < dates.Length; i++)
            {
                Console.Write("Skriv in ett datum använd yyyy/mm/dd formel: ");
                DateTime userInput = Input.GetDateFromUser();

                if (userInput < today)
                {
                    Console.WriteLine("Ogiltigt datum! Det får inte vara tidigare än dagens datum.");
                    i--;
                    continue;
                }

                if (i >= 1)
                {
                    if (dates[0] == userInput)
                    {
                        Console.WriteLine("Datumerna får inte vara samma datum.");
                        i--;
                        continue;
                    }
                    else if(userInput < dates[0])
                    {
                        Console.WriteLine("Slutdatumet får inte vara innan start datumet.");
                        i--;
                        continue;
                    }
                    else if (userInput > dates[0].AddDays(30))
                    {
                        Console.WriteLine("Du får inte vara mer än 30 dagar senare än startdatumet.");
                        i--;
                        continue;
                    }
                }
                dates[i] = userInput;
            }
            
            Console.WriteLine("Det är två korekta datum.");
            Console.ReadLine();
        }
    }
}
