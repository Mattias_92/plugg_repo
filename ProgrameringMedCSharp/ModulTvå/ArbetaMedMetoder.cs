﻿using System;
using System.Linq;

namespace ModulTvå
{
    public static class ArbetaMedMetoder
    {
        // 1.
        public static string PrintMessage()
        {
            string message = "Hello world";
            return message;
        }
        // 2.
        public static double ConvertInputToPercentage()
        {
            double userNumber;
            do
            {
                Console.Write("Enter a number between 1 and 0");
                userNumber = Input.GetDoubleFromUser();
            } while (userNumber > 1 || userNumber < 0);
            return ToPerCentage(userNumber);
        }
        public static double ToPerCentage(double userNumber)
        {
            double percentage = userNumber * 100;
            return percentage;
        }

        // 3.
        public static string MergeTwoStrings(string stringOne, string stringTwo) => stringOne + stringTwo;

        // 4.
        public static decimal CalculateVAT(int sum)
        {
            decimal vatRate = 0.25m;
            decimal vat = sum * vatRate;
            return vat;
        }

        // 5.
        public static void CheckUserAge()
        {
            DateTime today = DateTime.Today;
            DateTime userBirthDate = GetUserBirthDate();

            int userAge = today.Year - userBirthDate.Year;

            bool isOverEighteen = DoesUserPassAgeLimit(userAge, 18);
            if (isOverEighteen)
            {
                Console.WriteLine("The user is atleast 18 years old.");
            }
            else
            {
                Console.WriteLine("The user is not 18 years old.");
            }
        }

        private static bool DoesUserPassAgeLimit(int userAge, int ageLimit) => userAge >= ageLimit;

        public static DateTime GetUserBirthDate()
        {
            Console.Write("Enter your birth date use yyyy/mm/dd format: ");
            DateTime userBirthDate = Input.GetDateFromUser();
            return userBirthDate;
        }

        // 6.
        public static void GetTheLargetsValueFromThreeInts()
        {
            int[] userNumbers = new int[3];
            for (int i = 0; i < userNumbers.Length; i++)
            {
                Console.Write($"Enter value {i + 1}: ");
                userNumbers[i] = Input.GetIntFromUser();
            }
            int largestValue = ControllMaxValue(userNumbers[0], userNumbers[1], userNumbers[2]);
            Console.WriteLine($"The largest value is {largestValue}");
        }

        private static int MaxValue2(int numOne, int numTwo)
        {
            if (numOne > numTwo)
            {
                return numOne;
            }
            else
            {
                return numTwo;
            }
        }
        private static int MaxValue3(int numOne, int numTwo, int numThree)
        {
            if (numOne > numTwo && numOne > numThree)
            {
                return numOne;
            }
            else if (numTwo > numOne && numTwo > numThree)
            {
                return numTwo;
            }
            else
            {
                return numThree;
            }
        }

        private static int ControllMaxValue(int numOne, int numTwo, int numThree)
        {
            if (numOne == 0)
            {
                return MaxValue2(numTwo, numThree);
            }
            else if (numTwo == 0)
            {
                return MaxValue2(numOne, numThree);
            }
            else if (numThree == 0)
            {
                return MaxValue2(numOne, numTwo);
            }
            else
            {
                return MaxValue3(numOne, numTwo, numThree);
            }
        }
        // 7
        public static string FindLongestString(string[] vs)
        {
            string longestString = vs[0];
            for (int i = 1; i < vs.Length; i++)
            {
                if (vs[i].Length > longestString.Length)
                {
                    longestString = vs[i];
                }
            }
            return longestString;
        }
        // 8.
        public static int SumIntArray(int[] vs)
        {
            int sum = 0;
            foreach (var item in vs)
            {
                sum += item;
            }
            return sum;
        }

        public static int MultiplyIntArray(int[] vs)
        {
            int produkt = 1;
            foreach (var item in vs)
            {
                produkt *= item;
            }
            return produkt;
        }

        // 9. 
        public static bool IsSwedishVowel(char c, out string message)
        {
            char[] hardVowels = { 'a', 'o', 'u', 'å' };
            char[] softVowels = { 'e', 'i', 'y', 'ä', 'ö' };
            message = string.Empty;
            if (hardVowels.Contains(c))
            {
                message = $"{c} is a vowel it is considered a hard vowel.";
                return true;
            }
            else if (softVowels.Contains(c))
            {
                message = $"{c} is a vowel it is considered a soft vowel.";
                return true;
            }
            else
            {
                message = $"{c} is not a vowel.";
                return false;
            }
        }

        // 10. 
        public static void Translate(ref string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (!IsSwedishVowel(input[i], out string message) && input[i] != ' ')
                {
                    var currentLetter = input[i].ToString();
                    var stringToAppend = currentLetter + "o";
                    input = input.Insert(i, stringToAppend);
                    i += 2;
                }
            }
        }

        // 11.
        public static void ReferenceOut(ref int numOne, out int numTwo)
        {
            numOne *= 3;
            numTwo = numOne + 2;
        }
    }
}
