﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MemmoryGame
{
    public partial class MemmoryBoard : Form
    {
        MockDB.MockUserDb userDb;
        MockDB.MockScoresDb scoresDb;
        UserManager.User _user;
        GameLogic _logic;
        int seconds = 0;
        public MemmoryBoard()
        {           
            InitializeComponent();

            userDb = new MockDB.MockUserDb();
            scoresDb = new MockDB.MockScoresDb();

            userDb.PopulateUserDbWithPreMades();
            scoresDb.PopulateDbWithPreMadeScores();

            _user = (UserManager.User)userDb.GetEntity(1);

            _logic = new GameLogic(timer_ResetTimer, tbl_Board.Controls, txt_Clicks);
            _logic.PopulateTable();
        }

        private void MemmoryBoard_Resize(object sender, EventArgs e)
        {
            tbl_Board.Height = this.Height - 100;
            tbl_Board.Width = this.Width - 40;
        }

        private void lbl_Icon_Click(object sender, EventArgs e)
        {
            
            _logic.DisplayLable(sender);
            if (_logic.IsGameOver())
            {
                timer_GameTimer.Stop();
                var elapsedTime = txt_ElapsedTime.Text;
                TimeSpan timeSpan = TimeSpan.Parse(elapsedTime);
                MessageBox.Show("`You won!");
                MessageBox.Show($"It took {elapsedTime} to win!");
                var score = CreateScore();
                scoresDb.AddToDb(score,out string er, out int scoreId);
                //score.Id = scoreId;
                _user.Scores.Add(score);
                userDb.SaveDb();
                scoresDb.SaveDb();
                this.Close();
            }
        }

        private void timer_ResetTimer_Tick(object sender, EventArgs e)
        {           
            _logic.ResetLabels();
        }

        private void MemmoryBoard_Load(object sender, EventArgs e)
        {
            timer_GameTimer.Tick += new EventHandler(timer_GameTimer_Tick);
            timer_GameTimer.Interval = 1000;
            timer_GameTimer.Enabled = true;
        }

        private void timer_GameTimer_Tick(object sender, EventArgs e)
        {
            seconds++;
            txt_ElapsedTime.Text = TimeSpan.FromSeconds(seconds).ToString("mm\\:ss");
        }
        private UserManager.UserScore CreateScore()
        {
            UserManager.UserScore score = new UserManager.UserScore();
            
            score.UserId = _user.Id;
            //score.User = _user;
            score.Clicks = int.Parse(txt_Clicks.Text);
            score.ElapsedTime = TimeSpan.Parse(txt_ElapsedTime.Text);
            score.Game = UserManager.Games.Memmory;
            return score;
        }
    }
}
