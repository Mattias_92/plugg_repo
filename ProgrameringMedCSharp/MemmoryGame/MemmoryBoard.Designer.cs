﻿
namespace MemmoryGame
{
    partial class MemmoryBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbl_Board = new System.Windows.Forms.TableLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Icon = new System.Windows.Forms.Label();
            this.timer_ResetTimer = new System.Windows.Forms.Timer(this.components);
            this.txt_Clicks = new System.Windows.Forms.TextBox();
            this.lbl_Clicks = new System.Windows.Forms.Label();
            this.timer_GameTimer = new System.Windows.Forms.Timer(this.components);
            this.lbl_ElapsedTime = new System.Windows.Forms.Label();
            this.txt_ElapsedTime = new System.Windows.Forms.TextBox();
            this.tbl_Board.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbl_Board
            // 
            this.tbl_Board.BackColor = System.Drawing.Color.DodgerBlue;
            this.tbl_Board.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tbl_Board.ColumnCount = 4;
            this.tbl_Board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.Controls.Add(this.label15, 3, 3);
            this.tbl_Board.Controls.Add(this.label14, 2, 3);
            this.tbl_Board.Controls.Add(this.label13, 1, 3);
            this.tbl_Board.Controls.Add(this.label12, 0, 3);
            this.tbl_Board.Controls.Add(this.label11, 3, 2);
            this.tbl_Board.Controls.Add(this.label10, 2, 2);
            this.tbl_Board.Controls.Add(this.label9, 1, 2);
            this.tbl_Board.Controls.Add(this.label8, 0, 2);
            this.tbl_Board.Controls.Add(this.label7, 3, 1);
            this.tbl_Board.Controls.Add(this.label6, 2, 1);
            this.tbl_Board.Controls.Add(this.label5, 1, 1);
            this.tbl_Board.Controls.Add(this.label4, 0, 1);
            this.tbl_Board.Controls.Add(this.label3, 3, 0);
            this.tbl_Board.Controls.Add(this.label2, 2, 0);
            this.tbl_Board.Controls.Add(this.label1, 1, 0);
            this.tbl_Board.Controls.Add(this.lbl_Icon, 0, 0);
            this.tbl_Board.Location = new System.Drawing.Point(12, 49);
            this.tbl_Board.Name = "tbl_Board";
            this.tbl_Board.RowCount = 4;
            this.tbl_Board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tbl_Board.Size = new System.Drawing.Size(781, 491);
            this.tbl_Board.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label15.Location = new System.Drawing.Point(587, 368);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(189, 121);
            this.label15.TabIndex = 15;
            this.label15.Text = "c";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label14.Location = new System.Drawing.Point(393, 368);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(186, 121);
            this.label14.TabIndex = 14;
            this.label14.Text = "c";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label14.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label13.Location = new System.Drawing.Point(199, 368);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(186, 121);
            this.label13.TabIndex = 13;
            this.label13.Text = "c";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label12.Location = new System.Drawing.Point(5, 368);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(186, 121);
            this.label12.TabIndex = 12;
            this.label12.Text = "c";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label11.Location = new System.Drawing.Point(587, 246);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(189, 120);
            this.label11.TabIndex = 11;
            this.label11.Text = "c";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label10.Location = new System.Drawing.Point(393, 246);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 120);
            this.label10.TabIndex = 10;
            this.label10.Text = "c";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label9.Location = new System.Drawing.Point(199, 246);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(186, 120);
            this.label9.TabIndex = 9;
            this.label9.Text = "c";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label8.Location = new System.Drawing.Point(5, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 120);
            this.label8.TabIndex = 8;
            this.label8.Text = "c";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label7.Location = new System.Drawing.Point(587, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(189, 120);
            this.label7.TabIndex = 7;
            this.label7.Text = "c";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label6.Location = new System.Drawing.Point(393, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 120);
            this.label6.TabIndex = 6;
            this.label6.Text = "c";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label5.Location = new System.Drawing.Point(199, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 120);
            this.label5.TabIndex = 5;
            this.label5.Text = "c";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label4.Location = new System.Drawing.Point(5, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 120);
            this.label4.TabIndex = 4;
            this.label4.Text = "c";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label3.Location = new System.Drawing.Point(587, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 120);
            this.label3.TabIndex = 3;
            this.label3.Text = "c";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label2.Location = new System.Drawing.Point(393, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 120);
            this.label2.TabIndex = 2;
            this.label2.Text = "c";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.label1.Location = new System.Drawing.Point(199, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 120);
            this.label1.TabIndex = 1;
            this.label1.Text = "c";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // lbl_Icon
            // 
            this.lbl_Icon.AutoSize = true;
            this.lbl_Icon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Icon.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.lbl_Icon.Location = new System.Drawing.Point(5, 2);
            this.lbl_Icon.Name = "lbl_Icon";
            this.lbl_Icon.Size = new System.Drawing.Size(186, 120);
            this.lbl_Icon.TabIndex = 0;
            this.lbl_Icon.Text = "c";
            this.lbl_Icon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Icon.Click += new System.EventHandler(this.lbl_Icon_Click);
            // 
            // timer_ResetTimer
            // 
            this.timer_ResetTimer.Interval = 500;
            this.timer_ResetTimer.Tick += new System.EventHandler(this.timer_ResetTimer_Tick);
            // 
            // txt_Clicks
            // 
            this.txt_Clicks.Enabled = false;
            this.txt_Clicks.Location = new System.Drawing.Point(103, 12);
            this.txt_Clicks.Name = "txt_Clicks";
            this.txt_Clicks.ReadOnly = true;
            this.txt_Clicks.Size = new System.Drawing.Size(100, 20);
            this.txt_Clicks.TabIndex = 1;
            // 
            // lbl_Clicks
            // 
            this.lbl_Clicks.AutoSize = true;
            this.lbl_Clicks.Location = new System.Drawing.Point(12, 15);
            this.lbl_Clicks.Name = "lbl_Clicks";
            this.lbl_Clicks.Size = new System.Drawing.Size(89, 13);
            this.lbl_Clicks.TabIndex = 2;
            this.lbl_Clicks.Text = "Number of clicks:";
            // 
            // timer_GameTimer
            // 
            this.timer_GameTimer.Interval = 1000;
            this.timer_GameTimer.Tick += new System.EventHandler(this.timer_GameTimer_Tick);
            // 
            // lbl_ElapsedTime
            // 
            this.lbl_ElapsedTime.AutoSize = true;
            this.lbl_ElapsedTime.Location = new System.Drawing.Point(210, 18);
            this.lbl_ElapsedTime.Name = "lbl_ElapsedTime";
            this.lbl_ElapsedTime.Size = new System.Drawing.Size(70, 13);
            this.lbl_ElapsedTime.TabIndex = 3;
            this.lbl_ElapsedTime.Text = "Elapsed time:";
            // 
            // txt_ElapsedTime
            // 
            this.txt_ElapsedTime.Enabled = false;
            this.txt_ElapsedTime.Location = new System.Drawing.Point(286, 12);
            this.txt_ElapsedTime.Name = "txt_ElapsedTime";
            this.txt_ElapsedTime.ReadOnly = true;
            this.txt_ElapsedTime.Size = new System.Drawing.Size(100, 20);
            this.txt_ElapsedTime.TabIndex = 4;
            // 
            // MemmoryBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 552);
            this.Controls.Add(this.txt_ElapsedTime);
            this.Controls.Add(this.lbl_ElapsedTime);
            this.Controls.Add(this.lbl_Clicks);
            this.Controls.Add(this.txt_Clicks);
            this.Controls.Add(this.tbl_Board);
            this.Name = "MemmoryBoard";
            this.Text = "MemmoryBoard";
            this.Load += new System.EventHandler(this.MemmoryBoard_Load);
            this.Resize += new System.EventHandler(this.MemmoryBoard_Resize);
            this.tbl_Board.ResumeLayout(false);
            this.tbl_Board.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tbl_Board;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Icon;
        private System.Windows.Forms.Timer timer_ResetTimer;
        private System.Windows.Forms.TextBox txt_Clicks;
        private System.Windows.Forms.Label lbl_Clicks;
        private System.Windows.Forms.Timer timer_GameTimer;
        private System.Windows.Forms.Label lbl_ElapsedTime;
        private System.Windows.Forms.TextBox txt_ElapsedTime;
    }
}