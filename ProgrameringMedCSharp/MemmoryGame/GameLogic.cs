﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemmoryGame
{
    class GameLogic
    {
        private Random random = new Random();
        private List<string> chars = new List<string>
        {
            "$","%","#","@","!","*","a","b","c","d","e",
            "f","g","h","i","j","k","l","m","n","o","p",
            "q","r","s","t","u","v","w","x","y","z","1",
            "2","3","4","5","6","7","8","9","0","?",";",
            ":","A","B","C","D","E","F","G","H","I","J",
            "K","L","M","N","O","P","Q","R","S","T","U",
            "V","W","X","Y","Z","^"
        };
        private List<string> _icons = new List<string>();
        private Label firstClicked;
        private Label secondClicked;
        private Timer _reset_Timer;
        private TableLayoutControlCollection _controls;
        private TextBox _clickTextbox;
        private int numberOfClicks = 0;
        public GameLogic(Timer timer, TableLayoutControlCollection controls, TextBox textBox)
        {
            PopulateIcons();
            firstClicked = null;
            secondClicked = null;
            _reset_Timer = timer;
            _controls = controls;
            _clickTextbox = textBox;

        }

        public bool IsGameOver()
        {
            foreach (Control item in _controls)
            {
                Label label = item as Label;
                if (label != null)
                {
                    if (label.ForeColor == label.BackColor)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        internal void ResetLabels()
        {
            _reset_Timer.Stop();
            HideClickedLabels();
            SetClickedLabelsToNull();
        }

        private void HideClickedLabels()
        {
            firstClicked.ForeColor = firstClicked.BackColor;
            secondClicked.ForeColor = secondClicked.BackColor;
        }

        private void SetClickedLabelsToNull()
        {
            firstClicked = null;
            secondClicked = null;
        }

        internal void DisplayLable(object sender)
        {
            
            if (_reset_Timer.Enabled == true)
            {
                return;
            }
            Label label = sender as Label;
            if (label!= null)
            {                
                if (label.ForeColor != label.BackColor)
                {
                    return;
                }
                if (firstClicked == null)
                {
                    numberOfClicks++;
                    _clickTextbox.Text = numberOfClicks.ToString();
                    firstClicked = label;
                    label.ForeColor = Color.Black;
                    return;
                }
                else if (secondClicked == null)
                {
                    secondClicked = label;
                    secondClicked.ForeColor = Color.Black;
                    numberOfClicks++;
                    _clickTextbox.Text = numberOfClicks.ToString();
                    if (firstClicked.Text == secondClicked.Text)
                    {
                        SetClickedLabelsToNull();
                        return;
                    }
                   
                    _reset_Timer.Start();
                }
                
            }
        }

        public void PopulateTable()
        {
            foreach (Control control in _controls)
            {
                Label label = control as Label;
                if (label != null)
                {
                    int index = random.Next(_icons.Count);
                    label.Text = _icons[index];
                    label.ForeColor = label.BackColor;
                    _icons.RemoveAt(index);
                }
            }
        }

        private string GetRandomChar()
        {
            int rnd = random.Next(0, chars.Count);
            string c = chars[rnd];
            chars.RemoveAt(rnd);
            return c;
        }
        private void PopulateIcons()
        {
            string[] icons = new string[16];
            for (int i = 0; i < icons.Length; i+=2)
            {
                var icon = GetRandomChar();
                icons[i] = icon;
                icons[i+1] = icon;
            }
            _icons.AddRange(icons);
        }
    }
}
