﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeGame
{
    public partial class Maze : Form
    {
        MockDB.MockUserDb userDb;
        MockDB.MockScoresDb scoresDb;
        UserManager.User _user;
        GameLogic _logic;
        int seconds = 0;
        int tries = 0;
        public Maze()
        {
            InitializeComponent();
            userDb = new MockDB.MockUserDb();
            scoresDb = new MockDB.MockScoresDb();
            userDb.PopulateUserDbWithPreMades();
            scoresDb.PopulateDbWithPreMadeScores();

            _user = (UserManager.User)userDb.GetEntity(1);
            
            _logic = new GameLogic();
            _logic.MoveToStart(pnl_GameBoard.Location, this);

        }

        private void lbl_Finish_MouseEnter(object sender, EventArgs e)
        {
            MessageBox.Show("You made it!");
            var score = UserManager.ScoreFactory.CreateScore(_user.Id, txt_TriesCounter.Text, txt_ElapsedTime.Text);
            scoresDb.AddToDb(score, out string er, out int scoreId);
            _user.Scores.Add(score);
            userDb.SaveDb();
            scoresDb.SaveDb();
            this.Close();
        }

        private void wall_MouseEnter(object sender, EventArgs e)
        {
            
            _logic.MoveToStart(pnl_GameBoard.Location, this);
            Cursor.Hide();
            timer_MessageTimer.Start();
            
            lbl_TryAgainMessage.Visible = true;
            tries++;
            txt_TriesCounter.Text = tries.ToString();
            
        }

        private void Maze_Load(object sender, EventArgs e)
        {
            timer_ElapsedTime.Tick += new EventHandler(timer_ElapsedTime_Tick);
            timer_ElapsedTime.Interval = 1000;
            timer_ElapsedTime.Enabled = true;
        }

        private void timer_ElapsedTime_Tick(object sender, EventArgs e)
        {
            seconds++;
            txt_ElapsedTime.Text = TimeSpan.FromSeconds(seconds).ToString("mm\\:ss");
        }

        private void timer_MessageTimer_Tick(object sender, EventArgs e)
        {
            _logic.DisplayFailMessage(timer_MessageTimer, lbl_TryAgainMessage);
        }
        
    }
}
