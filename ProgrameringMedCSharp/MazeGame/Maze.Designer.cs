﻿
namespace MazeGame
{
    partial class Maze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnl_GameBoard = new System.Windows.Forms.Panel();
            this.lbl_Finish = new System.Windows.Forms.Label();
            this.lbl_Start = new System.Windows.Forms.Label();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.timer_ElapsedTime = new System.Windows.Forms.Timer(this.components);
            this.txt_ElapsedTime = new System.Windows.Forms.TextBox();
            this.lbl_ElapsedTime = new System.Windows.Forms.Label();
            this.lbl_Tries = new System.Windows.Forms.Label();
            this.txt_TriesCounter = new System.Windows.Forms.TextBox();
            this.timer_MessageTimer = new System.Windows.Forms.Timer(this.components);
            this.lbl_TryAgainMessage = new System.Windows.Forms.Label();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.pnl_GameBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_GameBoard
            // 
            this.pnl_GameBoard.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl_GameBoard.Controls.Add(this.lbl_TryAgainMessage);
            this.pnl_GameBoard.Controls.Add(this.lbl_Finish);
            this.pnl_GameBoard.Controls.Add(this.lbl_Start);
            this.pnl_GameBoard.Controls.Add(this.textBox106);
            this.pnl_GameBoard.Controls.Add(this.textBox105);
            this.pnl_GameBoard.Controls.Add(this.textBox55);
            this.pnl_GameBoard.Controls.Add(this.textBox104);
            this.pnl_GameBoard.Controls.Add(this.textBox103);
            this.pnl_GameBoard.Controls.Add(this.textBox102);
            this.pnl_GameBoard.Controls.Add(this.textBox101);
            this.pnl_GameBoard.Controls.Add(this.textBox100);
            this.pnl_GameBoard.Controls.Add(this.textBox99);
            this.pnl_GameBoard.Controls.Add(this.textBox98);
            this.pnl_GameBoard.Controls.Add(this.textBox97);
            this.pnl_GameBoard.Controls.Add(this.textBox96);
            this.pnl_GameBoard.Controls.Add(this.textBox95);
            this.pnl_GameBoard.Controls.Add(this.textBox94);
            this.pnl_GameBoard.Controls.Add(this.textBox93);
            this.pnl_GameBoard.Controls.Add(this.textBox92);
            this.pnl_GameBoard.Controls.Add(this.textBox91);
            this.pnl_GameBoard.Controls.Add(this.textBox90);
            this.pnl_GameBoard.Controls.Add(this.textBox89);
            this.pnl_GameBoard.Controls.Add(this.textBox88);
            this.pnl_GameBoard.Controls.Add(this.textBox87);
            this.pnl_GameBoard.Controls.Add(this.textBox86);
            this.pnl_GameBoard.Controls.Add(this.textBox85);
            this.pnl_GameBoard.Controls.Add(this.textBox84);
            this.pnl_GameBoard.Controls.Add(this.textBox83);
            this.pnl_GameBoard.Controls.Add(this.textBox82);
            this.pnl_GameBoard.Controls.Add(this.textBox81);
            this.pnl_GameBoard.Controls.Add(this.textBox80);
            this.pnl_GameBoard.Controls.Add(this.textBox79);
            this.pnl_GameBoard.Controls.Add(this.textBox78);
            this.pnl_GameBoard.Controls.Add(this.textBox77);
            this.pnl_GameBoard.Controls.Add(this.textBox76);
            this.pnl_GameBoard.Controls.Add(this.textBox75);
            this.pnl_GameBoard.Controls.Add(this.textBox74);
            this.pnl_GameBoard.Controls.Add(this.textBox73);
            this.pnl_GameBoard.Controls.Add(this.textBox72);
            this.pnl_GameBoard.Controls.Add(this.textBox71);
            this.pnl_GameBoard.Controls.Add(this.textBox70);
            this.pnl_GameBoard.Controls.Add(this.textBox69);
            this.pnl_GameBoard.Controls.Add(this.textBox68);
            this.pnl_GameBoard.Controls.Add(this.textBox67);
            this.pnl_GameBoard.Controls.Add(this.textBox66);
            this.pnl_GameBoard.Controls.Add(this.textBox65);
            this.pnl_GameBoard.Controls.Add(this.textBox64);
            this.pnl_GameBoard.Controls.Add(this.textBox63);
            this.pnl_GameBoard.Controls.Add(this.textBox62);
            this.pnl_GameBoard.Controls.Add(this.textBox61);
            this.pnl_GameBoard.Controls.Add(this.textBox60);
            this.pnl_GameBoard.Controls.Add(this.textBox59);
            this.pnl_GameBoard.Controls.Add(this.textBox58);
            this.pnl_GameBoard.Controls.Add(this.textBox57);
            this.pnl_GameBoard.Controls.Add(this.textBox56);
            this.pnl_GameBoard.Controls.Add(this.textBox54);
            this.pnl_GameBoard.Controls.Add(this.textBox53);
            this.pnl_GameBoard.Controls.Add(this.textBox52);
            this.pnl_GameBoard.Controls.Add(this.textBox51);
            this.pnl_GameBoard.Controls.Add(this.textBox50);
            this.pnl_GameBoard.Controls.Add(this.textBox49);
            this.pnl_GameBoard.Controls.Add(this.textBox48);
            this.pnl_GameBoard.Controls.Add(this.textBox47);
            this.pnl_GameBoard.Controls.Add(this.textBox46);
            this.pnl_GameBoard.Controls.Add(this.textBox45);
            this.pnl_GameBoard.Controls.Add(this.textBox44);
            this.pnl_GameBoard.Controls.Add(this.textBox43);
            this.pnl_GameBoard.Controls.Add(this.textBox42);
            this.pnl_GameBoard.Controls.Add(this.textBox41);
            this.pnl_GameBoard.Controls.Add(this.textBox40);
            this.pnl_GameBoard.Controls.Add(this.textBox39);
            this.pnl_GameBoard.Controls.Add(this.textBox38);
            this.pnl_GameBoard.Controls.Add(this.textBox37);
            this.pnl_GameBoard.Controls.Add(this.textBox36);
            this.pnl_GameBoard.Controls.Add(this.textBox35);
            this.pnl_GameBoard.Controls.Add(this.textBox34);
            this.pnl_GameBoard.Controls.Add(this.textBox33);
            this.pnl_GameBoard.Controls.Add(this.textBox32);
            this.pnl_GameBoard.Controls.Add(this.textBox31);
            this.pnl_GameBoard.Controls.Add(this.textBox30);
            this.pnl_GameBoard.Controls.Add(this.textBox29);
            this.pnl_GameBoard.Controls.Add(this.textBox28);
            this.pnl_GameBoard.Controls.Add(this.textBox27);
            this.pnl_GameBoard.Controls.Add(this.textBox26);
            this.pnl_GameBoard.Controls.Add(this.textBox25);
            this.pnl_GameBoard.Controls.Add(this.textBox24);
            this.pnl_GameBoard.Controls.Add(this.textBox23);
            this.pnl_GameBoard.Controls.Add(this.textBox22);
            this.pnl_GameBoard.Controls.Add(this.textBox21);
            this.pnl_GameBoard.Controls.Add(this.textBox20);
            this.pnl_GameBoard.Controls.Add(this.textBox19);
            this.pnl_GameBoard.Controls.Add(this.textBox18);
            this.pnl_GameBoard.Controls.Add(this.textBox17);
            this.pnl_GameBoard.Controls.Add(this.textBox16);
            this.pnl_GameBoard.Controls.Add(this.textBox15);
            this.pnl_GameBoard.Controls.Add(this.textBox14);
            this.pnl_GameBoard.Controls.Add(this.textBox13);
            this.pnl_GameBoard.Controls.Add(this.textBox12);
            this.pnl_GameBoard.Controls.Add(this.textBox11);
            this.pnl_GameBoard.Controls.Add(this.textBox10);
            this.pnl_GameBoard.Controls.Add(this.textBox9);
            this.pnl_GameBoard.Controls.Add(this.textBox8);
            this.pnl_GameBoard.Controls.Add(this.textBox7);
            this.pnl_GameBoard.Controls.Add(this.textBox6);
            this.pnl_GameBoard.Controls.Add(this.textBox5);
            this.pnl_GameBoard.Controls.Add(this.textBox4);
            this.pnl_GameBoard.Controls.Add(this.textBox3);
            this.pnl_GameBoard.Controls.Add(this.textBox2);
            this.pnl_GameBoard.Controls.Add(this.textBox1);
            this.pnl_GameBoard.Controls.Add(this.textBox107);
            this.pnl_GameBoard.Location = new System.Drawing.Point(23, 24);
            this.pnl_GameBoard.Name = "pnl_GameBoard";
            this.pnl_GameBoard.Size = new System.Drawing.Size(734, 562);
            this.pnl_GameBoard.TabIndex = 0;
            // 
            // lbl_Finish
            // 
            this.lbl_Finish.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Finish.Location = new System.Drawing.Point(673, 516);
            this.lbl_Finish.Name = "lbl_Finish";
            this.lbl_Finish.Size = new System.Drawing.Size(70, 23);
            this.lbl_Finish.TabIndex = 108;
            this.lbl_Finish.Text = "Finish";
            this.lbl_Finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Finish.MouseEnter += new System.EventHandler(this.lbl_Finish_MouseEnter);
            // 
            // lbl_Start
            // 
            this.lbl_Start.AutoSize = true;
            this.lbl_Start.Location = new System.Drawing.Point(27, 14);
            this.lbl_Start.Name = "lbl_Start";
            this.lbl_Start.Size = new System.Drawing.Size(29, 13);
            this.lbl_Start.TabIndex = 107;
            this.lbl_Start.Text = "Start";
            // 
            // textBox106
            // 
            this.textBox106.BackColor = System.Drawing.Color.Black;
            this.textBox106.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox106.CausesValidation = false;
            this.textBox106.Location = new System.Drawing.Point(655, 486);
            this.textBox106.Multiline = true;
            this.textBox106.Name = "textBox106";
            this.textBox106.ReadOnly = true;
            this.textBox106.ShortcutsEnabled = false;
            this.textBox106.Size = new System.Drawing.Size(12, 40);
            this.textBox106.TabIndex = 106;
            this.textBox106.TabStop = false;
            this.textBox106.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox105
            // 
            this.textBox105.BackColor = System.Drawing.Color.Black;
            this.textBox105.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox105.CausesValidation = false;
            this.textBox105.Location = new System.Drawing.Point(451, 395);
            this.textBox105.Multiline = true;
            this.textBox105.Name = "textBox105";
            this.textBox105.ReadOnly = true;
            this.textBox105.ShortcutsEnabled = false;
            this.textBox105.Size = new System.Drawing.Size(28, 10);
            this.textBox105.TabIndex = 105;
            this.textBox105.TabStop = false;
            this.textBox105.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.Color.Black;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox55.CausesValidation = false;
            this.textBox55.Location = new System.Drawing.Point(433, 343);
            this.textBox55.Multiline = true;
            this.textBox55.Name = "textBox55";
            this.textBox55.ReadOnly = true;
            this.textBox55.ShortcutsEnabled = false;
            this.textBox55.Size = new System.Drawing.Size(28, 10);
            this.textBox55.TabIndex = 104;
            this.textBox55.TabStop = false;
            this.textBox55.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox104
            // 
            this.textBox104.BackColor = System.Drawing.Color.Black;
            this.textBox104.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox104.CausesValidation = false;
            this.textBox104.Location = new System.Drawing.Point(441, 191);
            this.textBox104.Multiline = true;
            this.textBox104.Name = "textBox104";
            this.textBox104.ReadOnly = true;
            this.textBox104.ShortcutsEnabled = false;
            this.textBox104.Size = new System.Drawing.Size(44, 10);
            this.textBox104.TabIndex = 103;
            this.textBox104.TabStop = false;
            this.textBox104.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox103
            // 
            this.textBox103.BackColor = System.Drawing.Color.Black;
            this.textBox103.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox103.CausesValidation = false;
            this.textBox103.Location = new System.Drawing.Point(379, 125);
            this.textBox103.Multiline = true;
            this.textBox103.Name = "textBox103";
            this.textBox103.ReadOnly = true;
            this.textBox103.ShortcutsEnabled = false;
            this.textBox103.Size = new System.Drawing.Size(114, 11);
            this.textBox103.TabIndex = 102;
            this.textBox103.TabStop = false;
            this.textBox103.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.Color.Black;
            this.textBox102.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox102.CausesValidation = false;
            this.textBox102.Location = new System.Drawing.Point(405, 159);
            this.textBox102.Multiline = true;
            this.textBox102.Name = "textBox102";
            this.textBox102.ReadOnly = true;
            this.textBox102.ShortcutsEnabled = false;
            this.textBox102.Size = new System.Drawing.Size(12, 67);
            this.textBox102.TabIndex = 101;
            this.textBox102.TabStop = false;
            this.textBox102.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox101
            // 
            this.textBox101.BackColor = System.Drawing.Color.Black;
            this.textBox101.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox101.CausesValidation = false;
            this.textBox101.Location = new System.Drawing.Point(274, 182);
            this.textBox101.Multiline = true;
            this.textBox101.Name = "textBox101";
            this.textBox101.ReadOnly = true;
            this.textBox101.ShortcutsEnabled = false;
            this.textBox101.Size = new System.Drawing.Size(12, 67);
            this.textBox101.TabIndex = 100;
            this.textBox101.TabStop = false;
            this.textBox101.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox100
            // 
            this.textBox100.BackColor = System.Drawing.Color.Black;
            this.textBox100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox100.CausesValidation = false;
            this.textBox100.Location = new System.Drawing.Point(423, 225);
            this.textBox100.Multiline = true;
            this.textBox100.Name = "textBox100";
            this.textBox100.ReadOnly = true;
            this.textBox100.ShortcutsEnabled = false;
            this.textBox100.Size = new System.Drawing.Size(12, 103);
            this.textBox100.TabIndex = 99;
            this.textBox100.TabStop = false;
            this.textBox100.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.Color.Black;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox99.CausesValidation = false;
            this.textBox99.Location = new System.Drawing.Point(233, 225);
            this.textBox99.Multiline = true;
            this.textBox99.Name = "textBox99";
            this.textBox99.ReadOnly = true;
            this.textBox99.ShortcutsEnabled = false;
            this.textBox99.Size = new System.Drawing.Size(202, 11);
            this.textBox99.TabIndex = 98;
            this.textBox99.TabStop = false;
            this.textBox99.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.Color.Black;
            this.textBox98.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox98.CausesValidation = false;
            this.textBox98.Location = new System.Drawing.Point(204, 146);
            this.textBox98.Multiline = true;
            this.textBox98.Name = "textBox98";
            this.textBox98.ReadOnly = true;
            this.textBox98.ShortcutsEnabled = false;
            this.textBox98.Size = new System.Drawing.Size(12, 182);
            this.textBox98.TabIndex = 97;
            this.textBox98.TabStop = false;
            this.textBox98.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.Color.Black;
            this.textBox97.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox97.CausesValidation = false;
            this.textBox97.Location = new System.Drawing.Point(204, 146);
            this.textBox97.Multiline = true;
            this.textBox97.Name = "textBox97";
            this.textBox97.ReadOnly = true;
            this.textBox97.ShortcutsEnabled = false;
            this.textBox97.Size = new System.Drawing.Size(175, 11);
            this.textBox97.TabIndex = 96;
            this.textBox97.TabStop = false;
            this.textBox97.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox96
            // 
            this.textBox96.BackColor = System.Drawing.Color.Black;
            this.textBox96.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox96.CausesValidation = false;
            this.textBox96.Location = new System.Drawing.Point(318, 86);
            this.textBox96.Multiline = true;
            this.textBox96.Name = "textBox96";
            this.textBox96.ReadOnly = true;
            this.textBox96.ShortcutsEnabled = false;
            this.textBox96.Size = new System.Drawing.Size(12, 110);
            this.textBox96.TabIndex = 95;
            this.textBox96.TabStop = false;
            this.textBox96.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.Color.Black;
            this.textBox95.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox95.CausesValidation = false;
            this.textBox95.Location = new System.Drawing.Point(180, 116);
            this.textBox95.Multiline = true;
            this.textBox95.Name = "textBox95";
            this.textBox95.ReadOnly = true;
            this.textBox95.ShortcutsEnabled = false;
            this.textBox95.Size = new System.Drawing.Size(126, 10);
            this.textBox95.TabIndex = 94;
            this.textBox95.TabStop = false;
            this.textBox95.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.Color.Black;
            this.textBox94.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox94.CausesValidation = false;
            this.textBox94.Location = new System.Drawing.Point(204, 82);
            this.textBox94.Multiline = true;
            this.textBox94.Name = "textBox94";
            this.textBox94.ReadOnly = true;
            this.textBox94.ShortcutsEnabled = false;
            this.textBox94.Size = new System.Drawing.Size(156, 10);
            this.textBox94.TabIndex = 93;
            this.textBox94.TabStop = false;
            this.textBox94.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox93
            // 
            this.textBox93.BackColor = System.Drawing.Color.Black;
            this.textBox93.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox93.CausesValidation = false;
            this.textBox93.Location = new System.Drawing.Point(195, 37);
            this.textBox93.Multiline = true;
            this.textBox93.Name = "textBox93";
            this.textBox93.ReadOnly = true;
            this.textBox93.ShortcutsEnabled = false;
            this.textBox93.Size = new System.Drawing.Size(12, 55);
            this.textBox93.TabIndex = 92;
            this.textBox93.TabStop = false;
            this.textBox93.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.Color.Black;
            this.textBox92.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox92.CausesValidation = false;
            this.textBox92.Location = new System.Drawing.Point(151, 36);
            this.textBox92.Multiline = true;
            this.textBox92.Name = "textBox92";
            this.textBox92.ReadOnly = true;
            this.textBox92.ShortcutsEnabled = false;
            this.textBox92.Size = new System.Drawing.Size(56, 10);
            this.textBox92.TabIndex = 91;
            this.textBox92.TabStop = false;
            this.textBox92.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.Color.Black;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox91.CausesValidation = false;
            this.textBox91.Location = new System.Drawing.Point(685, 459);
            this.textBox91.Multiline = true;
            this.textBox91.Name = "textBox91";
            this.textBox91.ReadOnly = true;
            this.textBox91.ShortcutsEnabled = false;
            this.textBox91.Size = new System.Drawing.Size(28, 10);
            this.textBox91.TabIndex = 90;
            this.textBox91.TabStop = false;
            this.textBox91.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.Color.Black;
            this.textBox90.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox90.CausesValidation = false;
            this.textBox90.Location = new System.Drawing.Point(685, 303);
            this.textBox90.Multiline = true;
            this.textBox90.Name = "textBox90";
            this.textBox90.ReadOnly = true;
            this.textBox90.ShortcutsEnabled = false;
            this.textBox90.Size = new System.Drawing.Size(28, 10);
            this.textBox90.TabIndex = 89;
            this.textBox90.TabStop = false;
            this.textBox90.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.Color.Black;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox89.CausesValidation = false;
            this.textBox89.Location = new System.Drawing.Point(655, 391);
            this.textBox89.Multiline = true;
            this.textBox89.Name = "textBox89";
            this.textBox89.ReadOnly = true;
            this.textBox89.ShortcutsEnabled = false;
            this.textBox89.Size = new System.Drawing.Size(28, 10);
            this.textBox89.TabIndex = 88;
            this.textBox89.TabStop = false;
            this.textBox89.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.Color.Black;
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox88.CausesValidation = false;
            this.textBox88.Location = new System.Drawing.Point(657, 273);
            this.textBox88.Multiline = true;
            this.textBox88.Name = "textBox88";
            this.textBox88.ReadOnly = true;
            this.textBox88.ShortcutsEnabled = false;
            this.textBox88.Size = new System.Drawing.Size(28, 10);
            this.textBox88.TabIndex = 87;
            this.textBox88.TabStop = false;
            this.textBox88.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.Color.Black;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox87.CausesValidation = false;
            this.textBox87.Location = new System.Drawing.Point(655, 99);
            this.textBox87.Multiline = true;
            this.textBox87.Name = "textBox87";
            this.textBox87.ReadOnly = true;
            this.textBox87.ShortcutsEnabled = false;
            this.textBox87.Size = new System.Drawing.Size(12, 80);
            this.textBox87.TabIndex = 86;
            this.textBox87.TabStop = false;
            this.textBox87.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.Color.Black;
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox86.CausesValidation = false;
            this.textBox86.Location = new System.Drawing.Point(617, 26);
            this.textBox86.Multiline = true;
            this.textBox86.Name = "textBox86";
            this.textBox86.ReadOnly = true;
            this.textBox86.ShortcutsEnabled = false;
            this.textBox86.Size = new System.Drawing.Size(12, 110);
            this.textBox86.TabIndex = 85;
            this.textBox86.TabStop = false;
            this.textBox86.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.Color.Black;
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox85.CausesValidation = false;
            this.textBox85.Location = new System.Drawing.Point(629, 169);
            this.textBox85.Multiline = true;
            this.textBox85.Name = "textBox85";
            this.textBox85.ReadOnly = true;
            this.textBox85.ShortcutsEnabled = false;
            this.textBox85.Size = new System.Drawing.Size(56, 10);
            this.textBox85.TabIndex = 84;
            this.textBox85.TabStop = false;
            this.textBox85.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.Color.Black;
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox84.CausesValidation = false;
            this.textBox84.Location = new System.Drawing.Point(585, 323);
            this.textBox84.Multiline = true;
            this.textBox84.Name = "textBox84";
            this.textBox84.ReadOnly = true;
            this.textBox84.ShortcutsEnabled = false;
            this.textBox84.Size = new System.Drawing.Size(44, 10);
            this.textBox84.TabIndex = 83;
            this.textBox84.TabStop = false;
            this.textBox84.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.Color.Black;
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox83.CausesValidation = false;
            this.textBox83.Location = new System.Drawing.Point(530, 146);
            this.textBox83.Multiline = true;
            this.textBox83.Name = "textBox83";
            this.textBox83.ReadOnly = true;
            this.textBox83.ShortcutsEnabled = false;
            this.textBox83.Size = new System.Drawing.Size(39, 10);
            this.textBox83.TabIndex = 82;
            this.textBox83.TabStop = false;
            this.textBox83.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.Color.Black;
            this.textBox82.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox82.CausesValidation = false;
            this.textBox82.Location = new System.Drawing.Point(553, 3);
            this.textBox82.Multiline = true;
            this.textBox82.Name = "textBox82";
            this.textBox82.ReadOnly = true;
            this.textBox82.ShortcutsEnabled = false;
            this.textBox82.Size = new System.Drawing.Size(12, 55);
            this.textBox82.TabIndex = 81;
            this.textBox82.TabStop = false;
            this.textBox82.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.Color.Black;
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox81.CausesValidation = false;
            this.textBox81.Location = new System.Drawing.Point(557, 101);
            this.textBox81.Multiline = true;
            this.textBox81.Name = "textBox81";
            this.textBox81.ReadOnly = true;
            this.textBox81.ShortcutsEnabled = false;
            this.textBox81.Size = new System.Drawing.Size(12, 55);
            this.textBox81.TabIndex = 80;
            this.textBox81.TabStop = false;
            this.textBox81.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.Color.Black;
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox80.CausesValidation = false;
            this.textBox80.Location = new System.Drawing.Point(602, 37);
            this.textBox80.Multiline = true;
            this.textBox80.Name = "textBox80";
            this.textBox80.ReadOnly = true;
            this.textBox80.ShortcutsEnabled = false;
            this.textBox80.Size = new System.Drawing.Size(44, 10);
            this.textBox80.TabIndex = 79;
            this.textBox80.TabStop = false;
            this.textBox80.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.Color.Black;
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox79.CausesValidation = false;
            this.textBox79.Location = new System.Drawing.Point(557, 100);
            this.textBox79.Multiline = true;
            this.textBox79.Name = "textBox79";
            this.textBox79.ReadOnly = true;
            this.textBox79.ShortcutsEnabled = false;
            this.textBox79.Size = new System.Drawing.Size(44, 10);
            this.textBox79.TabIndex = 78;
            this.textBox79.TabStop = false;
            this.textBox79.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.Color.Black;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox78.CausesValidation = false;
            this.textBox78.Location = new System.Drawing.Point(557, 191);
            this.textBox78.Multiline = true;
            this.textBox78.Name = "textBox78";
            this.textBox78.ReadOnly = true;
            this.textBox78.ShortcutsEnabled = false;
            this.textBox78.Size = new System.Drawing.Size(44, 10);
            this.textBox78.TabIndex = 77;
            this.textBox78.TabStop = false;
            this.textBox78.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.Color.Black;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox77.CausesValidation = false;
            this.textBox77.Location = new System.Drawing.Point(590, 37);
            this.textBox77.Multiline = true;
            this.textBox77.Name = "textBox77";
            this.textBox77.ReadOnly = true;
            this.textBox77.ShortcutsEnabled = false;
            this.textBox77.Size = new System.Drawing.Size(12, 254);
            this.textBox77.TabIndex = 76;
            this.textBox77.TabStop = false;
            this.textBox77.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.Color.Black;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox76.CausesValidation = false;
            this.textBox76.Location = new System.Drawing.Point(602, 216);
            this.textBox76.Multiline = true;
            this.textBox76.Name = "textBox76";
            this.textBox76.ReadOnly = true;
            this.textBox76.ShortcutsEnabled = false;
            this.textBox76.Size = new System.Drawing.Size(27, 10);
            this.textBox76.TabIndex = 75;
            this.textBox76.TabStop = false;
            this.textBox76.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.Color.Black;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox75.CausesValidation = false;
            this.textBox75.Location = new System.Drawing.Point(617, 216);
            this.textBox75.Multiline = true;
            this.textBox75.Name = "textBox75";
            this.textBox75.ReadOnly = true;
            this.textBox75.ShortcutsEnabled = false;
            this.textBox75.Size = new System.Drawing.Size(44, 10);
            this.textBox75.TabIndex = 74;
            this.textBox75.TabStop = false;
            this.textBox75.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.Color.Black;
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox74.CausesValidation = false;
            this.textBox74.Location = new System.Drawing.Point(531, 225);
            this.textBox74.Multiline = true;
            this.textBox74.Name = "textBox74";
            this.textBox74.ReadOnly = true;
            this.textBox74.ShortcutsEnabled = false;
            this.textBox74.Size = new System.Drawing.Size(44, 10);
            this.textBox74.TabIndex = 73;
            this.textBox74.TabStop = false;
            this.textBox74.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.Color.Black;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox73.CausesValidation = false;
            this.textBox73.Location = new System.Drawing.Point(523, 67);
            this.textBox73.Multiline = true;
            this.textBox73.Name = "textBox73";
            this.textBox73.ReadOnly = true;
            this.textBox73.ShortcutsEnabled = false;
            this.textBox73.Size = new System.Drawing.Size(12, 224);
            this.textBox73.TabIndex = 72;
            this.textBox73.TabStop = false;
            this.textBox73.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.Color.Black;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox72.CausesValidation = false;
            this.textBox72.Location = new System.Drawing.Point(521, 353);
            this.textBox72.Multiline = true;
            this.textBox72.Name = "textBox72";
            this.textBox72.ReadOnly = true;
            this.textBox72.ShortcutsEnabled = false;
            this.textBox72.Size = new System.Drawing.Size(44, 10);
            this.textBox72.TabIndex = 71;
            this.textBox72.TabStop = false;
            this.textBox72.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.Color.Black;
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox71.CausesValidation = false;
            this.textBox71.Location = new System.Drawing.Point(479, 318);
            this.textBox71.Multiline = true;
            this.textBox71.Name = "textBox71";
            this.textBox71.ReadOnly = true;
            this.textBox71.ShortcutsEnabled = false;
            this.textBox71.Size = new System.Drawing.Size(56, 10);
            this.textBox71.TabIndex = 70;
            this.textBox71.TabStop = false;
            this.textBox71.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.Color.Black;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox70.CausesValidation = false;
            this.textBox70.Location = new System.Drawing.Point(479, 379);
            this.textBox70.Multiline = true;
            this.textBox70.Name = "textBox70";
            this.textBox70.ReadOnly = true;
            this.textBox70.ShortcutsEnabled = false;
            this.textBox70.Size = new System.Drawing.Size(44, 10);
            this.textBox70.TabIndex = 69;
            this.textBox70.TabStop = false;
            this.textBox70.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.Color.Black;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox69.CausesValidation = false;
            this.textBox69.Location = new System.Drawing.Point(473, 318);
            this.textBox69.Multiline = true;
            this.textBox69.Name = "textBox69";
            this.textBox69.ReadOnly = true;
            this.textBox69.ShortcutsEnabled = false;
            this.textBox69.Size = new System.Drawing.Size(12, 110);
            this.textBox69.TabIndex = 68;
            this.textBox69.TabStop = false;
            this.textBox69.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.Color.Black;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox68.CausesValidation = false;
            this.textBox68.Location = new System.Drawing.Point(557, 273);
            this.textBox68.Multiline = true;
            this.textBox68.Name = "textBox68";
            this.textBox68.ReadOnly = true;
            this.textBox68.ShortcutsEnabled = false;
            this.textBox68.Size = new System.Drawing.Size(12, 167);
            this.textBox68.TabIndex = 67;
            this.textBox68.TabStop = false;
            this.textBox68.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.Color.Black;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox67.CausesValidation = false;
            this.textBox67.Location = new System.Drawing.Point(521, 409);
            this.textBox67.Multiline = true;
            this.textBox67.Name = "textBox67";
            this.textBox67.ReadOnly = true;
            this.textBox67.ShortcutsEnabled = false;
            this.textBox67.Size = new System.Drawing.Size(44, 10);
            this.textBox67.TabIndex = 66;
            this.textBox67.TabStop = false;
            this.textBox67.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.Color.Black;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox66.CausesValidation = false;
            this.textBox66.Location = new System.Drawing.Point(617, 460);
            this.textBox66.Multiline = true;
            this.textBox66.Name = "textBox66";
            this.textBox66.ReadOnly = true;
            this.textBox66.ShortcutsEnabled = false;
            this.textBox66.Size = new System.Drawing.Size(44, 10);
            this.textBox66.TabIndex = 65;
            this.textBox66.TabStop = false;
            this.textBox66.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.Color.Black;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox65.CausesValidation = false;
            this.textBox65.Location = new System.Drawing.Point(655, 216);
            this.textBox65.Multiline = true;
            this.textBox65.Name = "textBox65";
            this.textBox65.ReadOnly = true;
            this.textBox65.ShortcutsEnabled = false;
            this.textBox65.Size = new System.Drawing.Size(12, 254);
            this.textBox65.TabIndex = 64;
            this.textBox65.TabStop = false;
            this.textBox65.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.Color.Black;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox64.CausesValidation = false;
            this.textBox64.Location = new System.Drawing.Point(557, 429);
            this.textBox64.Multiline = true;
            this.textBox64.Name = "textBox64";
            this.textBox64.ReadOnly = true;
            this.textBox64.ShortcutsEnabled = false;
            this.textBox64.Size = new System.Drawing.Size(72, 11);
            this.textBox64.TabIndex = 63;
            this.textBox64.TabStop = false;
            this.textBox64.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.Color.Black;
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox63.CausesValidation = false;
            this.textBox63.Location = new System.Drawing.Point(617, 251);
            this.textBox63.Multiline = true;
            this.textBox63.Name = "textBox63";
            this.textBox63.ReadOnly = true;
            this.textBox63.ShortcutsEnabled = false;
            this.textBox63.Size = new System.Drawing.Size(12, 254);
            this.textBox63.TabIndex = 62;
            this.textBox63.TabStop = false;
            this.textBox63.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.Color.Black;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox62.CausesValidation = false;
            this.textBox62.Location = new System.Drawing.Point(530, 468);
            this.textBox62.Multiline = true;
            this.textBox62.Name = "textBox62";
            this.textBox62.ReadOnly = true;
            this.textBox62.ShortcutsEnabled = false;
            this.textBox62.Size = new System.Drawing.Size(72, 11);
            this.textBox62.TabIndex = 61;
            this.textBox62.TabStop = false;
            this.textBox62.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.Color.Black;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox61.CausesValidation = false;
            this.textBox61.Location = new System.Drawing.Point(557, 494);
            this.textBox61.Multiline = true;
            this.textBox61.Name = "textBox61";
            this.textBox61.ReadOnly = true;
            this.textBox61.ShortcutsEnabled = false;
            this.textBox61.Size = new System.Drawing.Size(72, 11);
            this.textBox61.TabIndex = 60;
            this.textBox61.TabStop = false;
            this.textBox61.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.Color.Black;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox60.CausesValidation = false;
            this.textBox60.Location = new System.Drawing.Point(479, 515);
            this.textBox60.Multiline = true;
            this.textBox60.Name = "textBox60";
            this.textBox60.ReadOnly = true;
            this.textBox60.ShortcutsEnabled = false;
            this.textBox60.Size = new System.Drawing.Size(54, 11);
            this.textBox60.TabIndex = 59;
            this.textBox60.TabStop = false;
            this.textBox60.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.Color.Black;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox59.CausesValidation = false;
            this.textBox59.Location = new System.Drawing.Point(521, 440);
            this.textBox59.Multiline = true;
            this.textBox59.Name = "textBox59";
            this.textBox59.ReadOnly = true;
            this.textBox59.ShortcutsEnabled = false;
            this.textBox59.Size = new System.Drawing.Size(12, 80);
            this.textBox59.TabIndex = 58;
            this.textBox59.TabStop = false;
            this.textBox59.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.Color.Black;
            this.textBox58.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox58.CausesValidation = false;
            this.textBox58.Location = new System.Drawing.Point(479, 446);
            this.textBox58.Multiline = true;
            this.textBox58.Name = "textBox58";
            this.textBox58.ReadOnly = true;
            this.textBox58.ShortcutsEnabled = false;
            this.textBox58.Size = new System.Drawing.Size(12, 80);
            this.textBox58.TabIndex = 57;
            this.textBox58.TabStop = false;
            this.textBox58.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.Color.Black;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox57.CausesValidation = false;
            this.textBox57.Location = new System.Drawing.Point(433, 446);
            this.textBox57.Multiline = true;
            this.textBox57.Name = "textBox57";
            this.textBox57.ReadOnly = true;
            this.textBox57.ShortcutsEnabled = false;
            this.textBox57.Size = new System.Drawing.Size(52, 24);
            this.textBox57.TabIndex = 56;
            this.textBox57.TabStop = false;
            this.textBox57.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.Color.Black;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox56.CausesValidation = false;
            this.textBox56.Location = new System.Drawing.Point(359, 409);
            this.textBox56.Multiline = true;
            this.textBox56.Name = "textBox56";
            this.textBox56.ReadOnly = true;
            this.textBox56.ShortcutsEnabled = false;
            this.textBox56.Size = new System.Drawing.Size(53, 25);
            this.textBox56.TabIndex = 55;
            this.textBox56.TabStop = false;
            this.textBox56.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.Color.Black;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox54.CausesValidation = false;
            this.textBox54.Location = new System.Drawing.Point(391, 369);
            this.textBox54.Multiline = true;
            this.textBox54.Name = "textBox54";
            this.textBox54.ReadOnly = true;
            this.textBox54.ShortcutsEnabled = false;
            this.textBox54.Size = new System.Drawing.Size(44, 10);
            this.textBox54.TabIndex = 53;
            this.textBox54.TabStop = false;
            this.textBox54.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.Color.Black;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox53.CausesValidation = false;
            this.textBox53.Location = new System.Drawing.Point(433, 323);
            this.textBox53.Multiline = true;
            this.textBox53.Name = "textBox53";
            this.textBox53.ReadOnly = true;
            this.textBox53.ShortcutsEnabled = false;
            this.textBox53.Size = new System.Drawing.Size(12, 127);
            this.textBox53.TabIndex = 52;
            this.textBox53.TabStop = false;
            this.textBox53.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.Color.Black;
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox52.CausesValidation = false;
            this.textBox52.Location = new System.Drawing.Point(401, 318);
            this.textBox52.Multiline = true;
            this.textBox52.Name = "textBox52";
            this.textBox52.ReadOnly = true;
            this.textBox52.ShortcutsEnabled = false;
            this.textBox52.Size = new System.Drawing.Size(44, 10);
            this.textBox52.TabIndex = 51;
            this.textBox52.TabStop = false;
            this.textBox52.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.Color.Black;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox51.CausesValidation = false;
            this.textBox51.Location = new System.Drawing.Point(359, 343);
            this.textBox51.Multiline = true;
            this.textBox51.Name = "textBox51";
            this.textBox51.ReadOnly = true;
            this.textBox51.ShortcutsEnabled = false;
            this.textBox51.Size = new System.Drawing.Size(44, 10);
            this.textBox51.TabIndex = 50;
            this.textBox51.TabStop = false;
            this.textBox51.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.Black;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox50.CausesValidation = false;
            this.textBox50.Location = new System.Drawing.Point(391, 273);
            this.textBox50.Multiline = true;
            this.textBox50.Name = "textBox50";
            this.textBox50.ReadOnly = true;
            this.textBox50.ShortcutsEnabled = false;
            this.textBox50.Size = new System.Drawing.Size(12, 55);
            this.textBox50.TabIndex = 49;
            this.textBox50.TabStop = false;
            this.textBox50.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.Color.Black;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox49.CausesValidation = false;
            this.textBox49.Location = new System.Drawing.Point(316, 391);
            this.textBox49.Multiline = true;
            this.textBox49.Name = "textBox49";
            this.textBox49.ReadOnly = true;
            this.textBox49.ShortcutsEnabled = false;
            this.textBox49.Size = new System.Drawing.Size(44, 10);
            this.textBox49.TabIndex = 48;
            this.textBox49.TabStop = false;
            this.textBox49.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.Color.Black;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox48.CausesValidation = false;
            this.textBox48.Location = new System.Drawing.Point(286, 343);
            this.textBox48.Multiline = true;
            this.textBox48.Name = "textBox48";
            this.textBox48.ReadOnly = true;
            this.textBox48.ShortcutsEnabled = false;
            this.textBox48.Size = new System.Drawing.Size(44, 10);
            this.textBox48.TabIndex = 47;
            this.textBox48.TabStop = false;
            this.textBox48.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.Color.Black;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox47.CausesValidation = false;
            this.textBox47.Location = new System.Drawing.Point(400, 459);
            this.textBox47.Multiline = true;
            this.textBox47.Name = "textBox47";
            this.textBox47.ReadOnly = true;
            this.textBox47.ShortcutsEnabled = false;
            this.textBox47.Size = new System.Drawing.Size(12, 96);
            this.textBox47.TabIndex = 46;
            this.textBox47.TabStop = false;
            this.textBox47.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.Color.Black;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox46.CausesValidation = false;
            this.textBox46.Location = new System.Drawing.Point(351, 459);
            this.textBox46.Multiline = true;
            this.textBox46.Name = "textBox46";
            this.textBox46.ReadOnly = true;
            this.textBox46.ShortcutsEnabled = false;
            this.textBox46.Size = new System.Drawing.Size(52, 11);
            this.textBox46.TabIndex = 45;
            this.textBox46.TabStop = false;
            this.textBox46.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.Color.Black;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox45.CausesValidation = false;
            this.textBox45.Location = new System.Drawing.Point(348, 300);
            this.textBox45.Multiline = true;
            this.textBox45.Name = "textBox45";
            this.textBox45.ReadOnly = true;
            this.textBox45.ShortcutsEnabled = false;
            this.textBox45.Size = new System.Drawing.Size(12, 211);
            this.textBox45.TabIndex = 44;
            this.textBox45.TabStop = false;
            this.textBox45.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.Color.Black;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox44.CausesValidation = false;
            this.textBox44.Location = new System.Drawing.Point(248, 265);
            this.textBox44.Multiline = true;
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.ShortcutsEnabled = false;
            this.textBox44.Size = new System.Drawing.Size(155, 11);
            this.textBox44.TabIndex = 43;
            this.textBox44.TabStop = false;
            this.textBox44.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.Black;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox43.CausesValidation = false;
            this.textBox43.Location = new System.Drawing.Point(248, 273);
            this.textBox43.Multiline = true;
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.ShortcutsEnabled = false;
            this.textBox43.Size = new System.Drawing.Size(12, 80);
            this.textBox43.TabIndex = 42;
            this.textBox43.TabStop = false;
            this.textBox43.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.Color.Black;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox42.CausesValidation = false;
            this.textBox42.Location = new System.Drawing.Point(253, 378);
            this.textBox42.Multiline = true;
            this.textBox42.Name = "textBox42";
            this.textBox42.ReadOnly = true;
            this.textBox42.ShortcutsEnabled = false;
            this.textBox42.Size = new System.Drawing.Size(33, 10);
            this.textBox42.TabIndex = 41;
            this.textBox42.TabStop = false;
            this.textBox42.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.Color.Black;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox41.CausesValidation = false;
            this.textBox41.Location = new System.Drawing.Point(283, 300);
            this.textBox41.Multiline = true;
            this.textBox41.Name = "textBox41";
            this.textBox41.ReadOnly = true;
            this.textBox41.ShortcutsEnabled = false;
            this.textBox41.Size = new System.Drawing.Size(12, 211);
            this.textBox41.TabIndex = 40;
            this.textBox41.TabStop = false;
            this.textBox41.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.Color.Black;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox40.CausesValidation = false;
            this.textBox40.Location = new System.Drawing.Point(248, 378);
            this.textBox40.Multiline = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.ShortcutsEnabled = false;
            this.textBox40.Size = new System.Drawing.Size(12, 62);
            this.textBox40.TabIndex = 39;
            this.textBox40.TabStop = false;
            this.textBox40.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.Color.Black;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox39.CausesValidation = false;
            this.textBox39.Location = new System.Drawing.Point(218, 342);
            this.textBox39.Multiline = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.ReadOnly = true;
            this.textBox39.ShortcutsEnabled = false;
            this.textBox39.Size = new System.Drawing.Size(12, 80);
            this.textBox39.TabIndex = 38;
            this.textBox39.TabStop = false;
            this.textBox39.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.Color.Black;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox38.CausesValidation = false;
            this.textBox38.Location = new System.Drawing.Point(194, 440);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.ShortcutsEnabled = false;
            this.textBox38.Size = new System.Drawing.Size(66, 10);
            this.textBox38.TabIndex = 37;
            this.textBox38.TabStop = false;
            this.textBox38.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.Color.Black;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox37.CausesValidation = false;
            this.textBox37.Location = new System.Drawing.Point(192, 378);
            this.textBox37.Multiline = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.ReadOnly = true;
            this.textBox37.ShortcutsEnabled = false;
            this.textBox37.Size = new System.Drawing.Size(12, 127);
            this.textBox37.TabIndex = 36;
            this.textBox37.TabStop = false;
            this.textBox37.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.Color.Black;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox36.CausesValidation = false;
            this.textBox36.Location = new System.Drawing.Point(148, 424);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.ShortcutsEnabled = false;
            this.textBox36.Size = new System.Drawing.Size(56, 10);
            this.textBox36.TabIndex = 35;
            this.textBox36.TabStop = false;
            this.textBox36.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.Color.Black;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox35.CausesValidation = false;
            this.textBox35.Location = new System.Drawing.Point(148, 424);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.ShortcutsEnabled = false;
            this.textBox35.Size = new System.Drawing.Size(12, 55);
            this.textBox35.TabIndex = 34;
            this.textBox35.TabStop = false;
            this.textBox35.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.Color.Black;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox34.CausesValidation = false;
            this.textBox34.Location = new System.Drawing.Point(127, 495);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.ShortcutsEnabled = false;
            this.textBox34.Size = new System.Drawing.Size(33, 10);
            this.textBox34.TabIndex = 33;
            this.textBox34.TabStop = false;
            this.textBox34.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.Color.Black;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox33.CausesValidation = false;
            this.textBox33.Location = new System.Drawing.Point(56, 468);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.ShortcutsEnabled = false;
            this.textBox33.Size = new System.Drawing.Size(104, 11);
            this.textBox33.TabIndex = 32;
            this.textBox33.TabStop = false;
            this.textBox33.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.Color.Black;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox32.CausesValidation = false;
            this.textBox32.Location = new System.Drawing.Point(52, 409);
            this.textBox32.Multiline = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.ShortcutsEnabled = false;
            this.textBox32.Size = new System.Drawing.Size(12, 102);
            this.textBox32.TabIndex = 31;
            this.textBox32.TabStop = false;
            this.textBox32.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.Black;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox31.CausesValidation = false;
            this.textBox31.Location = new System.Drawing.Point(52, 280);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.ShortcutsEnabled = false;
            this.textBox31.Size = new System.Drawing.Size(12, 55);
            this.textBox31.TabIndex = 30;
            this.textBox31.TabStop = false;
            this.textBox31.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.Color.Black;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox30.CausesValidation = false;
            this.textBox30.Location = new System.Drawing.Point(96, 318);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.ShortcutsEnabled = false;
            this.textBox30.Size = new System.Drawing.Size(12, 127);
            this.textBox30.TabIndex = 29;
            this.textBox30.TabStop = false;
            this.textBox30.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.Black;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox29.CausesValidation = false;
            this.textBox29.Location = new System.Drawing.Point(56, 378);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.ShortcutsEnabled = false;
            this.textBox29.Size = new System.Drawing.Size(87, 11);
            this.textBox29.TabIndex = 28;
            this.textBox29.TabStop = false;
            this.textBox29.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.Black;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox28.CausesValidation = false;
            this.textBox28.Location = new System.Drawing.Point(131, 342);
            this.textBox28.Multiline = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.ShortcutsEnabled = false;
            this.textBox28.Size = new System.Drawing.Size(12, 47);
            this.textBox28.TabIndex = 27;
            this.textBox28.TabStop = false;
            this.textBox28.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.Black;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox27.CausesValidation = false;
            this.textBox27.Location = new System.Drawing.Point(75, 342);
            this.textBox27.Multiline = true;
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.ShortcutsEnabled = false;
            this.textBox27.Size = new System.Drawing.Size(185, 11);
            this.textBox27.TabIndex = 26;
            this.textBox27.TabStop = false;
            this.textBox27.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.Black;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.CausesValidation = false;
            this.textBox26.Location = new System.Drawing.Point(171, 99);
            this.textBox26.Multiline = true;
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.ShortcutsEnabled = false;
            this.textBox26.Size = new System.Drawing.Size(12, 254);
            this.textBox26.TabIndex = 25;
            this.textBox26.TabStop = false;
            this.textBox26.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Black;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.CausesValidation = false;
            this.textBox25.Location = new System.Drawing.Point(96, 99);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.ShortcutsEnabled = false;
            this.textBox25.Size = new System.Drawing.Size(87, 11);
            this.textBox25.TabIndex = 24;
            this.textBox25.TabStop = false;
            this.textBox25.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.Black;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.CausesValidation = false;
            this.textBox24.Location = new System.Drawing.Point(131, 146);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.ShortcutsEnabled = false;
            this.textBox24.Size = new System.Drawing.Size(12, 167);
            this.textBox24.TabIndex = 23;
            this.textBox24.TabStop = false;
            this.textBox24.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Black;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.CausesValidation = false;
            this.textBox23.Location = new System.Drawing.Point(87, 225);
            this.textBox23.Multiline = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.ShortcutsEnabled = false;
            this.textBox23.Size = new System.Drawing.Size(56, 10);
            this.textBox23.TabIndex = 22;
            this.textBox23.TabStop = false;
            this.textBox23.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.Color.Black;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox22.CausesValidation = false;
            this.textBox22.Location = new System.Drawing.Point(52, 191);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.ShortcutsEnabled = false;
            this.textBox22.Size = new System.Drawing.Size(56, 10);
            this.textBox22.TabIndex = 21;
            this.textBox22.TabStop = false;
            this.textBox22.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.Color.Black;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox21.CausesValidation = false;
            this.textBox21.Location = new System.Drawing.Point(74, 225);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.ShortcutsEnabled = false;
            this.textBox21.Size = new System.Drawing.Size(16, 66);
            this.textBox21.TabIndex = 20;
            this.textBox21.TabStop = false;
            this.textBox21.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.Black;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.CausesValidation = false;
            this.textBox20.Location = new System.Drawing.Point(44, 136);
            this.textBox20.Multiline = true;
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.ShortcutsEnabled = false;
            this.textBox20.Size = new System.Drawing.Size(20, 65);
            this.textBox20.TabIndex = 19;
            this.textBox20.TabStop = false;
            this.textBox20.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.Black;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.CausesValidation = false;
            this.textBox19.Location = new System.Drawing.Point(52, 136);
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.ShortcutsEnabled = false;
            this.textBox19.Size = new System.Drawing.Size(56, 10);
            this.textBox19.TabIndex = 18;
            this.textBox19.TabStop = false;
            this.textBox19.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.Black;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.CausesValidation = false;
            this.textBox18.Location = new System.Drawing.Point(96, 36);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.ShortcutsEnabled = false;
            this.textBox18.Size = new System.Drawing.Size(12, 110);
            this.textBox18.TabIndex = 17;
            this.textBox18.TabStop = false;
            this.textBox18.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.Black;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.CausesValidation = false;
            this.textBox17.Location = new System.Drawing.Point(74, 36);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.ShortcutsEnabled = false;
            this.textBox17.Size = new System.Drawing.Size(56, 10);
            this.textBox17.TabIndex = 16;
            this.textBox17.TabStop = false;
            this.textBox17.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.Color.Black;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox16.CausesValidation = false;
            this.textBox16.Location = new System.Drawing.Point(655, 497);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.ShortcutsEnabled = false;
            this.textBox16.Size = new System.Drawing.Size(58, 14);
            this.textBox16.TabIndex = 15;
            this.textBox16.TabStop = false;
            this.textBox16.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.Color.Black;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.CausesValidation = false;
            this.textBox15.Location = new System.Drawing.Point(655, 86);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.ShortcutsEnabled = false;
            this.textBox15.Size = new System.Drawing.Size(58, 14);
            this.textBox15.TabIndex = 14;
            this.textBox15.TabStop = false;
            this.textBox15.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.Black;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox14.CausesValidation = false;
            this.textBox14.Location = new System.Drawing.Point(18, 67);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.ShortcutsEnabled = false;
            this.textBox14.Size = new System.Drawing.Size(56, 10);
            this.textBox14.TabIndex = 13;
            this.textBox14.TabStop = false;
            this.textBox14.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.Color.Black;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.CausesValidation = false;
            this.textBox13.Location = new System.Drawing.Point(22, 280);
            this.textBox13.Multiline = true;
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.ShortcutsEnabled = false;
            this.textBox13.Size = new System.Drawing.Size(52, 11);
            this.textBox13.TabIndex = 12;
            this.textBox13.TabStop = false;
            this.textBox13.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.Black;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.CausesValidation = false;
            this.textBox12.Location = new System.Drawing.Point(115, 495);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.ShortcutsEnabled = false;
            this.textBox12.Size = new System.Drawing.Size(15, 48);
            this.textBox12.TabIndex = 11;
            this.textBox12.TabStop = false;
            this.textBox12.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.Black;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.CausesValidation = false;
            this.textBox11.Location = new System.Drawing.Point(313, 446);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.ShortcutsEnabled = false;
            this.textBox11.Size = new System.Drawing.Size(17, 97);
            this.textBox11.TabIndex = 10;
            this.textBox11.TabStop = false;
            this.textBox11.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Black;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.CausesValidation = false;
            this.textBox10.Location = new System.Drawing.Point(233, 446);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.ShortcutsEnabled = false;
            this.textBox10.Size = new System.Drawing.Size(17, 97);
            this.textBox10.TabIndex = 9;
            this.textBox10.TabStop = false;
            this.textBox10.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Black;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.CausesValidation = false;
            this.textBox9.Location = new System.Drawing.Point(428, 495);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.ShortcutsEnabled = false;
            this.textBox9.Size = new System.Drawing.Size(17, 60);
            this.textBox9.TabIndex = 8;
            this.textBox9.TabStop = false;
            this.textBox9.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.Black;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.CausesValidation = false;
            this.textBox8.Location = new System.Drawing.Point(557, 495);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.ShortcutsEnabled = false;
            this.textBox8.Size = new System.Drawing.Size(18, 48);
            this.textBox8.TabIndex = 7;
            this.textBox8.TabStop = false;
            this.textBox8.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Black;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.CausesValidation = false;
            this.textBox7.Location = new System.Drawing.Point(472, 3);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.ShortcutsEnabled = false;
            this.textBox7.Size = new System.Drawing.Size(19, 273);
            this.textBox7.TabIndex = 6;
            this.textBox7.TabStop = false;
            this.textBox7.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.Black;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.CausesValidation = false;
            this.textBox6.Location = new System.Drawing.Point(426, 3);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.ShortcutsEnabled = false;
            this.textBox6.Size = new System.Drawing.Size(19, 97);
            this.textBox6.TabIndex = 5;
            this.textBox6.TabStop = false;
            this.textBox6.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Black;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.CausesValidation = false;
            this.textBox5.Location = new System.Drawing.Point(248, 3);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.ShortcutsEnabled = false;
            this.textBox5.Size = new System.Drawing.Size(17, 89);
            this.textBox5.TabIndex = 4;
            this.textBox5.TabStop = false;
            this.textBox5.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.Black;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.CausesValidation = false;
            this.textBox4.Location = new System.Drawing.Point(712, 4);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.ShortcutsEnabled = false;
            this.textBox4.Size = new System.Drawing.Size(20, 508);
            this.textBox4.TabIndex = 3;
            this.textBox4.TabStop = false;
            this.textBox4.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.Black;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.CausesValidation = false;
            this.textBox3.Location = new System.Drawing.Point(-2, 36);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.ShortcutsEnabled = false;
            this.textBox3.Size = new System.Drawing.Size(25, 507);
            this.textBox3.TabIndex = 2;
            this.textBox3.TabStop = false;
            this.textBox3.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Black;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.CausesValidation = false;
            this.textBox2.Location = new System.Drawing.Point(0, 542);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ShortcutsEnabled = false;
            this.textBox2.Size = new System.Drawing.Size(734, 13);
            this.textBox2.TabIndex = 1;
            this.textBox2.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.CausesValidation = false;
            this.textBox1.Location = new System.Drawing.Point(-2, -2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ShortcutsEnabled = false;
            this.textBox1.Size = new System.Drawing.Size(734, 13);
            this.textBox1.TabIndex = 0;
            this.textBox1.TabStop = false;
            this.textBox1.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // timer_ElapsedTime
            // 
            this.timer_ElapsedTime.Tick += new System.EventHandler(this.timer_ElapsedTime_Tick);
            // 
            // txt_ElapsedTime
            // 
            this.txt_ElapsedTime.Location = new System.Drawing.Point(278, -2);
            this.txt_ElapsedTime.Name = "txt_ElapsedTime";
            this.txt_ElapsedTime.ReadOnly = true;
            this.txt_ElapsedTime.Size = new System.Drawing.Size(109, 20);
            this.txt_ElapsedTime.TabIndex = 1;
            // 
            // lbl_ElapsedTime
            // 
            this.lbl_ElapsedTime.AutoSize = true;
            this.lbl_ElapsedTime.Location = new System.Drawing.Point(237, 5);
            this.lbl_ElapsedTime.Name = "lbl_ElapsedTime";
            this.lbl_ElapsedTime.Size = new System.Drawing.Size(30, 13);
            this.lbl_ElapsedTime.TabIndex = 2;
            this.lbl_ElapsedTime.Text = "Time";
            // 
            // lbl_Tries
            // 
            this.lbl_Tries.AutoSize = true;
            this.lbl_Tries.Location = new System.Drawing.Point(22, 5);
            this.lbl_Tries.Name = "lbl_Tries";
            this.lbl_Tries.Size = new System.Drawing.Size(30, 13);
            this.lbl_Tries.TabIndex = 3;
            this.lbl_Tries.Text = "Tries";
            // 
            // txt_TriesCounter
            // 
            this.txt_TriesCounter.Location = new System.Drawing.Point(55, -2);
            this.txt_TriesCounter.Name = "txt_TriesCounter";
            this.txt_TriesCounter.ReadOnly = true;
            this.txt_TriesCounter.Size = new System.Drawing.Size(100, 20);
            this.txt_TriesCounter.TabIndex = 4;
            // 
            // timer_MessageTimer
            // 
            this.timer_MessageTimer.Interval = 500;
            this.timer_MessageTimer.Tick += new System.EventHandler(this.timer_MessageTimer_Tick);
            // 
            // lbl_TryAgainMessage
            // 
            this.lbl_TryAgainMessage.BackColor = System.Drawing.Color.DarkRed;
            this.lbl_TryAgainMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_TryAgainMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TryAgainMessage.Location = new System.Drawing.Point(0, 0);
            this.lbl_TryAgainMessage.Name = "lbl_TryAgainMessage";
            this.lbl_TryAgainMessage.Size = new System.Drawing.Size(730, 558);
            this.lbl_TryAgainMessage.TabIndex = 6;
            this.lbl_TryAgainMessage.Text = "Opps! \r\nDon\'t touch the walls. \r\nTry again.";
            this.lbl_TryAgainMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_TryAgainMessage.Visible = false;
            // 
            // textBox107
            // 
            this.textBox107.BackColor = System.Drawing.Color.Black;
            this.textBox107.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox107.CausesValidation = false;
            this.textBox107.Location = new System.Drawing.Point(0, 3);
            this.textBox107.Multiline = true;
            this.textBox107.Name = "textBox107";
            this.textBox107.ReadOnly = true;
            this.textBox107.ShortcutsEnabled = false;
            this.textBox107.Size = new System.Drawing.Size(12, 55);
            this.textBox107.TabIndex = 109;
            this.textBox107.TabStop = false;
            this.textBox107.MouseEnter += new System.EventHandler(this.wall_MouseEnter);
            // 
            // Maze
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 606);
            this.Controls.Add(this.txt_TriesCounter);
            this.Controls.Add(this.lbl_Tries);
            this.Controls.Add(this.lbl_ElapsedTime);
            this.Controls.Add(this.txt_ElapsedTime);
            this.Controls.Add(this.pnl_GameBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Maze";
            this.Text = "Maze";
            this.Load += new System.EventHandler(this.Maze_Load);
            this.pnl_GameBoard.ResumeLayout(false);
            this.pnl_GameBoard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnl_GameBoard;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label lbl_Finish;
        private System.Windows.Forms.Label lbl_Start;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.Timer timer_ElapsedTime;
        private System.Windows.Forms.TextBox txt_ElapsedTime;
        private System.Windows.Forms.Label lbl_ElapsedTime;
        private System.Windows.Forms.Label lbl_Tries;
        private System.Windows.Forms.TextBox txt_TriesCounter;
        private System.Windows.Forms.Timer timer_MessageTimer;
        private System.Windows.Forms.Label lbl_TryAgainMessage;
        private System.Windows.Forms.TextBox textBox107;
    }
}