﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MazeGame
{
    public class GameLogic
    {
        public void MoveToStart(Point location, Maze maze)
        {
            Point start = location;
            start.Offset(15, 15);
            Cursor.Position = maze.PointToScreen(start);
        }

        internal void DisplayFailMessage(Timer timer_MessageTimer, Label lbl_TryAgainMessage)
        {
            timer_MessageTimer.Stop();
            lbl_TryAgainMessage.Visible = false;
            Cursor.Show();
        }
    }
}