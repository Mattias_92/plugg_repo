﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserManager;

namespace ModulTvåFroms
{
    public partial class frmRegister : Form
    {
        private readonly MockDB.MockUserDb _userDb;

        public frmRegister(MockDB.MockUserDb userDb)
        {
            InitializeComponent();
            _userDb = userDb;
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            User newUser = new User();
            if (CreateUser.IsEmailValid(txt_Email.Text))
            {
                var existingEmail = _userDb.Users.FirstOrDefault(x => x.UserEmail == txt_Email.Text);
                if (existingEmail == null)
                {
                    var userBirthDate = dtp_BirthDate.Value;
                    if (userBirthDate > DateTime.Today)
                    {
                        lbl_InvalidBirthDate.Text = "Invalid Date!";
                        lbl_InvalidBirthDate.ForeColor = Color.Red;
                        lbl_InvalidBirthDate.Visible = true;
                    }
                    else if (string.IsNullOrWhiteSpace(txt_FirstName.Text)|| string.IsNullOrWhiteSpace(txt_LastName.Text))
                    {
                        lbl_invaliFIrstName.Text = "Please fill in your name";
                        lbl_InvalidLastName.Text = "Please fill in your name";

                        lbl_invaliFIrstName.ForeColor = Color.Red;
                        lbl_InvalidLastName.ForeColor = Color.Red;

                        lbl_invaliFIrstName.Visible = true;
                        lbl_InvalidLastName.Visible = true;
                    }
                    else
                    {
                        newUser.UserEmail = txt_Email.Text;
                        var hashedPass = CreateUser.GetAPassword(txt_Password.Text);
                        newUser.HashedPassword = hashedPass;
                        newUser.FirstName = txt_FirstName.Text;
                        newUser.LastName = txt_LastName.Text;
                        newUser.BirtDate = userBirthDate;
                        _users.Add(newUser);
                        MessageBox.Show("New user Added Succesfully!");
                        this.Close();
                    }
                }
                else
                {
                    lbl_InvalidEmail.Text = "Email already exists";
                    lbl_InvalidEmail.Visible = true;
                }
                
            }
            else
            {
                lbl_InvalidEmail.Text = "Invalid email format!";
                lbl_InvalidEmail.Visible = true;
                lbl_InvalidEmail.ForeColor = Color.Red;
            }
        }

        private void btn_ReturnToLogin_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
