﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModulTvåFroms
{
    public partial class frmLogin : Form
    {
        private readonly List<User> _users;
        public frmLogin()
        {
            InitializeComponent();
            _users = new List<User>();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            _users.Add(new User
            {
                UserEmail = "test@live.se",
                HashedPassword = "9C478BF63E9500CB5DB1E85ECE82F18C8EB9E52E2F9135ACD7F10972C8D563BA",
                BirtDate = new DateTime(2020,07,25),
                FirstName = "Test",
                LastName = "Test2"

            });
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            frmRegister frmRegister = new frmRegister(_users);
            frmRegister.ShowDialog();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            var password = ManageUser.GetAPassword(txt_Password.Text);
            var user = _users.FirstOrDefault(x => x.UserEmail == txt_Email.Text && x.HashedPassword == password);
            if (user == null)
            {
                lbl_InvalidLogin.Text = "Invalid email or password!";
                lbl_InvalidLogin.ForeColor = Color.Red;
                lbl_InvalidLogin.Visible = true;
            }
            else
            {
                frmWelcomeForm welcomeForm = new frmWelcomeForm(user);
                welcomeForm.ShowDialog();
            }
        }
    }
}
