﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModulTvåFroms
{
    public partial class frmWelcomeForm : Form
    {
        private readonly User _user;
        public frmWelcomeForm(User user)
        {
            InitializeComponent();
            _user = user;
        }

        private void frmWelcomeForm_Load(object sender, EventArgs e)
        {
            lbl_Welcome.Text = $"Welcome {_user.FullName}";
            lbl_Welcome.ForeColor = Color.ForestGreen;
            lbl_Welcome.Visible = true;
            var userAge = ManageUser.GetUserAge(_user);
            lbl_age.Text = $"You are {userAge.UserYears} years, {userAge.UserMonts} months and {userAge.UserDays} days old.";
            lbl_age.Visible = true;
            var nextBirtDate = ManageUser.TimeTillNextBirtDay(_user);
            lbl_NextBirtDay.Text = $"You have {nextBirtDate.UserMonts} months and {nextBirtDate.UserDays} days till next birth day.";
            lbl_NextBirtDay.Visible = true;
        }

        private void btn_LogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
