﻿
namespace ModulTvåFroms
{
    partial class frmShowTextMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txb_MessageOne = new System.Windows.Forms.TextBox();
            this.txb_MessageTwo = new System.Windows.Forms.TextBox();
            this.btn_ShowMessage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txb_MessageOne
            // 
            this.txb_MessageOne.Location = new System.Drawing.Point(257, 101);
            this.txb_MessageOne.Name = "txb_MessageOne";
            this.txb_MessageOne.Size = new System.Drawing.Size(301, 38);
            this.txb_MessageOne.TabIndex = 0;
            // 
            // txb_MessageTwo
            // 
            this.txb_MessageTwo.Location = new System.Drawing.Point(985, 101);
            this.txb_MessageTwo.Name = "txb_MessageTwo";
            this.txb_MessageTwo.Size = new System.Drawing.Size(301, 38);
            this.txb_MessageTwo.TabIndex = 1;
            // 
            // btn_ShowMessage
            // 
            this.btn_ShowMessage.Location = new System.Drawing.Point(639, 316);
            this.btn_ShowMessage.Name = "btn_ShowMessage";
            this.btn_ShowMessage.Size = new System.Drawing.Size(273, 70);
            this.btn_ShowMessage.TabIndex = 2;
            this.btn_ShowMessage.Text = "Show";
            this.btn_ShowMessage.UseVisualStyleBackColor = true;
            this.btn_ShowMessage.Click += new System.EventHandler(this.btn_ShowMessage_Click);
            // 
            // frmShowTextMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1592, 685);
            this.Controls.Add(this.btn_ShowMessage);
            this.Controls.Add(this.txb_MessageTwo);
            this.Controls.Add(this.txb_MessageOne);
            this.Name = "frmShowTextMessage";
            this.Text = "Visa Text Meddelande";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txb_MessageOne;
        private System.Windows.Forms.TextBox txb_MessageTwo;
        private System.Windows.Forms.Button btn_ShowMessage;
    }
}

