﻿
namespace ModulTvåFroms
{
    partial class frmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Email = new System.Windows.Forms.TextBox();
            this.txt_Password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtp_BirthDate = new System.Windows.Forms.DateTimePicker();
            this.btn_Register = new System.Windows.Forms.Button();
            this.lbl_InvalidEmail = new System.Windows.Forms.Label();
            this.lbl_InvalidBirthDate = new System.Windows.Forms.Label();
            this.btn_ReturnToLogin = new System.Windows.Forms.Button();
            this.txt_FirstName = new System.Windows.Forms.TextBox();
            this.txt_LastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_invaliFIrstName = new System.Windows.Forms.Label();
            this.lbl_InvalidLastName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_Email
            // 
            this.txt_Email.Location = new System.Drawing.Point(50, 129);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(417, 38);
            this.txt_Email.TabIndex = 0;
            // 
            // txt_Password
            // 
            this.txt_Password.Location = new System.Drawing.Point(56, 281);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.Size = new System.Drawing.Size(417, 38);
            this.txt_Password.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 229);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 32);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(699, 388);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Birthdate";
            // 
            // dtp_BirthDate
            // 
            this.dtp_BirthDate.Location = new System.Drawing.Point(705, 454);
            this.dtp_BirthDate.Name = "dtp_BirthDate";
            this.dtp_BirthDate.Size = new System.Drawing.Size(411, 38);
            this.dtp_BirthDate.TabIndex = 6;
            // 
            // btn_Register
            // 
            this.btn_Register.Location = new System.Drawing.Point(412, 588);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(214, 79);
            this.btn_Register.TabIndex = 7;
            this.btn_Register.Text = "Register";
            this.btn_Register.UseVisualStyleBackColor = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // lbl_InvalidEmail
            // 
            this.lbl_InvalidEmail.AutoSize = true;
            this.lbl_InvalidEmail.Location = new System.Drawing.Point(56, 174);
            this.lbl_InvalidEmail.Name = "lbl_InvalidEmail";
            this.lbl_InvalidEmail.Size = new System.Drawing.Size(0, 32);
            this.lbl_InvalidEmail.TabIndex = 8;
            this.lbl_InvalidEmail.Visible = false;
            // 
            // lbl_InvalidBirthDate
            // 
            this.lbl_InvalidBirthDate.AutoSize = true;
            this.lbl_InvalidBirthDate.Location = new System.Drawing.Point(705, 499);
            this.lbl_InvalidBirthDate.Name = "lbl_InvalidBirthDate";
            this.lbl_InvalidBirthDate.Size = new System.Drawing.Size(0, 32);
            this.lbl_InvalidBirthDate.TabIndex = 9;
            this.lbl_InvalidBirthDate.Visible = false;
            // 
            // btn_ReturnToLogin
            // 
            this.btn_ReturnToLogin.Location = new System.Drawing.Point(633, 588);
            this.btn_ReturnToLogin.Name = "btn_ReturnToLogin";
            this.btn_ReturnToLogin.Size = new System.Drawing.Size(196, 79);
            this.btn_ReturnToLogin.TabIndex = 10;
            this.btn_ReturnToLogin.Text = "Cancel";
            this.btn_ReturnToLogin.UseVisualStyleBackColor = true;
            this.btn_ReturnToLogin.Click += new System.EventHandler(this.btn_ReturnToLogin_Click);
            // 
            // txt_FirstName
            // 
            this.txt_FirstName.Location = new System.Drawing.Point(705, 129);
            this.txt_FirstName.Name = "txt_FirstName";
            this.txt_FirstName.Size = new System.Drawing.Size(417, 38);
            this.txt_FirstName.TabIndex = 11;
            // 
            // txt_LastName
            // 
            this.txt_LastName.Location = new System.Drawing.Point(705, 281);
            this.txt_LastName.Name = "txt_LastName";
            this.txt_LastName.Size = new System.Drawing.Size(417, 38);
            this.txt_LastName.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(709, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 32);
            this.label4.TabIndex = 13;
            this.label4.Text = "Last Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(709, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 32);
            this.label5.TabIndex = 14;
            this.label5.Text = "First Name";
            // 
            // lbl_invaliFIrstName
            // 
            this.lbl_invaliFIrstName.AutoSize = true;
            this.lbl_invaliFIrstName.Location = new System.Drawing.Point(705, 174);
            this.lbl_invaliFIrstName.Name = "lbl_invaliFIrstName";
            this.lbl_invaliFIrstName.Size = new System.Drawing.Size(0, 32);
            this.lbl_invaliFIrstName.TabIndex = 15;
            this.lbl_invaliFIrstName.Visible = false;
            // 
            // lbl_InvalidLastName
            // 
            this.lbl_InvalidLastName.AutoSize = true;
            this.lbl_InvalidLastName.Location = new System.Drawing.Point(705, 334);
            this.lbl_InvalidLastName.Name = "lbl_InvalidLastName";
            this.lbl_InvalidLastName.Size = new System.Drawing.Size(0, 32);
            this.lbl_InvalidLastName.TabIndex = 16;
            this.lbl_InvalidLastName.Visible = false;
            // 
            // frmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 763);
            this.Controls.Add(this.lbl_InvalidLastName);
            this.Controls.Add(this.lbl_invaliFIrstName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_LastName);
            this.Controls.Add(this.txt_FirstName);
            this.Controls.Add(this.btn_ReturnToLogin);
            this.Controls.Add(this.lbl_InvalidBirthDate);
            this.Controls.Add(this.lbl_InvalidEmail);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.dtp_BirthDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Password);
            this.Controls.Add(this.txt_Email);
            this.Name = "frmRegister";
            this.Text = "Register New User";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Email;
        private System.Windows.Forms.TextBox txt_Password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtp_BirthDate;
        private System.Windows.Forms.Button btn_Register;
        private System.Windows.Forms.Label lbl_InvalidEmail;
        private System.Windows.Forms.Label lbl_InvalidBirthDate;
        private System.Windows.Forms.Button btn_ReturnToLogin;
        private System.Windows.Forms.TextBox txt_FirstName;
        private System.Windows.Forms.TextBox txt_LastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_invaliFIrstName;
        private System.Windows.Forms.Label lbl_InvalidLastName;
    }
}