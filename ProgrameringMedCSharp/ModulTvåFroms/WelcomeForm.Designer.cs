﻿
namespace ModulTvåFroms
{
    partial class frmWelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Welcome = new System.Windows.Forms.Label();
            this.btn_LogOut = new System.Windows.Forms.Button();
            this.lbl_age = new System.Windows.Forms.Label();
            this.lbl_NextBirtDay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_Welcome
            // 
            this.lbl_Welcome.AutoSize = true;
            this.lbl_Welcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Welcome.Location = new System.Drawing.Point(443, 44);
            this.lbl_Welcome.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.lbl_Welcome.Name = "lbl_Welcome";
            this.lbl_Welcome.Size = new System.Drawing.Size(0, 58);
            this.lbl_Welcome.TabIndex = 0;
            this.lbl_Welcome.Visible = false;
            // 
            // btn_LogOut
            // 
            this.btn_LogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LogOut.Location = new System.Drawing.Point(322, 473);
            this.btn_LogOut.Name = "btn_LogOut";
            this.btn_LogOut.Size = new System.Drawing.Size(235, 82);
            this.btn_LogOut.TabIndex = 2;
            this.btn_LogOut.Text = "Log out";
            this.btn_LogOut.UseVisualStyleBackColor = true;
            this.btn_LogOut.Click += new System.EventHandler(this.btn_LogOut_Click);
            // 
            // lbl_age
            // 
            this.lbl_age.AutoSize = true;
            this.lbl_age.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_age.Location = new System.Drawing.Point(72, 127);
            this.lbl_age.Name = "lbl_age";
            this.lbl_age.Size = new System.Drawing.Size(0, 46);
            this.lbl_age.TabIndex = 3;
            this.lbl_age.Visible = false;
            // 
            // lbl_NextBirtDay
            // 
            this.lbl_NextBirtDay.AutoSize = true;
            this.lbl_NextBirtDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lbl_NextBirtDay.Location = new System.Drawing.Point(72, 243);
            this.lbl_NextBirtDay.Name = "lbl_NextBirtDay";
            this.lbl_NextBirtDay.Size = new System.Drawing.Size(0, 46);
            this.lbl_NextBirtDay.TabIndex = 4;
            this.lbl_NextBirtDay.Visible = false;
            // 
            // frmWelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(49F, 95F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 603);
            this.Controls.Add(this.lbl_NextBirtDay);
            this.Controls.Add(this.lbl_age);
            this.Controls.Add(this.btn_LogOut);
            this.Controls.Add(this.lbl_Welcome);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(9);
            this.Name = "frmWelcomeForm";
            this.Text = "Welcome Form";
            this.Load += new System.EventHandler(this.frmWelcomeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Welcome;
        private System.Windows.Forms.Button btn_LogOut;
        private System.Windows.Forms.Label lbl_age;
        private System.Windows.Forms.Label lbl_NextBirtDay;
    }
}