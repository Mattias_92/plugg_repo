﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModulTvåFroms
{
    public partial class frmShowTextMessage : Form
    {
        public frmShowTextMessage()
        {
            InitializeComponent();
        }

        private void btn_ShowMessage_Click(object sender, EventArgs e)
        {
            string messageOne = txb_MessageOne.Text;
            string messageTwo = txb_MessageTwo.Text;
            string mergedMessage = messageOne + " " + messageTwo;
            MessageBox.Show(mergedMessage);
        }
    }
}
