﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockDB
{
    public class MockUserDb : MockDb
    {
        public ReadOnlyCollection<UserManager.User> Users { 
            get 
            {
                return new ReadOnlyCollection<UserManager.User>(this.users); 
            } 
        }
        List<UserManager.User> users;
        public MockUserDb()
        {
            users = new List<UserManager.User>();
            ReadDbFromFile();
        }

        public void PopulateUserDbWithPreMades()
        {
            _db.AddRange(PreMadeData.GetUsers());
        }
        public override void ReadDbFromFile()
        {
            string path = GetDbPath();
            using (StreamReader file = File.OpenText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<UserManager.User> users = (List<UserManager.User>)serializer.Deserialize(file, typeof(List<UserManager.User>));
                _db.AddRange(users);
            }
        }
        


    }
}
