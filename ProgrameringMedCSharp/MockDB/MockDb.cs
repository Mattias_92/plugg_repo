﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MockDB
{
    public abstract class MockDb
    {
        protected List<HelperModels.IDbObject> _db { get; }
        private string dbPath;
        public MockDb()
        {
            _db = new List<HelperModels.IDbObject>();
        }
        public HelperModels.IDbObject GetEntity(int id)
        {
            var retEntity = _db.FirstOrDefault(x => x.Id == id);
            return retEntity;
        }
        public List<HelperModels.IDbObject> ViewDb()
        {
            string serialisedDb = JsonConvert.SerializeObject(_db, Formatting.Indented);
            string type = GetTypeOfDb(this);

            List<HelperModels.IDbObject> dbObjects = (List<HelperModels.IDbObject>)JsonConvert.DeserializeObject(serialisedDb);
            
             return dbObjects;
        }
        public void EditEntry(int id, HelperModels.IDbObject editedObject, out string errorMessage)
        {
            errorMessage = "Edit Succses";

            try
            {
                var objToEdit = _db.FirstOrDefault(x => x.Id == id);
                objToEdit = editedObject;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                throw;
            }
            
        }
        public void AddToDb(HelperModels.IDbObject obj, out string errorMessage, out int scoreId)
        {
           
            try
            {
                var currentHighestId = _db.Where(x => x.Id != 0)
                    .Max(x => x.Id);
                scoreId = ++currentHighestId;
                obj.Id = scoreId;
                _db.Add(obj);
                errorMessage = "Added entry Succsesfully";
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                throw;
            }
            
        }
        public void DeleteEntry(int id, out string errorMessage)
        {
            errorMessage = "Deleted entry Succsesfully";
            try
            {
                var objToRemove = _db.FirstOrDefault(x => x.Id == id);
                _db.Remove(objToRemove);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                throw;
            }         
        }
        public abstract void ReadDbFromFile();

       
        public void SaveDb()
        {          
            string serialisedDb = JsonConvert.SerializeObject(_db, Formatting.Indented);
            string path = GetDbPath();
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, _db);
            }
        }
        protected string GetDbPath()
        {
            string type = GetTypeOfDb(this);
            var solutionDir = GetSolutionDirectory();
            if (string.IsNullOrWhiteSpace(dbPath))
            {
                dbPath = Path.Combine(solutionDir.FullName, $".\\MockDbs\\{type}.txt");
            }
            return dbPath;
        }
        protected DirectoryInfo GetSolutionDirectory(string currentPath = null)
        {
            var directory = new DirectoryInfo(
                currentPath ?? Directory.GetCurrentDirectory());
            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory;
        }
        protected string GetTypeOfDb(MockDb mockDb)
        {
            string dbType;
            switch (mockDb)
            {
                case MockUserDb x when mockDb is MockUserDb:
                    dbType = "UserDb";
                    break;
                case MockScoresDb x when mockDb is MockScoresDb:
                    dbType = "ScoreDb";
                    break;
                default:
                    dbType = "Undefiend";
                    break;
            }
            return dbType;
        }

    }
}
