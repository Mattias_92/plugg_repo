﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockDB
{
    public class MockScoresDb : MockDb
    {
        public MockScoresDb()
        {

        }

        public void PopulateDbWithPreMadeScores()
        {
            _db.AddRange(PreMadeData.GetUserScores());
        }
        public override void ReadDbFromFile()
        {
            string path = GetDbPath();
            using (StreamReader file = File.OpenText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<UserManager.UserScore> users = (List<UserManager.UserScore>)serializer.Deserialize(file, typeof(List<UserManager.UserScore>));
                _db.AddRange(users);
            }
        }
    }
}
