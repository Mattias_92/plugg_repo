﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockDB
{
    public static class PreMadeData
    {
        private static List<UserManager.User> users = new List<UserManager.User>();
        private static List<UserManager.UserScore> userScores = new List<UserManager.UserScore>();
        public static List<UserManager.User> GetUsers()
        {
            testUser1.Scores.AddRange(test1Scores);
            users.Add(testUser1);
            janeDoe.Scores.AddRange(janeDoeScores);
            users.Add(janeDoe);
            johnDoe.Scores.AddRange(johnDoeScores);
            users.Add(johnDoe);
            return users;
        }

        public static List<UserManager.UserScore> GetUserScores()
        {
            
            userScores.AddRange(test1Scores);
            userScores.AddRange(janeDoeScores);
            userScores.AddRange(johnDoeScores);
            return userScores;
        }
        // premade users
        private static UserManager.User testUser1 = new UserManager.User()
        {
            Id = 1,
            UserEmail = "test1@gmail.com",
            HashedPassword = "test",
            FirstName = "test1First",
            LastName = "test1Last",
            BirtDate = DateTime.Parse("2000 01 01"),
            Scores = new List<UserManager.UserScore>()
        };
        private static UserManager.User janeDoe = new UserManager.User()
        {
            Id = 2,
            UserEmail = "janeDoe@gmail.com",
            HashedPassword = "test",
            FirstName = "Jane",
            LastName = "Doe",
            BirtDate = DateTime.Parse("1999 01 01"),
            Scores = new List<UserManager.UserScore>()
        };
        private static UserManager.User johnDoe = new UserManager.User()
        {
            Id = 3,
            UserEmail = "JohnDoe@gmail.com",
            HashedPassword = "test",
            FirstName = "John",
            LastName = "Doe",
            BirtDate = DateTime.Parse("1998 01 01"),
            Scores = new List<UserManager.UserScore>()
        };

        // pre made scores
        private static List<UserManager.UserScore> test1Scores = new List<UserManager.UserScore>()
        {
            new UserManager.UserScore()
            {
                Id = 1,
                UserId = 1,
                //User = testUser1,
                Clicks = 25,
                ElapsedTime = TimeSpan.Parse("04:12"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 2,
                UserId = 1,
                //User = testUser1,
                Clicks = 47,
                ElapsedTime = TimeSpan.Parse("02:12"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 3,
                UserId = 1,
                //User = testUser1,
                Clicks = 14,
                ElapsedTime = TimeSpan.Parse("03:12"),
                Game = UserManager.Games.TicTack
            }
        };

        private static List<UserManager.UserScore> janeDoeScores = new List<UserManager.UserScore>()
        {
            new UserManager.UserScore()
            {
                Id = 4,
                UserId = 2,
                ///*User = janeDoe*/,
                Clicks = 25,
                ElapsedTime = TimeSpan.Parse("04:08"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 5,
                UserId = 2,
                //User = janeDoe,
                Clicks = 19,
                ElapsedTime = TimeSpan.Parse("02:43"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 6,
                UserId = 2,
                //User = janeDoe,
                Clicks = 11,
                ElapsedTime = TimeSpan.Parse("03:12"),
                Game = UserManager.Games.TicTack
            }
        };
        private static List<UserManager.UserScore> johnDoeScores = new List<UserManager.UserScore>()
        {
            new UserManager.UserScore()
            {
                Id = 7,
                UserId = 3,
                //User = johnDoe,
                Clicks = 25,
                ElapsedTime = TimeSpan.Parse("04:08"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 8,
                UserId = 3,
                //User = johnDoe,
                Clicks = 20,
                ElapsedTime = TimeSpan.Parse("02:43"),
                Game = UserManager.Games.Memmory
            },
            new UserManager.UserScore()
            {
                Id = 9,
                UserId = 3,
                //User = johnDoe,
                Clicks = 11,
                ElapsedTime = TimeSpan.Parse("04:12"),
                Game = UserManager.Games.TicTack
            }
        };

    }
}
