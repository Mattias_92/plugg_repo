﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestPlayGround
{
    public partial class Form1 : Form
    {
        bool goleft = false;
        bool goright = false;
        bool jumping = false;

        int jumpSpeed = 10;
        int force = 8;
        int score = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = true;
            }
            if (e.KeyCode == Keys.Space && !jumping)
            {
                jumping = true;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = false;
            }

            if (e.KeyCode == Keys.Right)
            {
                goright = false;
            }
            if (jumping)
            {
                jumping = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // fake gravity will push player down
            player.Top += jumpSpeed;
            // check if jumping
            if (jumping && force < 0)
            {
                jumping = false;
            }
            // if true push the player 5px to left
            if (goleft)
            {
                player.Left -= 5;
            }
            // if true push the player 5px to rigt
            if (goright)
            {
                player.Left += 5;
            }
            // by setting the js to minus we will thrust the player upwawrds and decrease the force by one to limit the jump so the player
            // dont fly 
            if (jumping)
            {
                jumpSpeed = -12;
                force -= 1;
            }
            //otherwise the jump is set to twelve
            else
            {
                jumpSpeed = 12;
            }
            // loop through the form and check if a controill is a PB and have the tag "tag_Platform"
            // this will let the player jump onto platforms
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag.ToString() == "tag_Platform")
                {
                    if (player.Bounds.IntersectsWith(x.Bounds) && !jumping)
                    {
                        force = 8;
                        player.Top = x.Top - player.Height;
                    }
                }
            }
        }
    }
}
