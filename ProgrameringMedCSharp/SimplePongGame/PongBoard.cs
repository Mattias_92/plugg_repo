﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePongGame
{
    public partial class frmPongBoard : Form
    {
        MockDB.MockUserDb userDb;
        MockDB.MockScoresDb scoresDb;
        UserManager.User _user;
        Random _random;

        bool gameIsOver = false;
        bool goleft = false;
        bool goright = false;

        int seconds = 0;
        int speedLeft = 4;
        int speedTop = 4;
        int score = 0;
        public frmPongBoard()
        {
            _random = new Random();
            InitializeComponent();
            SetFormProperties();
            SetUpDataBases();
            SetGameOverMessageToMiddle();
            pb_Racket.Top = pnl_PongTable.Bottom - 100;
            _user = (UserManager.User)userDb.GetEntity(1);

        }

        private void PongBoard_Resize(object sender, EventArgs e)
        {
            pnl_PongTable.Width = this.Width - 40;
            pnl_PongTable.Height = this.Height - 115;
        }
        private void SetFormProperties()
        {
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
        }
        private void SetUpDataBases()
        {
            userDb = new MockDB.MockUserDb();
            scoresDb = new MockDB.MockScoresDb();

            userDb.PopulateUserDbWithPreMades();
            scoresDb.PopulateDbWithPreMadeScores();
        }
        private void SetGameOverMessageToMiddle()
        {
            lbl_GameOver.Left = (pnl_PongTable.Width / 2) - (lbl_GameOver.Width / 2);
            lbl_GameOver.Top = (pnl_PongTable.Height / 2) - (lbl_GameOver.Height / 2);
        }

        private void timer_ElapsedTime_Tick(object sender, EventArgs e)
        {
            seconds++;
            lbl_ElapsedTime.Text = TimeSpan.FromSeconds(seconds).ToString("mm\\:ss");
        }

        private void frmPongBoard_Load(object sender, EventArgs e)
        {
            timer_ElapsedTime.Tick += new EventHandler(timer_ElapsedTime_Tick);
            timer_ElapsedTime.Interval = 1000;
            timer_ElapsedTime.Enabled = true;
        }

        private void timer_GameEngine_Tick(object sender, EventArgs e)
        {
            pb_Ball.Left += speedLeft;
            pb_Ball.Top += speedTop;

            if (pb_Ball.Bottom >= pb_Racket.Top && 
                pb_Ball.Bottom <= pb_Racket.Bottom &&
                pb_Ball.Left >= pb_Racket.Left &&
                pb_Ball.Right <= pb_Racket.Right)
            {
                speedTop += 1;
                speedLeft += 1;
                speedTop = -speedTop;
                score++;
                lbl_ScoreDispaly.Text = score.ToString();

                pnl_PongTable.BackColor = Color.FromArgb(_random.Next(150, 255), _random.Next(150, 255), _random.Next(150, 255));

            }

            if (pb_Ball.Left <= pnl_PongTable.Left)
            {
                speedLeft = -speedLeft;
            }
            if (pb_Ball.Right >= pnl_PongTable.Right)
            {
                speedLeft = -speedLeft;
            }
            if (pb_Ball.Top <= pnl_PongTable.Top)
            {
                speedTop = -speedTop;
            }
            if (pb_Ball.Bottom >= pnl_PongTable.Bottom)
            {
                timer_ElapsedTime.Stop();
                gameIsOver = true;
                timer_GameEngine.Enabled = false;
                lbl_GameOver.Text = $"Game over!\r\n You got {lbl_ScoreDispaly.Text} points. \r\n You played for {lbl_ElapsedTime.Text}. \r\n Press E to exit.";
                lbl_GameOver.Visible = true;
                FinishGame();
            }
        }

        private void FinishGame()
        {
            var score = UserManager.ScoreFactory.CreateScore(_user.Id, lbl_ScoreDispaly.Text, lbl_ElapsedTime.Text);
            scoresDb.AddToDb(score, out string er, out int scoreId);
            _user.Scores.Add(score);
            userDb.SaveDb();
            scoresDb.SaveDb();
        }

        private void frmPongBoard_KeyDown(object sender, KeyEventArgs e)
        {
            if (gameIsOver)
            {
                if (e.KeyCode == Keys.E)
                {
                    this.Close();
                }
            }
            // handles controll over racket
            if (e.KeyCode == Keys.Left)
            {
                goleft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goright = true;
            }
        }


        private void frmPongBoard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goleft = false;
            }

            if (e.KeyCode == Keys.Right)
            {
                goright = false;
            }
        }

        private void timer_Movement_Tick(object sender, EventArgs e)
        {

            // if true push the player 5px to left
            if (goleft)
            {
                pb_Racket.Left -= 15;
            }
            // if true push the player 5px to rigt
            if (goright)
            {
                pb_Racket.Left += 15;
            }
        }
    }
}
