﻿
namespace SimplePongGame
{
    partial class frmPongBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnl_PongTable = new System.Windows.Forms.Panel();
            this.lbl_GameOver = new System.Windows.Forms.Label();
            this.pb_Ball = new System.Windows.Forms.PictureBox();
            this.pb_Racket = new System.Windows.Forms.PictureBox();
            this.lbl_Score = new System.Windows.Forms.Label();
            this.lbl_ScoreDispaly = new System.Windows.Forms.Label();
            this.lbl_Time = new System.Windows.Forms.Label();
            this.lbl_ElapsedTime = new System.Windows.Forms.Label();
            this.timer_GameEngine = new System.Windows.Forms.Timer(this.components);
            this.timer_ElapsedTime = new System.Windows.Forms.Timer(this.components);
            this.timer_Movement = new System.Windows.Forms.Timer(this.components);
            this.pnl_PongTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Ball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Racket)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_PongTable
            // 
            this.pnl_PongTable.Controls.Add(this.lbl_GameOver);
            this.pnl_PongTable.Controls.Add(this.pb_Ball);
            this.pnl_PongTable.Controls.Add(this.pb_Racket);
            this.pnl_PongTable.Location = new System.Drawing.Point(12, 63);
            this.pnl_PongTable.Name = "pnl_PongTable";
            this.pnl_PongTable.Size = new System.Drawing.Size(830, 420);
            this.pnl_PongTable.TabIndex = 0;
            // 
            // lbl_GameOver
            // 
            this.lbl_GameOver.BackColor = System.Drawing.Color.DarkRed;
            this.lbl_GameOver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_GameOver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_GameOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_GameOver.Location = new System.Drawing.Point(0, 0);
            this.lbl_GameOver.Name = "lbl_GameOver";
            this.lbl_GameOver.Size = new System.Drawing.Size(830, 420);
            this.lbl_GameOver.TabIndex = 2;
            this.lbl_GameOver.Text = "Game Over!\r\n";
            this.lbl_GameOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_GameOver.Visible = false;
            // 
            // pb_Ball
            // 
            this.pb_Ball.BackColor = System.Drawing.Color.Black;
            this.pb_Ball.Location = new System.Drawing.Point(36, 120);
            this.pb_Ball.Name = "pb_Ball";
            this.pb_Ball.Size = new System.Drawing.Size(30, 30);
            this.pb_Ball.TabIndex = 1;
            this.pb_Ball.TabStop = false;
            // 
            // pb_Racket
            // 
            this.pb_Racket.BackColor = System.Drawing.Color.Black;
            this.pb_Racket.Location = new System.Drawing.Point(4, 397);
            this.pb_Racket.Name = "pb_Racket";
            this.pb_Racket.Size = new System.Drawing.Size(200, 20);
            this.pb_Racket.TabIndex = 0;
            this.pb_Racket.TabStop = false;
            // 
            // lbl_Score
            // 
            this.lbl_Score.AutoSize = true;
            this.lbl_Score.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Score.Location = new System.Drawing.Point(12, 9);
            this.lbl_Score.Name = "lbl_Score";
            this.lbl_Score.Size = new System.Drawing.Size(55, 20);
            this.lbl_Score.TabIndex = 1;
            this.lbl_Score.Text = "Score:";
            // 
            // lbl_ScoreDispaly
            // 
            this.lbl_ScoreDispaly.AutoSize = true;
            this.lbl_ScoreDispaly.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ScoreDispaly.Location = new System.Drawing.Point(73, 9);
            this.lbl_ScoreDispaly.Name = "lbl_ScoreDispaly";
            this.lbl_ScoreDispaly.Size = new System.Drawing.Size(18, 20);
            this.lbl_ScoreDispaly.TabIndex = 4;
            this.lbl_ScoreDispaly.Text = "0";
            // 
            // lbl_Time
            // 
            this.lbl_Time.AutoSize = true;
            this.lbl_Time.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Time.Location = new System.Drawing.Point(142, 9);
            this.lbl_Time.Name = "lbl_Time";
            this.lbl_Time.Size = new System.Drawing.Size(47, 20);
            this.lbl_Time.TabIndex = 5;
            this.lbl_Time.Text = "Time:";
            // 
            // lbl_ElapsedTime
            // 
            this.lbl_ElapsedTime.AutoSize = true;
            this.lbl_ElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ElapsedTime.Location = new System.Drawing.Point(195, 9);
            this.lbl_ElapsedTime.Name = "lbl_ElapsedTime";
            this.lbl_ElapsedTime.Size = new System.Drawing.Size(0, 20);
            this.lbl_ElapsedTime.TabIndex = 6;
            // 
            // timer_GameEngine
            // 
            this.timer_GameEngine.Enabled = true;
            this.timer_GameEngine.Interval = 10;
            this.timer_GameEngine.Tick += new System.EventHandler(this.timer_GameEngine_Tick);
            // 
            // timer_ElapsedTime
            // 
            this.timer_ElapsedTime.Tick += new System.EventHandler(this.timer_ElapsedTime_Tick);
            // 
            // timer_Movement
            // 
            this.timer_Movement.Enabled = true;
            this.timer_Movement.Interval = 20;
            this.timer_Movement.Tick += new System.EventHandler(this.timer_Movement_Tick);
            // 
            // frmPongBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 496);
            this.Controls.Add(this.lbl_ElapsedTime);
            this.Controls.Add(this.lbl_Time);
            this.Controls.Add(this.lbl_ScoreDispaly);
            this.Controls.Add(this.lbl_Score);
            this.Controls.Add(this.pnl_PongTable);
            this.MaximizeBox = false;
            this.Name = "frmPongBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pong game";
            this.Load += new System.EventHandler(this.frmPongBoard_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPongBoard_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmPongBoard_KeyUp);
            this.Resize += new System.EventHandler(this.PongBoard_Resize);
            this.pnl_PongTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Ball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Racket)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnl_PongTable;
        private System.Windows.Forms.Label lbl_GameOver;
        private System.Windows.Forms.PictureBox pb_Ball;
        private System.Windows.Forms.PictureBox pb_Racket;
        private System.Windows.Forms.Label lbl_Score;
        private System.Windows.Forms.Label lbl_ScoreDispaly;
        private System.Windows.Forms.Label lbl_Time;
        private System.Windows.Forms.Label lbl_ElapsedTime;
        private System.Windows.Forms.Timer timer_GameEngine;
        private System.Windows.Forms.Timer timer_ElapsedTime;
        private System.Windows.Forms.Timer timer_Movement;
    }
}