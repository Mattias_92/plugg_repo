﻿using AdressbookOne.Data;
using AdressbookOne.FormHandlers;
using AdressbookOne.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdressbookOne
{
    public partial class frmCreateAddress : Form
    {
        frmAddressBook _addressBook;       
        Adress _addressToEdit;
        private readonly FileHandler<Adress> _addressEntities;
        bool isEditMode = false;
        public frmCreateAddress(frmAddressBook addressBook, FileHandler<Adress> addressEntities)
        {
            InitializeComponent();
            _addressBook = addressBook;
            _addressEntities = addressEntities;
        }
        public frmCreateAddress(frmAddressBook addressBook, Adress addressToEdit, FileHandler<Adress> addressEntities)
        {
            InitializeComponent();
            _addressBook = addressBook;
            _addressToEdit = addressToEdit;
            _addressEntities = addressEntities;
            isEditMode = true;
        }

        private void btn_CreateContact_Click(object sender, EventArgs e)
        {
            if (CheckForm())
            {
                if (isEditMode)
                {
                    EditAddress();
                }
                else
                {
                    Adress newAddress = new Adress();
                    newAddress.FirstName = txt_FirstName.Text;
                    newAddress.LastName = txt_LastName.Text;
                    newAddress.Email = txt_Email.Text;
                    newAddress.StreetAdress = txt_StreetAddress.Text;
                    newAddress.PostArea = txt_PostArea.Text;
                    newAddress.ZipCode = int.Parse(txt_ZipCode.Text);
                    newAddress.PhoneNumber = txt_PhoneNumber.Text;
                    _addressEntities.Add(newAddress);
                }
                _addressEntities.Save();
                _addressBook.PopulateListWithAddresses();
                MessageBox.Show("Saved new contact");
                ResetTexBoxes();
            }
        }

        private void ResetTexBoxes()
        {
            foreach (var control in this.Controls)
            {
                var textBox = control as TextBox;
                if (textBox != null)
                {
                    textBox.ResetText();
                }
            }
        }

        private void EditAddress()
        {
            var updatedAddress = _addressEntities.Find(x => x.Id == _addressToEdit.Id).First();
            updatedAddress.FirstName = txt_FirstName.Text;
            updatedAddress.LastName = txt_LastName.Text;
            updatedAddress.Email = txt_Email.Text;
            updatedAddress.StreetAdress = txt_StreetAddress.Text;
            updatedAddress.PostArea = txt_PostArea.Text;
            updatedAddress.ZipCode = int.Parse(txt_ZipCode.Text);
            updatedAddress.PhoneNumber = txt_PhoneNumber.Text;
        }

        private void frmCreateAddress_Load(object sender, EventArgs e)
        {
            if (isEditMode)
            {
                PopulateTextBoxes();
                lbl_TitleHeader.Text = "Edit Address";
            }
        }
        private bool CheckForm()
        {
            bool formIsOk = true;
            int errorIndex;
            if (!FormValidator.IsNameInputedCorrectly(out errorIndex, txt_FirstName.Text, txt_LastName.Text))
            {
                LabelHandler.ShowNameErrorMessage(lbl_ErrorFirstName, lbl_ErrorLastName, errorIndex);
                formIsOk = false;
            }
            else
            {
                lbl_ErrorFirstName.Visible = false;
                lbl_ErrorLastName.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txt_StreetAddress.Text))
            {
                LabelHandler.AssignErrorMessage(lbl_ErrorStreetAdd, Constants.Constants.StreetAddErrorMsg);
                formIsOk = false;
            }
            else if (lbl_ErrorStreetAdd.Visible == true)
            {
                lbl_ErrorStreetAdd.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(txt_PostArea.Text))
            {
                LabelHandler.AssignErrorMessage(lbl_ErrorPostArea, Constants.Constants.StreetAddErrorMsg);
                formIsOk = false;
            }
            else if (lbl_ErrorPostArea.Visible == true)
            {
                lbl_ErrorPostArea.Visible = false;
            }
            var test = _addressEntities.Find(x => x.Email == txt_Email.Text);
            if (!FormValidator.IsEmailCorrect(txt_Email.Text) || (test.FirstOrDefault(x => x.Email == txt_Email.Text) != null && isEditMode == false))
            {
                LabelHandler.AssignErrorMessage(lbl_ErrorEmail, Constants.Constants.EmailErrorMsg);
                formIsOk = false;
            }
            else if (lbl_ErrorEmail.Visible == true)
            {
                lbl_ErrorEmail.Visible = false;
            }
            if (!FormValidator.IsPhoneCorrect(txt_PhoneNumber.Text))
            {
                LabelHandler.AssignErrorMessage(lbl_ErrorPhoneN, Constants.Constants.PhoneErrorMsg);
                formIsOk = false;
            }
            else if (lbl_ErrorPhoneN.Visible == true)
            {
                lbl_ErrorPhoneN.Visible = false;
            }
            if (!FormValidator.IsZipeCodeCorrect(txt_ZipCode.Text))
            {
                LabelHandler.AssignErrorMessage(lbl_ErrorZipCode, Constants.Constants.ZipCodeErrorMsg);
                formIsOk = false;
            }
            else if (lbl_ErrorZipCode.Visible == true)
            {
                lbl_ErrorZipCode.Visible = false;
            }
            return formIsOk;
        }
        private void PopulateTextBoxes()
        {
            txt_FirstName.Text = _addressToEdit.FirstName;
            txt_LastName.Text = _addressToEdit.LastName;
            txt_StreetAddress.Text = _addressToEdit.StreetAdress;
            txt_PostArea.Text = _addressToEdit.PostArea;
            txt_Email.Text = _addressToEdit.Email;
            txt_PhoneNumber.Text = _addressToEdit.PhoneNumber.ToString();
            txt_ZipCode.Text = _addressToEdit.ZipCode.ToString();
        }

        private void frmCreateAddress_FormClosing(object sender, FormClosingEventArgs e)
        {
            _addressBook.PopulateListWithAddresses();
            _addressBook.Show();          
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
