﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdressbookOne.FormHandlers
{
    public class LabelHandler
    {
        internal static void ShowNameErrorMessage(Label lbl_FirstNameError, Label lbl_LastNameError, int errorIndex)
        {
            switch (errorIndex)
            {
                case 0:
                    AssignErrorMessage(lbl_FirstNameError, Constants.Constants.NameErrorMsg);
                    break;
                case -1:
                    AssignErrorMessage(lbl_LastNameError, Constants.Constants.NameErrorMsg);
                    break;
                case -2:
                    AssignErrorMessage(lbl_FirstNameError, Constants.Constants.NameErrorMsg);
                    AssignErrorMessage(lbl_LastNameError, Constants.Constants.NameErrorMsg);
                    break;
                default:
                    break;
            }
        }
        public static void AssignErrorMessage(Label lbl_error, string message)
        {
            lbl_error.Visible = true;
            lbl_error.Text = message;
            lbl_error.ForeColor = Color.Red;
        }
    }
}
