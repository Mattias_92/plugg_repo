﻿using AdressbookOne.Data;
using AdressbookOne.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdressbookOne
{
    public partial class frmAddressBook : Form
    {
        private readonly FileHandler<Adress> _addressEntities;
        private readonly string _userName;
        private readonly string _fileName;
        private List<Adress> _addresses;
        public frmAddressBook(string userName)
        {
            InitializeComponent();
            _userName = userName;
            _fileName = userName + "AddressBook.txt";
            _addressEntities = new FileHandler<Adress>(_fileName);
            
        }

        private void btn_CreateContact_Click(object sender, EventArgs e)
        {
            try
            {

                if (Application.OpenForms.OfType<frmCreateAddress>().Count() == 1)
                {
                    Application.OpenForms.OfType<frmCreateAddress>().First().Close();
                }
                else
                {
                    frmCreateAddress createAddress = new frmCreateAddress(this, _addressEntities);
                    this.Hide();
                    createAddress.Show();
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Invalid action: " + er);
            }
        }

        private void btn_DeleteContact_Click(object sender, EventArgs e)
        {
            var addressToDelete = (Adress)lst_Contacts.SelectedItem;
            _addressEntities.Remove(addressToDelete);
            _addressEntities.Save();
            PopulateListWithAddresses();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_EditContact_Click(object sender, EventArgs e)
        {
            try
            {
                if (lst_Contacts.SelectedItem != null)
                {
                    var addressToEdit = (Adress)lst_Contacts.SelectedItem;
                    if (Application.OpenForms.OfType<frmCreateAddress>().Count() == 1)
                    {
                        Application.OpenForms.OfType<frmCreateAddress>().First().Close();
                    }
                    else
                    {
                        frmCreateAddress createAddress = new frmCreateAddress(this, addressToEdit, _addressEntities);
                        this.Hide();
                        createAddress.Show();
                    }
                }               
            }
            catch (Exception er)
            {
                MessageBox.Show("Invalid action: " + er);
            }
        }
        internal void PopulateListWithAddresses()
        {
            _addresses = _addressEntities.GetAll().ToList();
            
            lst_Contacts.DisplayMember = "FullName";
            lst_Contacts.ValueMember = "Id";
            lst_Contacts.DataSource = _addresses;
        }
        internal void PopulateListWithAddresses(List<Adress> addresses, string displayMember = "FullName")
        {           
            lst_Contacts.DisplayMember = displayMember;
            lst_Contacts.ValueMember = "Id";
            lst_Contacts.DataSource = addresses;
        }
        private void PopulateComboBoxes(ComboBox comboBox)
        {
            var addressProperties = typeof(Adress).GetProperties();
            Dictionary<string, PropertyInfo> keyValuePairs = new Dictionary<string, PropertyInfo>()
            {
                { "Name",addressProperties[3]},
                { "Street Address",addressProperties[4]},
                { "Post Office",addressProperties[5]},
                { "Email",addressProperties[6]},
                { "Zip Code",addressProperties[7]},
                { "Phone Number",addressProperties[8]}

            };

            comboBox.DisplayMember = "Key";
            comboBox.ValueMember = "Value";
            comboBox.DataSource = keyValuePairs.ToList();
        }

        private void frmAddressBook_Load(object sender, EventArgs e)
        {
            PopulateListWithAddresses();
            PopulateComboBoxes(cmb_SearchTerms);
            PopulateComboBoxes(cmb_DisplayBy);
        }

        private void lst_Contacts_SelectedIndexChanged(object sender, EventArgs e)
        {

            List<Adress> selectedItem = new List<Adress>();
            selectedItem.Add((Adress)lst_Contacts.SelectedItem);
            dgv_ContactInfo.DataSource = selectedItem;
            dgv_ContactInfo.Columns[0].Visible = false;
            dgv_ContactInfo.Columns[3].Visible = false;
            dgv_ContactInfo.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_ContactInfo.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_ContactInfo.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }      

        private void txt_SearchTerm_TextChanged(object sender, EventArgs e)
        {
            List<Adress> result = new List<Adress>();
            KeyValuePair<string, PropertyInfo> searhCategory = (KeyValuePair<string, PropertyInfo>)cmb_SearchTerms.SelectedItem;
            string searchTerm = txt_SearchTerm.Text;
            if (cmb_DisplayBy.Items.Count != 0)
            {
                if (string.IsNullOrWhiteSpace(searchTerm))
                {
                    cmb_DisplayBy.SelectedIndex = 0;
                    PopulateListWithAddresses(_addresses);
                }
                else
                {
                    switch (searhCategory.Key)
                    {
                        case "Name":
                            result = _addressEntities.Find(x => x.FullName.ToLower().Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result);
                            cmb_DisplayBy.SelectedIndex = 0;
                            break;
                        case "Street Address":
                            result = _addressEntities.Find(x => x.StreetAdress.ToLower().Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result, searhCategory.Value.Name);
                            cmb_DisplayBy.SelectedIndex = 1;
                            break;
                        case "Post Office":
                            result = _addressEntities.Find(x => x.PostArea.Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result, searhCategory.Value.Name);
                            cmb_DisplayBy.SelectedIndex = 2;
                            break;
                        case "Email":
                            result = _addressEntities.Find(x => x.Email.ToLower().Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result, searhCategory.Value.Name);
                            cmb_DisplayBy.SelectedIndex = 3;
                            break;
                        case "Zip Code":
                            result = _addressEntities.Find(x => x.ZipCode.ToString().ToLower().Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result, searhCategory.Value.Name);
                            cmb_DisplayBy.SelectedIndex = 4;
                            break;
                        case "Phone Number":
                            result = _addressEntities.Find(x => x.PhoneNumber.ToLower().Contains(searchTerm.ToLower())).ToList();
                            PopulateListWithAddresses(result, searhCategory.Value.Name);
                            cmb_DisplayBy.SelectedIndex = 5;
                            break;
                        default:
                            PopulateListWithAddresses(_addresses);
                            cmb_DisplayBy.SelectedIndex = 0;
                            cmb_SearchTerms.SelectedIndex = 0;
                            break;
                    }
                }           
            }         
        }

        private void cmb_DisplayBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            KeyValuePair<string, PropertyInfo> displayValue = (KeyValuePair<string, PropertyInfo>)cmb_DisplayBy.SelectedItem;
            lst_Contacts.DisplayMember = displayValue.Value.Name;
        }
    }
}
