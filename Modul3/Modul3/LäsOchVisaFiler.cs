﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul2
{
    public partial class LäsOchVisaFiler : Form
    {
        public LäsOchVisaFiler()
        {
            InitializeComponent();
        }

        private void btn_ShowFiles_Click(object sender, EventArgs e)
        {

            string path = txt_Filkatlog.Text;
            if (string.IsNullOrWhiteSpace(path) || !Directory.Exists(path))
            {
                path = BrowseFolder(path);
            }
            try
            {
                lst_Files.Items.Clear();
                CheckAndAppendBackSlashToPath(ref path);
                txt_Filkatlog.Text = path;
                string[] filesInCatalog = Directory.GetFiles(path);
                AddItemsToList(filesInCatalog);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong.");
            }
            

        }

        private void AddItemsToList(string[] filesInCatalog)
        {
            foreach (var file in filesInCatalog)
            {
                lst_Files.Items.Add(Path.GetFileName(file));
            }
        }

        private string BrowseFolder(string path)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult dialogResult = folderBrowserDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                path = folderBrowserDialog1.SelectedPath;
                Environment.SpecialFolder specialFolder = folderBrowserDialog1.RootFolder;
            }

            return path;
        }

        private void btn_Open_Click(object sender, EventArgs e)
        {
            try
            {
                if (lst_Files.Items.Count > 0)
                {
                    var selectedItem = lst_Files.SelectedItem.ToString();
                    string path = txt_Filkatlog.Text + selectedItem;

                    var content = File.ReadAllText(path);
                    txt_Content.Text = content;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong");
            }
            
        }

        private void CheckAndAppendBackSlashToPath(ref string path)
        {
            if (path[path.Length - 1] != '\\')
            {
                path += @"\\";
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txt_Content.Text))
                {
                    var selectedItem = lst_Files.SelectedItem.ToString();
                    string path = txt_Filkatlog.Text + selectedItem;
                    File.WriteAllText(path, txt_Content.Text);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong");
            }
        }

        private void btn_CreateNewTxtFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txt_Filkatlog.Text))
                {
                    var newFileName = txt_CreateNewFile.Text;
                    newFileName += ".txt";
                    string path = txt_Filkatlog.Text + newFileName;
                    string content;
                    if (string.IsNullOrWhiteSpace(txt_Content.Text))
                    {
                        content = "";
                    }
                    else
                    {
                        content = txt_Content.Text;
                    }
                    File.WriteAllText(path, content);
                    lst_Files.Items.Clear();
                    txt_Content.Clear();
                    AddItemsToList(Directory.GetFiles(txt_Filkatlog.Text));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Choose a folder to add new file");
            }
        }
    }
}
