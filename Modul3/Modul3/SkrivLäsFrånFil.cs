﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul2
{
    public partial class SkrivLäsFrånFil : Form
    {
        string path = @"C:\Users\Mattinor\source\repos\Modul3\Modul3\Files\message.txt";
        public SkrivLäsFrånFil()
        {
            InitializeComponent();
        }

        private void btn_Write_Click(object sender, EventArgs e)
        {
            
            string message = "";
            if (string.IsNullOrWhiteSpace(txt_Skriv.Text))
            {
                message = "I am a C# hacker";
            }
            else
            {
                message = txt_Skriv.Text;
            }
            File.WriteAllText(path, message);
        }

        private void btn_Läs_Click(object sender, EventArgs e)
        {
            string message = File.ReadAllText(path);
            txt_Läs.Text = message;


        }
    }
}
