﻿
namespace Modul2
{
    partial class SkrivLäsFrånFil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Skriv = new System.Windows.Forms.TextBox();
            this.btn_Write = new System.Windows.Forms.Button();
            this.txt_Läs = new System.Windows.Forms.TextBox();
            this.btn_Läs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_Skriv
            // 
            this.txt_Skriv.Location = new System.Drawing.Point(107, 101);
            this.txt_Skriv.Multiline = true;
            this.txt_Skriv.Name = "txt_Skriv";
            this.txt_Skriv.Size = new System.Drawing.Size(320, 38);
            this.txt_Skriv.TabIndex = 0;
            // 
            // btn_Write
            // 
            this.btn_Write.Location = new System.Drawing.Point(107, 179);
            this.btn_Write.Name = "btn_Write";
            this.btn_Write.Size = new System.Drawing.Size(320, 90);
            this.btn_Write.TabIndex = 1;
            this.btn_Write.Text = "Skriv";
            this.btn_Write.UseVisualStyleBackColor = true;
            this.btn_Write.Click += new System.EventHandler(this.btn_Write_Click);
            // 
            // txt_Läs
            // 
            this.txt_Läs.Location = new System.Drawing.Point(479, 101);
            this.txt_Läs.Multiline = true;
            this.txt_Läs.Name = "txt_Läs";
            this.txt_Läs.Size = new System.Drawing.Size(320, 38);
            this.txt_Läs.TabIndex = 2;
            // 
            // btn_Läs
            // 
            this.btn_Läs.Location = new System.Drawing.Point(479, 179);
            this.btn_Läs.Name = "btn_Läs";
            this.btn_Läs.Size = new System.Drawing.Size(320, 90);
            this.btn_Läs.TabIndex = 3;
            this.btn_Läs.Text = "Läs";
            this.btn_Läs.UseVisualStyleBackColor = true;
            this.btn_Läs.Click += new System.EventHandler(this.btn_Läs_Click);
            // 
            // SkrivLäsFrånFil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 289);
            this.Controls.Add(this.btn_Läs);
            this.Controls.Add(this.txt_Läs);
            this.Controls.Add(this.btn_Write);
            this.Controls.Add(this.txt_Skriv);
            this.Name = "SkrivLäsFrånFil";
            this.Text = "SkrivLäsFrånFil";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Skriv;
        private System.Windows.Forms.Button btn_Write;
        private System.Windows.Forms.TextBox txt_Läs;
        private System.Windows.Forms.Button btn_Läs;
    }
}