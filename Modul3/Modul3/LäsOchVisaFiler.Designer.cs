﻿
namespace Modul2
{
    partial class LäsOchVisaFiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Content = new System.Windows.Forms.TextBox();
            this.txt_Filkatlog = new System.Windows.Forms.MaskedTextBox();
            this.lbl_FilCatlog = new System.Windows.Forms.Label();
            this.btn_ShowFiles = new System.Windows.Forms.Button();
            this.lst_Files = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btn_Open = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.txt_CreateNewFile = new System.Windows.Forms.MaskedTextBox();
            this.btn_CreateNewTxtFile = new System.Windows.Forms.Button();
            this.lbl_CreateNew = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_Content
            // 
            this.txt_Content.Location = new System.Drawing.Point(370, 36);
            this.txt_Content.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.txt_Content.Multiline = true;
            this.txt_Content.Name = "txt_Content";
            this.txt_Content.Size = new System.Drawing.Size(134, 169);
            this.txt_Content.TabIndex = 0;
            // 
            // txt_Filkatlog
            // 
            this.txt_Filkatlog.Location = new System.Drawing.Point(50, 32);
            this.txt_Filkatlog.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.txt_Filkatlog.Name = "txt_Filkatlog";
            this.txt_Filkatlog.Size = new System.Drawing.Size(132, 20);
            this.txt_Filkatlog.TabIndex = 1;
            // 
            // lbl_FilCatlog
            // 
            this.lbl_FilCatlog.AutoSize = true;
            this.lbl_FilCatlog.Location = new System.Drawing.Point(47, 18);
            this.lbl_FilCatlog.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lbl_FilCatlog.Name = "lbl_FilCatlog";
            this.lbl_FilCatlog.Size = new System.Drawing.Size(52, 13);
            this.lbl_FilCatlog.TabIndex = 2;
            this.lbl_FilCatlog.Text = "Filkatalog";
            // 
            // btn_ShowFiles
            // 
            this.btn_ShowFiles.Location = new System.Drawing.Point(184, 29);
            this.btn_ShowFiles.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.btn_ShowFiles.Name = "btn_ShowFiles";
            this.btn_ShowFiles.Size = new System.Drawing.Size(56, 23);
            this.btn_ShowFiles.TabIndex = 3;
            this.btn_ShowFiles.Text = "Visa filer";
            this.btn_ShowFiles.UseVisualStyleBackColor = true;
            this.btn_ShowFiles.Click += new System.EventHandler(this.btn_ShowFiles_Click);
            // 
            // lst_Files
            // 
            this.lst_Files.FormattingEnabled = true;
            this.lst_Files.Location = new System.Drawing.Point(49, 58);
            this.lst_Files.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.lst_Files.Name = "lst_Files";
            this.lst_Files.Size = new System.Drawing.Size(189, 147);
            this.lst_Files.TabIndex = 4;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // folderBrowserDialog1
            // 
            // 
            // btn_Open
            // 
            this.btn_Open.Location = new System.Drawing.Point(370, 205);
            this.btn_Open.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.Size = new System.Drawing.Size(64, 27);
            this.btn_Open.TabIndex = 5;
            this.btn_Open.Text = "Öppna";
            this.btn_Open.UseVisualStyleBackColor = true;
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(439, 205);
            this.btn_Save.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(64, 27);
            this.btn_Save.TabIndex = 6;
            this.btn_Save.Text = "Spara";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // txt_CreateNewFile
            // 
            this.txt_CreateNewFile.Location = new System.Drawing.Point(50, 240);
            this.txt_CreateNewFile.Name = "txt_CreateNewFile";
            this.txt_CreateNewFile.Size = new System.Drawing.Size(188, 20);
            this.txt_CreateNewFile.TabIndex = 8;
            // 
            // btn_CreateNewTxtFile
            // 
            this.btn_CreateNewTxtFile.Location = new System.Drawing.Point(49, 264);
            this.btn_CreateNewTxtFile.Margin = new System.Windows.Forms.Padding(1);
            this.btn_CreateNewTxtFile.Name = "btn_CreateNewTxtFile";
            this.btn_CreateNewTxtFile.Size = new System.Drawing.Size(64, 27);
            this.btn_CreateNewTxtFile.TabIndex = 9;
            this.btn_CreateNewTxtFile.Text = "Ny";
            this.btn_CreateNewTxtFile.UseVisualStyleBackColor = true;
            this.btn_CreateNewTxtFile.Click += new System.EventHandler(this.btn_CreateNewTxtFile_Click);
            // 
            // lbl_CreateNew
            // 
            this.lbl_CreateNew.AutoSize = true;
            this.lbl_CreateNew.Location = new System.Drawing.Point(47, 224);
            this.lbl_CreateNew.Name = "lbl_CreateNew";
            this.lbl_CreateNew.Size = new System.Drawing.Size(82, 13);
            this.lbl_CreateNew.TabIndex = 10;
            this.lbl_CreateNew.Text = "Skapa ny text fil";
            // 
            // LäsOchVisaFiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 303);
            this.Controls.Add(this.lbl_CreateNew);
            this.Controls.Add(this.btn_CreateNewTxtFile);
            this.Controls.Add(this.txt_CreateNewFile);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_Open);
            this.Controls.Add(this.lst_Files);
            this.Controls.Add(this.btn_ShowFiles);
            this.Controls.Add(this.lbl_FilCatlog);
            this.Controls.Add(this.txt_Filkatlog);
            this.Controls.Add(this.txt_Content);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "LäsOchVisaFiler";
            this.Text = "LäsOchVisaFiler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Content;
        private System.Windows.Forms.MaskedTextBox txt_Filkatlog;
        private System.Windows.Forms.Label lbl_FilCatlog;
        private System.Windows.Forms.Button btn_ShowFiles;
        private System.Windows.Forms.ListBox lst_Files;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btn_Open;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.MaskedTextBox txt_CreateNewFile;
        private System.Windows.Forms.Button btn_CreateNewTxtFile;
        private System.Windows.Forms.Label lbl_CreateNew;
    }
}