﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        public delegate int Calculate(int a, int b);
        static void Main(string[] args)
        {

            string text = "Programmering C# på Nackademin";

            int position = text.IndexOf('C');

            string x = text.Substring(position + 5, 11);

            Console.WriteLine();
            Console.ReadLine();
        }       
        public void Pizza()
        {
            Dictionary<string, int> shoppingCart = new Dictionary<string, int>()
            {
                { "Margarita", 100},
                { "Calzone", 110 },
                { "Al Tonne", 120}

            };
            int lowest = shoppingCart.ElementAt(0).Value;
            int sum = 0;
            for (int i = 1; i < shoppingCart.Count; i++)
            {
                sum += shoppingCart.ElementAt(i).Value;
                if (shoppingCart.ElementAt(i).Value < lowest)
                {
                    lowest = shoppingCart.ElementAt(i).Value;
            }
            }
            if (shoppingCart.Count > 2)
            {
                sum -= lowest;
            }
            
        }

    }
}

