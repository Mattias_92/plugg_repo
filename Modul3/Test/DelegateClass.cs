﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class DelegateClass
    {
        // A delegate is a pointer to a method
        // A delegate can point to any method that has the same signature and return type.
        
        public static int Add(int x, int y)
        {
            Console.WriteLine("Inside add ");
            return x + y;
        }
        public static int Subtract(int x, int y)
        {
            Console.WriteLine("Inside subtract ");
            return x - y;
        }

        // To use an instance method with delegates first create an instance of the class then set the delegate to the instance method
        // Ie DelegateClass obj = new DelegateClass();
        // Calculate calc = obj.Divide;
        public int Divide(int x, int y)
        {
            Console.WriteLine("Inside divide");
            return x / y;
        }
    }
}
