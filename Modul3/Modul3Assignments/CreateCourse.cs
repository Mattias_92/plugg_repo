﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    public partial class frm_CreateCourse : Form
    {
        string teachersFilePath = Constants.FilePath + "teachers.txt";
        string coursesFIlePath = Constants.FilePath + "courses.txt";
        FileHandler<Teacher> teachers;
        FileHandler<Course> courses;
        public frm_CreateCourse()
        {
            InitializeComponent();
            teachers = new FileHandler<Teacher>(teachersFilePath);
            courses = new FileHandler<Course>(coursesFIlePath);
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            if (CheckForm(out int score))
            {
                Course course = new Course()
                {
                    Name = txt_CourseName.Text,
                    Score = score,
                    StartDate = dtp_StartDate.Value,
                    EndDate = dtp_EndDate.Value
                };

                Teacher teacher = (Teacher)lst_Teachers.SelectedItem;               
                if (teacher == null)
                {
                    MessageBox.Show("Lägg till en lärare för kursen senare");
                }
                else
                {
                    course.TeacherId = teacher.Id;
                }

                courses.SaveToFile(course);
                ResetFrom();
                MessageBox.Show($"Kursen {course.Name} skapad.");
            }
        }

        private void ResetFrom()
        {
            txt_CourseName.Clear();
            txt_Score.Clear();
            dtp_EndDate.ResetText();
            dtp_StartDate.ResetText();
        }

        private bool CheckForm(out int score)
        {
            bool formIsValid = true;

            if (!FormCheck.IsNameInputedCorrectly(out int errorIndex, txt_CourseName.Text))
            {
                LabelModule.AssignErrorMEssage(lbl_NameError, "Skriv in ett namn");
                formIsValid = false;
            }
            else if (lbl_NameError.Visible == true)
            {
                lbl_NameError.Visible = false;
            }
            if (!FormCheck.IsScoreInputedCorrect(txt_Score.Text, out score))
            {
                LabelModule.AssignErrorMEssage(lbl_ScoreError, "Skriv in ett giltigt score i heltal.");
                formIsValid = false;
            }
            else if (lbl_ScoreError.Visible == true)
            {
                lbl_ScoreError.Visible = false;
            }
            if (dtp_EndDate.Value < dtp_StartDate.Value)
            {
                LabelModule.AssignErrorMEssage(lbl_SlutDatumError, "Slutdatumet får inte vara före startdatum.");
                LabelModule.AssignErrorMEssage(lbl_StartDatumError, "Startdatumet får inte vara före slutdatum.");
                formIsValid = false;
            }
            else if (lbl_ScoreError.Visible == true)
            {
                lbl_SlutDatumError.Visible = false;
            }   lbl_StartDatumError.Visible = false;

            return formIsValid;
        }
        private void PopulateListBoxWithTeachrs()
        {
            lst_Teachers.DataSource = teachers.FileContent;
            lst_Teachers.DisplayMember = "FullName";
        }

        private void frm_CreateCourse_Load(object sender, EventArgs e)
        {
            PopulateListBoxWithTeachrs();
            PopulateDgvWithCourses();
        }

        private void PopulateDgvWithCourses()
        {
            dgv_CurrentCourses.Columns.Add("CourseNameColum", "Course");
            dgv_CurrentCourses.Columns.Add("TeacherNameColum", "Teacher");

            for (int i = 0; i < courses.FileContent.Count; i++)
            {
                Teacher teacher = teachers.FileContent.FirstOrDefault(x => x.Id == courses.FileContent[i].TeacherId);
                dgv_CurrentCourses.Rows.Add(courses.FileContent[i].Name, teacher.FullName);
            }
            dgv_CurrentCourses.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_CurrentCourses.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
