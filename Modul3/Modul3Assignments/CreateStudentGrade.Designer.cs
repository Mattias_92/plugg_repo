﻿
namespace Modul3Assignments
{
    partial class frm_CreateStudentGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_CourseAndTeacherListing = new System.Windows.Forms.DataGridView();
            this.dgv_Students = new System.Windows.Forms.DataGridView();
            this.cmb_Grades = new System.Windows.Forms.ComboBox();
            this.lbl_Grades = new System.Windows.Forms.Label();
            this.btn_CreateStudentGrade = new System.Windows.Forms.Button();
            this.txt_ChosenCourse = new System.Windows.Forms.TextBox();
            this.txt_ChosenStudent = new System.Windows.Forms.TextBox();
            this.lbl_ChosenStudent = new System.Windows.Forms.Label();
            this.lbl_CHoosenCourse = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CourseAndTeacherListing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Students)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_CourseAndTeacherListing
            // 
            this.dgv_CourseAndTeacherListing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CourseAndTeacherListing.Location = new System.Drawing.Point(9, 32);
            this.dgv_CourseAndTeacherListing.Margin = new System.Windows.Forms.Padding(1);
            this.dgv_CourseAndTeacherListing.Name = "dgv_CourseAndTeacherListing";
            this.dgv_CourseAndTeacherListing.RowHeadersWidth = 102;
            this.dgv_CourseAndTeacherListing.RowTemplate.Height = 40;
            this.dgv_CourseAndTeacherListing.Size = new System.Drawing.Size(552, 339);
            this.dgv_CourseAndTeacherListing.TabIndex = 0;
            this.dgv_CourseAndTeacherListing.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CourseAndTeacherListing_CellClick);
            this.dgv_CourseAndTeacherListing.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CourseAndTeacherListing_CellContentClick);
            // 
            // dgv_Students
            // 
            this.dgv_Students.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Students.Location = new System.Drawing.Point(576, 32);
            this.dgv_Students.Margin = new System.Windows.Forms.Padding(1);
            this.dgv_Students.Name = "dgv_Students";
            this.dgv_Students.RowHeadersWidth = 102;
            this.dgv_Students.RowTemplate.Height = 40;
            this.dgv_Students.Size = new System.Drawing.Size(260, 339);
            this.dgv_Students.TabIndex = 1;
            this.dgv_Students.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CourseAndTeacherListing_CellClick);
            // 
            // cmb_Grades
            // 
            this.cmb_Grades.FormattingEnabled = true;
            this.cmb_Grades.Location = new System.Drawing.Point(9, 436);
            this.cmb_Grades.Margin = new System.Windows.Forms.Padding(1);
            this.cmb_Grades.Name = "cmb_Grades";
            this.cmb_Grades.Size = new System.Drawing.Size(137, 21);
            this.cmb_Grades.TabIndex = 2;
            // 
            // lbl_Grades
            // 
            this.lbl_Grades.AutoSize = true;
            this.lbl_Grades.Location = new System.Drawing.Point(14, 413);
            this.lbl_Grades.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lbl_Grades.Name = "lbl_Grades";
            this.lbl_Grades.Size = new System.Drawing.Size(34, 13);
            this.lbl_Grades.TabIndex = 3;
            this.lbl_Grades.Text = "Betyg";
            // 
            // btn_CreateStudentGrade
            // 
            this.btn_CreateStudentGrade.Location = new System.Drawing.Point(545, 422);
            this.btn_CreateStudentGrade.Margin = new System.Windows.Forms.Padding(1);
            this.btn_CreateStudentGrade.Name = "btn_CreateStudentGrade";
            this.btn_CreateStudentGrade.Size = new System.Drawing.Size(186, 30);
            this.btn_CreateStudentGrade.TabIndex = 4;
            this.btn_CreateStudentGrade.Text = "Create";
            this.btn_CreateStudentGrade.UseVisualStyleBackColor = true;
            this.btn_CreateStudentGrade.Click += new System.EventHandler(this.btn_CreateStudentGrade_Click);
            // 
            // txt_ChosenCourse
            // 
            this.txt_ChosenCourse.Location = new System.Drawing.Point(162, 436);
            this.txt_ChosenCourse.Margin = new System.Windows.Forms.Padding(1);
            this.txt_ChosenCourse.Name = "txt_ChosenCourse";
            this.txt_ChosenCourse.ReadOnly = true;
            this.txt_ChosenCourse.Size = new System.Drawing.Size(168, 20);
            this.txt_ChosenCourse.TabIndex = 5;
            // 
            // txt_ChosenStudent
            // 
            this.txt_ChosenStudent.Location = new System.Drawing.Point(340, 436);
            this.txt_ChosenStudent.Margin = new System.Windows.Forms.Padding(1);
            this.txt_ChosenStudent.Name = "txt_ChosenStudent";
            this.txt_ChosenStudent.ReadOnly = true;
            this.txt_ChosenStudent.Size = new System.Drawing.Size(168, 20);
            this.txt_ChosenStudent.TabIndex = 6;
            // 
            // lbl_ChosenStudent
            // 
            this.lbl_ChosenStudent.AutoSize = true;
            this.lbl_ChosenStudent.Location = new System.Drawing.Point(338, 413);
            this.lbl_ChosenStudent.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lbl_ChosenStudent.Name = "lbl_ChosenStudent";
            this.lbl_ChosenStudent.Size = new System.Drawing.Size(52, 13);
            this.lbl_ChosenStudent.TabIndex = 7;
            this.lbl_ChosenStudent.Text = "Vald Elev";
            // 
            // lbl_CHoosenCourse
            // 
            this.lbl_CHoosenCourse.AutoSize = true;
            this.lbl_CHoosenCourse.Location = new System.Drawing.Point(165, 413);
            this.lbl_CHoosenCourse.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lbl_CHoosenCourse.Name = "lbl_CHoosenCourse";
            this.lbl_CHoosenCourse.Size = new System.Drawing.Size(51, 13);
            this.lbl_CHoosenCourse.TabIndex = 8;
            this.lbl_CHoosenCourse.Text = "Vald kurs";
            // 
            // frm_CreateStudentGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 500);
            this.Controls.Add(this.lbl_CHoosenCourse);
            this.Controls.Add(this.lbl_ChosenStudent);
            this.Controls.Add(this.txt_ChosenStudent);
            this.Controls.Add(this.txt_ChosenCourse);
            this.Controls.Add(this.btn_CreateStudentGrade);
            this.Controls.Add(this.lbl_Grades);
            this.Controls.Add(this.cmb_Grades);
            this.Controls.Add(this.dgv_Students);
            this.Controls.Add(this.dgv_CourseAndTeacherListing);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "frm_CreateStudentGrade";
            this.Text = "CreateStudentGrade";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CourseAndTeacherListing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Students)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_CourseAndTeacherListing;
        private System.Windows.Forms.DataGridView dgv_Students;
        private System.Windows.Forms.ComboBox cmb_Grades;
        private System.Windows.Forms.Label lbl_Grades;
        private System.Windows.Forms.Button btn_CreateStudentGrade;
        private System.Windows.Forms.TextBox txt_ChosenCourse;
        private System.Windows.Forms.TextBox txt_ChosenStudent;
        private System.Windows.Forms.Label lbl_ChosenStudent;
        private System.Windows.Forms.Label lbl_CHoosenCourse;
    }
}