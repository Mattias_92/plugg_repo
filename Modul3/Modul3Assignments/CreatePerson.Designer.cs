﻿
namespace Modul3Assignments
{
    partial class frm_CreateTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_FirstName = new System.Windows.Forms.Label();
            this.lbl_LastName = new System.Windows.Forms.Label();
            this.lbl_Email = new System.Windows.Forms.Label();
            this.lbl_PhoneNumber = new System.Windows.Forms.Label();
            this.txt_FirstName = new System.Windows.Forms.TextBox();
            this.txt_LastName = new System.Windows.Forms.TextBox();
            this.txt_PhoneNumber = new System.Windows.Forms.TextBox();
            this.txt_Email = new System.Windows.Forms.TextBox();
            this.btn_Create = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.lbl_FirstNameError = new System.Windows.Forms.Label();
            this.lbl_EmailError = new System.Windows.Forms.Label();
            this.lbl_LastNameError = new System.Windows.Forms.Label();
            this.lbl_PhoneError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_FirstName
            // 
            this.lbl_FirstName.AutoSize = true;
            this.lbl_FirstName.Location = new System.Drawing.Point(164, 201);
            this.lbl_FirstName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_FirstName.Name = "lbl_FirstName";
            this.lbl_FirstName.Size = new System.Drawing.Size(135, 32);
            this.lbl_FirstName.TabIndex = 0;
            this.lbl_FirstName.Text = "För namn";
            // 
            // lbl_LastName
            // 
            this.lbl_LastName.AutoSize = true;
            this.lbl_LastName.Location = new System.Drawing.Point(164, 301);
            this.lbl_LastName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_LastName.Name = "lbl_LastName";
            this.lbl_LastName.Size = new System.Drawing.Size(146, 32);
            this.lbl_LastName.TabIndex = 1;
            this.lbl_LastName.Text = "Efternamn";
            // 
            // lbl_Email
            // 
            this.lbl_Email.AutoSize = true;
            this.lbl_Email.Location = new System.Drawing.Point(164, 425);
            this.lbl_Email.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_Email.Name = "lbl_Email";
            this.lbl_Email.Size = new System.Drawing.Size(87, 32);
            this.lbl_Email.TabIndex = 2;
            this.lbl_Email.Text = "Email";
            // 
            // lbl_PhoneNumber
            // 
            this.lbl_PhoneNumber.AutoSize = true;
            this.lbl_PhoneNumber.Location = new System.Drawing.Point(164, 532);
            this.lbl_PhoneNumber.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_PhoneNumber.Name = "lbl_PhoneNumber";
            this.lbl_PhoneNumber.Size = new System.Drawing.Size(111, 32);
            this.lbl_PhoneNumber.TabIndex = 3;
            this.lbl_PhoneNumber.Text = "Telefon";
            // 
            // txt_FirstName
            // 
            this.txt_FirstName.Location = new System.Drawing.Point(353, 184);
            this.txt_FirstName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_FirstName.Name = "txt_FirstName";
            this.txt_FirstName.Size = new System.Drawing.Size(417, 38);
            this.txt_FirstName.TabIndex = 4;
            // 
            // txt_LastName
            // 
            this.txt_LastName.Location = new System.Drawing.Point(353, 284);
            this.txt_LastName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_LastName.Name = "txt_LastName";
            this.txt_LastName.Size = new System.Drawing.Size(417, 38);
            this.txt_LastName.TabIndex = 5;
            // 
            // txt_PhoneNumber
            // 
            this.txt_PhoneNumber.Location = new System.Drawing.Point(353, 515);
            this.txt_PhoneNumber.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_PhoneNumber.Name = "txt_PhoneNumber";
            this.txt_PhoneNumber.Size = new System.Drawing.Size(417, 38);
            this.txt_PhoneNumber.TabIndex = 6;
            // 
            // txt_Email
            // 
            this.txt_Email.Location = new System.Drawing.Point(353, 408);
            this.txt_Email.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(417, 38);
            this.txt_Email.TabIndex = 7;
            // 
            // btn_Create
            // 
            this.btn_Create.Location = new System.Drawing.Point(353, 647);
            this.btn_Create.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(200, 55);
            this.btn_Create.TabIndex = 8;
            this.btn_Create.Text = "Skapa";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(577, 647);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(200, 55);
            this.btn_Close.TabIndex = 9;
            this.btn_Close.Text = "Avbtyta";
            this.btn_Close.UseVisualStyleBackColor = true;
            // 
            // lbl_FirstNameError
            // 
            this.lbl_FirstNameError.AutoSize = true;
            this.lbl_FirstNameError.Location = new System.Drawing.Point(345, 239);
            this.lbl_FirstNameError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_FirstNameError.Name = "lbl_FirstNameError";
            this.lbl_FirstNameError.Size = new System.Drawing.Size(197, 32);
            this.lbl_FirstNameError.TabIndex = 10;
            this.lbl_FirstNameError.Text = "FelMedelande";
            this.lbl_FirstNameError.Visible = false;
            // 
            // lbl_EmailError
            // 
            this.lbl_EmailError.AutoSize = true;
            this.lbl_EmailError.Location = new System.Drawing.Point(345, 463);
            this.lbl_EmailError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_EmailError.Name = "lbl_EmailError";
            this.lbl_EmailError.Size = new System.Drawing.Size(197, 32);
            this.lbl_EmailError.TabIndex = 11;
            this.lbl_EmailError.Text = "Felmedelande";
            this.lbl_EmailError.Visible = false;
            // 
            // lbl_LastNameError
            // 
            this.lbl_LastNameError.AutoSize = true;
            this.lbl_LastNameError.Location = new System.Drawing.Point(345, 339);
            this.lbl_LastNameError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_LastNameError.Name = "lbl_LastNameError";
            this.lbl_LastNameError.Size = new System.Drawing.Size(197, 32);
            this.lbl_LastNameError.TabIndex = 12;
            this.lbl_LastNameError.Text = "FelMedelande";
            this.lbl_LastNameError.Visible = false;
            // 
            // lbl_PhoneError
            // 
            this.lbl_PhoneError.AutoSize = true;
            this.lbl_PhoneError.Location = new System.Drawing.Point(345, 570);
            this.lbl_PhoneError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_PhoneError.Name = "lbl_PhoneError";
            this.lbl_PhoneError.Size = new System.Drawing.Size(197, 32);
            this.lbl_PhoneError.TabIndex = 13;
            this.lbl_PhoneError.Text = "Felmedelande";
            this.lbl_PhoneError.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(176, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 69);
            this.label1.TabIndex = 14;
            this.label1.Text = "Skapa en";
            this.label1.Visible = false;
            // 
            // frm_CreateTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 847);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_PhoneError);
            this.Controls.Add(this.lbl_LastNameError);
            this.Controls.Add(this.lbl_EmailError);
            this.Controls.Add(this.lbl_FirstNameError);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.txt_Email);
            this.Controls.Add(this.txt_PhoneNumber);
            this.Controls.Add(this.txt_LastName);
            this.Controls.Add(this.txt_FirstName);
            this.Controls.Add(this.lbl_PhoneNumber);
            this.Controls.Add(this.lbl_Email);
            this.Controls.Add(this.lbl_LastName);
            this.Controls.Add(this.lbl_FirstName);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "frm_CreateTeacher";
            this.Text = "Create Person";
            this.Load += new System.EventHandler(this.frm_CreateTeacher_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_FirstName;
        private System.Windows.Forms.Label lbl_LastName;
        private System.Windows.Forms.Label lbl_Email;
        private System.Windows.Forms.Label lbl_PhoneNumber;
        private System.Windows.Forms.TextBox txt_FirstName;
        private System.Windows.Forms.TextBox txt_LastName;
        private System.Windows.Forms.TextBox txt_PhoneNumber;
        private System.Windows.Forms.TextBox txt_Email;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Label lbl_FirstNameError;
        private System.Windows.Forms.Label lbl_EmailError;
        private System.Windows.Forms.Label lbl_LastNameError;
        private System.Windows.Forms.Label lbl_PhoneError;
        private System.Windows.Forms.Label label1;
    }
}