﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    public partial class frm_CreateTeacher : Form
    {
        string fileType;
        //string path = @"..\..\Files\teachers.txt";
        string path;
        FileHandler<Person> file;
        List<ISaveObject> teachers = new List<ISaveObject>();
        Person type1;
        public frm_CreateTeacher(string fileName, Person type)
        {
            InitializeComponent();
            fileType = fileName;
            path = Constants.FilePath + fileType;
            file = new FileHandler<Person>(path);
            teachers.AddRange(file.FileContent);
            type1 = type;

        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            if (CheckForm())
            {
                Person person;
                switch (type1)
                {
                    case Teacher x when type1 is Teacher:
                        person = new Teacher();
                        break;
                    default:
                        person = new Student();
                        break;
                }

                person.FirstName = txt_FirstName.Text;
                person.LastName = txt_LastName.Text;
                person.Email = txt_Email.Text;
                person.PhoneNumber = txt_PhoneNumber.Text;
                this.Close();
                file.SaveToFile(person);
            }
        }

        private bool CheckForm()
        {
            bool formIsOk = true;
            int errorIndex;
            if (!FormCheck.IsNameInputedCorrectly(out errorIndex, txt_FirstName.Text, txt_LastName.Text))
            {
                LabelModule.ShowNameErrorMessage(lbl_FirstNameError, lbl_LastNameError, errorIndex);
                HideNameErrors(errorIndex);
                formIsOk = false;
            }
            else
            {
                HideNameErrors(errorIndex);
            }
            if (!FormCheck.IsEmailCorrect(txt_Email.Text) || FormCheck.EmailAlreadyInUse(txt_Email.Text, teachers))
            {
                LabelModule.ShowEmailErrorMessage(lbl_EmailError);
                formIsOk = false;
            }
            else if (lbl_EmailError.Visible == true)
            {
                LabelModule.HideLabel(lbl_EmailError);
            }
            if (!FormCheck.IsPhoneCorrect(txt_PhoneNumber.Text))
            {
                LabelModule.ShowPhoneErrorMessage(lbl_PhoneError);
                formIsOk = false;
            }
            else if (lbl_PhoneError.Visible == true)
            {
                LabelModule.HideLabel(lbl_PhoneError);
            }
            return formIsOk;
        }

        private void HideNameErrors(int errorIndex)
        {
            switch (errorIndex)
            {
                case 1:
                    LabelModule.HideLabel(lbl_FirstNameError);
                    LabelModule.HideLabel(lbl_LastNameError);
                    break;
                case -1:
                    LabelModule.HideLabel(lbl_FirstNameError);
                    break;
                case 0:
                    LabelModule.HideLabel(lbl_LastNameError);
                    break;
                default:
                    break;
            }
        }

        private void frm_CreateTeacher_Load(object sender, EventArgs e)
        {

        }
    }
}
