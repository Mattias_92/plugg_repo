﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul3Assignments
{
    class FileHandler<T> where T : ISaveObject
    {
        public string FilePath { get; set; }
        public List<T> FileContent { get; set; }
        public Type type1 { get; set; }


        public FileHandler(string fileName)
        {
            FilePath =Constants.FilePath + fileName;           
            ReadFile();
        }
        private void ReadFile()
        {
            using (StreamReader file = File.OpenText(FilePath))
            {
                JsonSerializer seroalizer = new JsonSerializer();
                var ts = (List<T>)seroalizer.Deserialize(file, typeof(List<T>));
                if (ts != null)
                {
                    FileContent = ts;   
                }
                else
                {
                    FileContent = new List<T>();
                }
            }
        }
        public string SaveToFile(T saveObject)
        {
            try
            {
                int currentHighestId = 0;
                if (FileContent.Count != 0)
                {
                    currentHighestId = FileContent.Where(x => x.Id != 0)
                    .Max(x => x.Id);
                }
                int newId = ++currentHighestId;
                saveObject.Id = newId;
                FileContent.Add(saveObject);
            }
            catch (Exception ex)
            {
                return "Someting went wrong";
            }
            using (StreamWriter file = File.CreateText(FilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, FileContent);
            }
            return "Success";
        }

        public void DeleteFromFile(int deleteId)
        {
            if (FileContent.Count > 0)
            {
                var objectToDelete = FileContent.FirstOrDefault(x => x.Id == deleteId);
                FileContent.Remove(objectToDelete);
                List<T> test = new List<T>();
                FileContent.RemoveAll(x => x.Id == test.FirstOrDefault(y => y.Id == x.Id).Id);

                var dataSource2 = (from c in FileContent
                                   join t in test
                                   on c.Id equals t.Id
                                   select c).ToList();
                foreach (var item in dataSource2)
                {
                    FileContent.Remove(item);
                }
                using (StreamWriter file = File.CreateText(FilePath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, FileContent);
                }
            }
        }

    }
}
