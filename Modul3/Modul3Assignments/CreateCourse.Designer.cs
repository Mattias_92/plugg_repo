﻿
namespace Modul3Assignments
{
    partial class frm_CreateCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_CourseName = new System.Windows.Forms.Label();
            this.lbl_Score = new System.Windows.Forms.Label();
            this.lbl_StartDate = new System.Windows.Forms.Label();
            this.lbl_EndDate = new System.Windows.Forms.Label();
            this.lbl_StartDatumError = new System.Windows.Forms.Label();
            this.txt_CourseName = new System.Windows.Forms.TextBox();
            this.txt_Score = new System.Windows.Forms.TextBox();
            this.dtp_StartDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_EndDate = new System.Windows.Forms.DateTimePicker();
            this.lst_Teachers = new System.Windows.Forms.ListBox();
            this.lbl_RepsonsTeacher = new System.Windows.Forms.Label();
            this.btn_Create = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.lbl_SlutDatumError = new System.Windows.Forms.Label();
            this.lbl_NameError = new System.Windows.Forms.Label();
            this.lbl_ScoreError = new System.Windows.Forms.Label();
            this.lbl_ExistingCourses = new System.Windows.Forms.Label();
            this.dgv_CurrentCourses = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CurrentCourses)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_CourseName
            // 
            this.lbl_CourseName.AutoSize = true;
            this.lbl_CourseName.Location = new System.Drawing.Point(192, 174);
            this.lbl_CourseName.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_CourseName.Name = "lbl_CourseName";
            this.lbl_CourseName.Size = new System.Drawing.Size(144, 32);
            this.lbl_CourseName.TabIndex = 0;
            this.lbl_CourseName.Text = "Kursnamn";
            // 
            // lbl_Score
            // 
            this.lbl_Score.AutoSize = true;
            this.lbl_Score.Location = new System.Drawing.Point(192, 320);
            this.lbl_Score.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_Score.Name = "lbl_Score";
            this.lbl_Score.Size = new System.Drawing.Size(168, 32);
            this.lbl_Score.TabIndex = 1;
            this.lbl_Score.Text = "Antal poäng";
            // 
            // lbl_StartDate
            // 
            this.lbl_StartDate.AutoSize = true;
            this.lbl_StartDate.Location = new System.Drawing.Point(192, 451);
            this.lbl_StartDate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_StartDate.Name = "lbl_StartDate";
            this.lbl_StartDate.Size = new System.Drawing.Size(154, 32);
            this.lbl_StartDate.TabIndex = 2;
            this.lbl_StartDate.Text = "Startdatum";
            // 
            // lbl_EndDate
            // 
            this.lbl_EndDate.AutoSize = true;
            this.lbl_EndDate.Location = new System.Drawing.Point(192, 618);
            this.lbl_EndDate.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_EndDate.Name = "lbl_EndDate";
            this.lbl_EndDate.Size = new System.Drawing.Size(144, 32);
            this.lbl_EndDate.TabIndex = 3;
            this.lbl_EndDate.Text = "Slutdatum";
            // 
            // lbl_StartDatumError
            // 
            this.lbl_StartDatumError.AutoSize = true;
            this.lbl_StartDatumError.Location = new System.Drawing.Point(371, 513);
            this.lbl_StartDatumError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_StartDatumError.Name = "lbl_StartDatumError";
            this.lbl_StartDatumError.Size = new System.Drawing.Size(197, 32);
            this.lbl_StartDatumError.TabIndex = 4;
            this.lbl_StartDatumError.Text = "Felmedelande";
            // 
            // txt_CourseName
            // 
            this.txt_CourseName.Location = new System.Drawing.Point(379, 157);
            this.txt_CourseName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_CourseName.Name = "txt_CourseName";
            this.txt_CourseName.Size = new System.Drawing.Size(391, 38);
            this.txt_CourseName.TabIndex = 5;
            // 
            // txt_Score
            // 
            this.txt_Score.Location = new System.Drawing.Point(379, 303);
            this.txt_Score.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt_Score.Name = "txt_Score";
            this.txt_Score.Size = new System.Drawing.Size(391, 38);
            this.txt_Score.TabIndex = 6;
            // 
            // dtp_StartDate
            // 
            this.dtp_StartDate.Location = new System.Drawing.Point(379, 434);
            this.dtp_StartDate.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.dtp_StartDate.Name = "dtp_StartDate";
            this.dtp_StartDate.Size = new System.Drawing.Size(527, 38);
            this.dtp_StartDate.TabIndex = 7;
            // 
            // dtp_EndDate
            // 
            this.dtp_EndDate.Location = new System.Drawing.Point(379, 601);
            this.dtp_EndDate.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.dtp_EndDate.Name = "dtp_EndDate";
            this.dtp_EndDate.Size = new System.Drawing.Size(527, 38);
            this.dtp_EndDate.TabIndex = 8;
            // 
            // lst_Teachers
            // 
            this.lst_Teachers.FormattingEnabled = true;
            this.lst_Teachers.ItemHeight = 31;
            this.lst_Teachers.Location = new System.Drawing.Point(1080, 143);
            this.lst_Teachers.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.lst_Teachers.Name = "lst_Teachers";
            this.lst_Teachers.Size = new System.Drawing.Size(796, 500);
            this.lst_Teachers.TabIndex = 9;
            // 
            // lbl_RepsonsTeacher
            // 
            this.lbl_RepsonsTeacher.AutoSize = true;
            this.lbl_RepsonsTeacher.Location = new System.Drawing.Point(1072, 105);
            this.lbl_RepsonsTeacher.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_RepsonsTeacher.Name = "lbl_RepsonsTeacher";
            this.lbl_RepsonsTeacher.Size = new System.Drawing.Size(215, 32);
            this.lbl_RepsonsTeacher.TabIndex = 10;
            this.lbl_RepsonsTeacher.Text = "Ansvarig Lärare";
            // 
            // btn_Create
            // 
            this.btn_Create.Location = new System.Drawing.Point(388, 1061);
            this.btn_Create.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(200, 55);
            this.btn_Create.TabIndex = 12;
            this.btn_Create.Text = "Skapa";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(706, 1061);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(200, 55);
            this.btn_Cancel.TabIndex = 13;
            this.btn_Cancel.Text = "Avbryt";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // lbl_SlutDatumError
            // 
            this.lbl_SlutDatumError.AutoSize = true;
            this.lbl_SlutDatumError.Location = new System.Drawing.Point(371, 663);
            this.lbl_SlutDatumError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_SlutDatumError.Name = "lbl_SlutDatumError";
            this.lbl_SlutDatumError.Size = new System.Drawing.Size(197, 32);
            this.lbl_SlutDatumError.TabIndex = 14;
            this.lbl_SlutDatumError.Text = "Felmedelande";
            // 
            // lbl_NameError
            // 
            this.lbl_NameError.AutoSize = true;
            this.lbl_NameError.Location = new System.Drawing.Point(382, 211);
            this.lbl_NameError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_NameError.Name = "lbl_NameError";
            this.lbl_NameError.Size = new System.Drawing.Size(197, 32);
            this.lbl_NameError.TabIndex = 15;
            this.lbl_NameError.Text = "Felmedelande";
            // 
            // lbl_ScoreError
            // 
            this.lbl_ScoreError.AutoSize = true;
            this.lbl_ScoreError.Location = new System.Drawing.Point(382, 363);
            this.lbl_ScoreError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_ScoreError.Name = "lbl_ScoreError";
            this.lbl_ScoreError.Size = new System.Drawing.Size(197, 32);
            this.lbl_ScoreError.TabIndex = 16;
            this.lbl_ScoreError.Text = "Felmedelande";
            // 
            // lbl_ExistingCourses
            // 
            this.lbl_ExistingCourses.AutoSize = true;
            this.lbl_ExistingCourses.Location = new System.Drawing.Point(1074, 692);
            this.lbl_ExistingCourses.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_ExistingCourses.Name = "lbl_ExistingCourses";
            this.lbl_ExistingCourses.Size = new System.Drawing.Size(239, 32);
            this.lbl_ExistingCourses.TabIndex = 18;
            this.lbl_ExistingCourses.Text = "Nuvarande kurser";
            // 
            // dgv_CurrentCourses
            // 
            this.dgv_CurrentCourses.AllowUserToAddRows = false;
            this.dgv_CurrentCourses.AllowUserToDeleteRows = false;
            this.dgv_CurrentCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CurrentCourses.Location = new System.Drawing.Point(1078, 751);
            this.dgv_CurrentCourses.Name = "dgv_CurrentCourses";
            this.dgv_CurrentCourses.ReadOnly = true;
            this.dgv_CurrentCourses.RowHeadersWidth = 102;
            this.dgv_CurrentCourses.RowTemplate.Height = 40;
            this.dgv_CurrentCourses.Size = new System.Drawing.Size(798, 365);
            this.dgv_CurrentCourses.TabIndex = 19;
            // 
            // frm_CreateCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2424, 1424);
            this.Controls.Add(this.dgv_CurrentCourses);
            this.Controls.Add(this.lbl_ExistingCourses);
            this.Controls.Add(this.lbl_ScoreError);
            this.Controls.Add(this.lbl_NameError);
            this.Controls.Add(this.lbl_SlutDatumError);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Create);
            this.Controls.Add(this.lbl_RepsonsTeacher);
            this.Controls.Add(this.lst_Teachers);
            this.Controls.Add(this.dtp_EndDate);
            this.Controls.Add(this.dtp_StartDate);
            this.Controls.Add(this.txt_Score);
            this.Controls.Add(this.txt_CourseName);
            this.Controls.Add(this.lbl_StartDatumError);
            this.Controls.Add(this.lbl_EndDate);
            this.Controls.Add(this.lbl_StartDate);
            this.Controls.Add(this.lbl_Score);
            this.Controls.Add(this.lbl_CourseName);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "frm_CreateCourse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Course";
            this.Load += new System.EventHandler(this.frm_CreateCourse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CurrentCourses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_CourseName;
        private System.Windows.Forms.Label lbl_Score;
        private System.Windows.Forms.Label lbl_StartDate;
        private System.Windows.Forms.Label lbl_EndDate;
        private System.Windows.Forms.Label lbl_StartDatumError;
        private System.Windows.Forms.TextBox txt_CourseName;
        private System.Windows.Forms.TextBox txt_Score;
        private System.Windows.Forms.DateTimePicker dtp_StartDate;
        private System.Windows.Forms.DateTimePicker dtp_EndDate;
        private System.Windows.Forms.ListBox lst_Teachers;
        private System.Windows.Forms.Label lbl_RepsonsTeacher;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lbl_SlutDatumError;
        private System.Windows.Forms.Label lbl_NameError;
        private System.Windows.Forms.Label lbl_ScoreError;
        private System.Windows.Forms.Label lbl_ExistingCourses;
        private System.Windows.Forms.DataGridView dgv_CurrentCourses;
    }
}