﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    public partial class TestPlayGround : Form
    {
        List<char> testList;
        DataTable dataTable;
        DataTable coursesTable;
        string teachersFilePath = Constants.FilePath + "teachers.txt";
        string coursesFIlePath = Constants.FilePath + "courses.txt";
        FileHandler<Teacher> teachers;
        FileHandler<Course> courses;
        public TestPlayGround()
        {
            InitializeComponent();
            teachers = new FileHandler<Teacher>(teachersFilePath);
            courses = new FileHandler<Course>(coursesFIlePath);
            testList = Enumerable.Range('A', 'Z' - 'A' + 1).Select(c => (char)c).ToList();
            var columnCount = 4;
            dataTable = new DataTable();
            coursesTable = new DataTable();
            for (int i = 0; i < columnCount; i++)
            {
                dataTable.Columns.Add($"C{i + 1}");
            }
            testList.Select((x, i) => new { Value = x, Index = i })
                .GroupBy(x => x.Index / columnCount).ToList()
                .ForEach(x => dataTable.Rows.Add(x.Select(m => m.Value).Cast<object>().ToArray()));

            dgv_Test.DataSource = dataTable;
            
            PopulateDgvWithCourses2();
        }
        private void PopulateDgvWithCourses()
        {
            var columnCount = 2;
            coursesTable.Columns.Add("Course Name");
            coursesTable.Columns.Add("Teacher Name");
            courses.FileContent.Select((x, i) => new { Name = x.Name, Index = i })
                .GroupBy(x => x.Index / 1).ToList()
                .ForEach(x => coursesTable.Rows.Add(x.Select(m => m.Name).Cast<object>().ToArray()));
            teachers.FileContent.Select((x, i) => new { Name = x.FirstName, Index = i })
                .GroupBy(x => x.Index / 2).ToList()
                .ForEach(x => coursesTable.Rows.Add(x.Select(m => m.Name).Cast<object>().ToArray()));
            dgv_test2.DataSource = coursesTable;
        }

        private void PopulateDgvWithCourses2()
        {
            dgv_test2.Columns.Add("CourseNameColum", "Course");
            dgv_test2.Columns.Add("TeacherNameColum", "Teacher");

            for (int i = 0; i < courses.FileContent.Count; i++)
            {
                Teacher teacher = teachers.FileContent.FirstOrDefault(x => x.Id == courses.FileContent[i].TeacherId);
                dgv_test2.Rows.Add(courses.FileContent[i].Name, teacher.FullName);
            }
            dgv_test2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_test2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void TestPlayGround_Load(object sender, EventArgs e)
        {

        }
    }
}
