﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    public partial class frm_Uppgift2 : Form
    {
        public frm_Uppgift2()
        {
            InitializeComponent();
        }

        private void btn_CreateStudent_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<frm_CreateTeacher>().Count() == 1)
            {
                Application.OpenForms.OfType<frm_CreateTeacher>().First().Close();
            }
            else
            {
                Student student = new Student();
                frm_CreateTeacher frm_Create = new frm_CreateTeacher("students.txt",  student);
                frm_Create.ShowDialog();
            }
        }

        private void btn_CreateTeacher_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<frm_CreateTeacher>().Count() == 1)
            {
                Application.OpenForms.OfType<frm_CreateTeacher>().First().Close();
            }
            else
            {
                Teacher student = new Teacher();
                frm_CreateTeacher frm_Create = new frm_CreateTeacher("teachers.txt", student);
                frm_Create.ShowDialog();
            }
        }

        private void btn_CreateCourse_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<frm_CreateCourse>().Count() == 1)
            {
                Application.OpenForms.OfType<frm_CreateCourse>().First().Close();
            }
            else
            {
                frm_CreateCourse frm_Create = new frm_CreateCourse();
                frm_Create.ShowDialog();
            }
        }
    }
}
