﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    class LabelModule
    {
        internal static void HideLabel(Label lbl_Hide)
        {
            lbl_Hide.Visible = false;
        }
        internal static void ShowPhoneErrorMessage(Label lbl_PhoneError)
        {
            AssignErrorMEssage(lbl_PhoneError, "Skriv in ett giltig telefonnummber");
        }
        internal static void ShowEmailErrorMessage(Label lbl_EmailError)
        {
            AssignErrorMEssage(lbl_EmailError, "Skriv in en giltig email adress");
        }

        internal static void ShowNameErrorMessage(Label lbl_FirstNameError, Label lbl_LastNameError, int errorIndex)
        {
            switch (errorIndex)
            {
                case 0:
                    AssignErrorMEssage(lbl_FirstNameError, "Skriv in ett namn");
                    if (lbl_LastNameError.Visible == true)
                    {
                        lbl_LastNameError.Visible = false;
                    }
                    break;
                case -1:
                    AssignErrorMEssage(lbl_LastNameError, "Skriv in ett namn");
                    if (lbl_FirstNameError.Visible == true)
                    {
                        lbl_FirstNameError.Visible = false;
                    }
                    break;
                case -2:
                    AssignErrorMEssage(lbl_FirstNameError, "Skriv in ett namn");
                    AssignErrorMEssage(lbl_LastNameError, "Skriv in ett namn");
                    break;
                default:
                    break;
            }
        }

        public static void AssignErrorMEssage(Label lbl_error, string message)
        {
            lbl_error.Visible = true;
            lbl_error.Text = message;
            lbl_error.ForeColor = Color.Red;
        }
    }
}

