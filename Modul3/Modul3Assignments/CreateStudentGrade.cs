﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul3Assignments
{
    public partial class frm_CreateStudentGrade : Form
    {
        FileHandler<Teacher> teachers;
        FileHandler<Student> students;
        FileHandler<Course> courses;
        public frm_CreateStudentGrade()
        {
            InitializeComponent();
            teachers = new FileHandler<Teacher>("teachers.txt");
            students = new FileHandler<Student>("students.txt");
            courses = new FileHandler<Course>("courses.txt");
            PopulateCourseAndTeacherListing();
            PopulateStudentGrid();
            PopulateComboBox();
        }
        private void PopulateCourseAndTeacherListing()
        {
            // lambda
            var coursesDataSource = courses.FileContent.Select(c => new
            {
                CourseId = c.Id,
                TeacherId = c.TeacherId,
                Name = c.Name,
                StartDate = c.StartDate,
                EndDate = c.EndDate,
                TeacherName = teachers.FileContent.FirstOrDefault(x => x.Id == c.TeacherId).FullName
            }).ToList();
            // linq
            var dataSource2 = (from c in courses.FileContent
                               join t in teachers.FileContent
                               on c.TeacherId equals t.Id
                               select new
                               {
                                   CourseId = c.Id,
                                   TeacherId = c.TeacherId,
                                   Name = c.Name,
                                   StartDate = c.StartDate,
                                   EndDate = c.EndDate,
                                   TeacherName = t.FullName
                               }).ToList();
            
            dgv_CourseAndTeacherListing.DataSource = coursesDataSource;
            dgv_CourseAndTeacherListing.Columns[0].Visible = false;
            dgv_CourseAndTeacherListing.Columns[1].Visible = false;
            dgv_CourseAndTeacherListing.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_CourseAndTeacherListing.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void PopulateStudentGrid()
        {
            var studentsDataSource = students.FileContent.Select(s => new
            {
                StudentId = s.Id,
                StudentName = s.FullName,
            }).ToList();

            dgv_Students.DataSource = studentsDataSource;
            dgv_Students.Columns[0].Visible = false;
            dgv_Students.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }

        private void PopulateComboBox()
        {
            cmb_Grades.DataSource = Enum.GetValues(typeof(Grade));
        }
        private Grade GetValueFromComboBox()
        {
            var grade = (Grade)cmb_Grades.SelectedItem;
            return grade;
        }

        private void btn_CreateStudentGrade_Click(object sender, EventArgs e)
        {
            var grade = GetValueFromComboBox();
        }

        private void dgv_CourseAndTeacherListing_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var test = (string)dgv_CourseAndTeacherListing.SelectedRows[2].Cells["Name"].Value;
            txt_ChosenCourse.Text = test;
        }

        private void dgv_CourseAndTeacherListing_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
