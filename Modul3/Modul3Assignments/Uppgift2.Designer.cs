﻿
namespace Modul3Assignments
{
    partial class frm_Uppgift2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_CreateCourse = new System.Windows.Forms.Button();
            this.btn_CreateTeacher = new System.Windows.Forms.Button();
            this.btn_CreateStudent = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_CreateCourse
            // 
            this.btn_CreateCourse.Location = new System.Drawing.Point(58, 105);
            this.btn_CreateCourse.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_CreateCourse.Name = "btn_CreateCourse";
            this.btn_CreateCourse.Size = new System.Drawing.Size(312, 55);
            this.btn_CreateCourse.TabIndex = 0;
            this.btn_CreateCourse.Text = "Skapa Kurs";
            this.btn_CreateCourse.UseVisualStyleBackColor = true;
            this.btn_CreateCourse.Click += new System.EventHandler(this.btn_CreateCourse_Click);
            // 
            // btn_CreateTeacher
            // 
            this.btn_CreateTeacher.Location = new System.Drawing.Point(58, 198);
            this.btn_CreateTeacher.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_CreateTeacher.Name = "btn_CreateTeacher";
            this.btn_CreateTeacher.Size = new System.Drawing.Size(312, 50);
            this.btn_CreateTeacher.TabIndex = 1;
            this.btn_CreateTeacher.Text = "Skapa Lärare";
            this.btn_CreateTeacher.UseVisualStyleBackColor = true;
            this.btn_CreateTeacher.Click += new System.EventHandler(this.btn_CreateTeacher_Click);
            // 
            // btn_CreateStudent
            // 
            this.btn_CreateStudent.Location = new System.Drawing.Point(58, 293);
            this.btn_CreateStudent.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_CreateStudent.Name = "btn_CreateStudent";
            this.btn_CreateStudent.Size = new System.Drawing.Size(312, 50);
            this.btn_CreateStudent.TabIndex = 2;
            this.btn_CreateStudent.Text = "Skapa Student";
            this.btn_CreateStudent.UseVisualStyleBackColor = true;
            this.btn_CreateStudent.Click += new System.EventHandler(this.btn_CreateStudent_Click);
            // 
            // frm_Uppgift2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 406);
            this.Controls.Add(this.btn_CreateStudent);
            this.Controls.Add(this.btn_CreateTeacher);
            this.Controls.Add(this.btn_CreateCourse);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "frm_Uppgift2";
            this.Text = "Uppgift2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_CreateCourse;
        private System.Windows.Forms.Button btn_CreateTeacher;
        private System.Windows.Forms.Button btn_CreateStudent;
    }
}