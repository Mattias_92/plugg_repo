﻿
namespace Modul3Assignments
{
    partial class TestPlayGround
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_Test = new System.Windows.Forms.DataGridView();
            this.dgv_test2 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Test)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_Test
            // 
            this.dgv_Test.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Test.Location = new System.Drawing.Point(12, 168);
            this.dgv_Test.Name = "dgv_Test";
            this.dgv_Test.RowHeadersWidth = 102;
            this.dgv_Test.RowTemplate.Height = 40;
            this.dgv_Test.Size = new System.Drawing.Size(1363, 735);
            this.dgv_Test.TabIndex = 0;
            // 
            // dgv_test2
            // 
            this.dgv_test2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_test2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test2.Location = new System.Drawing.Point(1761, 168);
            this.dgv_test2.Name = "dgv_test2";
            this.dgv_test2.ReadOnly = true;
            this.dgv_test2.RowHeadersWidth = 102;
            this.dgv_test2.RowTemplate.Height = 40;
            this.dgv_test2.Size = new System.Drawing.Size(1363, 735);
            this.dgv_test2.TabIndex = 1;
            // 
            // TestPlayGround
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(3295, 1179);
            this.Controls.Add(this.dgv_test2);
            this.Controls.Add(this.dgv_Test);
            this.Name = "TestPlayGround";
            this.Text = "TestPlayGround";
            this.Load += new System.EventHandler(this.TestPlayGround_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Test)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_Test;
        private System.Windows.Forms.DataGridView dgv_test2;
    }
}