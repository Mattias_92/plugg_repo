﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int years;
            int months;
            int days;
            DateTime userBirthDate;
            DateTime today = DateTime.Today;

            Console.Write("Enter your name: ");
            string userName = Console.ReadLine();

            Console.Write("<enter your birth date use yyyy/mm/dd format: ");

            while (!DateTime.TryParse(Console.ReadLine(), out userBirthDate))
            {
                Console.Write("enter your birth date use yyyy/mm/dd format: ");
            }

            years = today.Year - userBirthDate.Year;
            months = today.Month - userBirthDate.Month;

            if (today.Day < userBirthDate.Day)
            {
                months--;
            }
            if (months < 0)
            {
                years--;
                months += 12;
            }
            var test = today - userBirthDate;
            var test2 = today - userBirthDate.AddMonths((years * 12) + months);
            days = (today - userBirthDate.AddMonths((years * 12) + months)).Days;
            Console.WriteLine( years + months + days);
        }
    }
}
