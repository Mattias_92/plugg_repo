﻿
namespace AdressbookOne
{
    partial class frmAddressBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.lbl_ContactInfo = new System.Windows.Forms.Label();
            this.lbl_Contacts = new System.Windows.Forms.Label();
            this.lbl_SearchBy = new System.Windows.Forms.Label();
            this.Search = new System.Windows.Forms.Label();
            this.cmb_SearchTerms = new System.Windows.Forms.ComboBox();
            this.txt_SearchTerm = new System.Windows.Forms.TextBox();
            this.lst_Contacts = new System.Windows.Forms.ListBox();
            this.btn_CreateContact = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_DeleteContact = new System.Windows.Forms.Button();
            this.btn_EditContact = new System.Windows.Forms.Button();
            this.btn_Back = new System.Windows.Forms.Button();
            this.dgv_ContactInfo = new System.Windows.Forms.DataGridView();
            this.lbl_DisplayBy = new System.Windows.Forms.Label();
            this.cmb_DisplayBy = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ContactInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(528, 542);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(176, 23);
            this.btn_Cancel.TabIndex = 41;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.button3_Click);
            // 
            // lbl_ContactInfo
            // 
            this.lbl_ContactInfo.AutoSize = true;
            this.lbl_ContactInfo.Location = new System.Drawing.Point(12, 272);
            this.lbl_ContactInfo.Name = "lbl_ContactInfo";
            this.lbl_ContactInfo.Size = new System.Drawing.Size(65, 13);
            this.lbl_ContactInfo.TabIndex = 39;
            this.lbl_ContactInfo.Text = "Contact Info";
            // 
            // lbl_Contacts
            // 
            this.lbl_Contacts.AutoSize = true;
            this.lbl_Contacts.Location = new System.Drawing.Point(12, 106);
            this.lbl_Contacts.Name = "lbl_Contacts";
            this.lbl_Contacts.Size = new System.Drawing.Size(49, 13);
            this.lbl_Contacts.TabIndex = 38;
            this.lbl_Contacts.Text = "Contacts";
            // 
            // lbl_SearchBy
            // 
            this.lbl_SearchBy.AutoSize = true;
            this.lbl_SearchBy.Location = new System.Drawing.Point(374, 156);
            this.lbl_SearchBy.Name = "lbl_SearchBy";
            this.lbl_SearchBy.Size = new System.Drawing.Size(55, 13);
            this.lbl_SearchBy.TabIndex = 37;
            this.lbl_SearchBy.Text = "Search by";
            // 
            // Search
            // 
            this.Search.AutoSize = true;
            this.Search.Location = new System.Drawing.Point(374, 106);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(41, 13);
            this.Search.TabIndex = 36;
            this.Search.Text = "Search";
            // 
            // cmb_SearchTerms
            // 
            this.cmb_SearchTerms.FormattingEnabled = true;
            this.cmb_SearchTerms.Location = new System.Drawing.Point(435, 150);
            this.cmb_SearchTerms.Name = "cmb_SearchTerms";
            this.cmb_SearchTerms.Size = new System.Drawing.Size(243, 21);
            this.cmb_SearchTerms.TabIndex = 35;
            this.cmb_SearchTerms.SelectedIndexChanged += new System.EventHandler(this.txt_SearchTerm_TextChanged);
            // 
            // txt_SearchTerm
            // 
            this.txt_SearchTerm.Location = new System.Drawing.Point(377, 122);
            this.txt_SearchTerm.Name = "txt_SearchTerm";
            this.txt_SearchTerm.Size = new System.Drawing.Size(301, 20);
            this.txt_SearchTerm.TabIndex = 34;
            this.txt_SearchTerm.TextChanged += new System.EventHandler(this.txt_SearchTerm_TextChanged);
            // 
            // lst_Contacts
            // 
            this.lst_Contacts.FormattingEnabled = true;
            this.lst_Contacts.Location = new System.Drawing.Point(15, 122);
            this.lst_Contacts.Name = "lst_Contacts";
            this.lst_Contacts.Size = new System.Drawing.Size(268, 147);
            this.lst_Contacts.TabIndex = 32;
            this.lst_Contacts.SelectedIndexChanged += new System.EventHandler(this.lst_Contacts_SelectedIndexChanged);
            // 
            // btn_CreateContact
            // 
            this.btn_CreateContact.Location = new System.Drawing.Point(434, 179);
            this.btn_CreateContact.Name = "btn_CreateContact";
            this.btn_CreateContact.Size = new System.Drawing.Size(244, 23);
            this.btn_CreateContact.TabIndex = 42;
            this.btn_CreateContact.Text = "Create Contact";
            this.btn_CreateContact.UseVisualStyleBackColor = true;
            this.btn_CreateContact.Click += new System.EventHandler(this.btn_CreateContact_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(174, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(459, 37);
            this.label1.TabIndex = 43;
            this.label1.Text = "Welcome to your address book";
            // 
            // btn_DeleteContact
            // 
            this.btn_DeleteContact.Location = new System.Drawing.Point(434, 237);
            this.btn_DeleteContact.Name = "btn_DeleteContact";
            this.btn_DeleteContact.Size = new System.Drawing.Size(244, 23);
            this.btn_DeleteContact.TabIndex = 44;
            this.btn_DeleteContact.Text = "Delete Contact";
            this.btn_DeleteContact.UseVisualStyleBackColor = true;
            this.btn_DeleteContact.Click += new System.EventHandler(this.btn_DeleteContact_Click);
            // 
            // btn_EditContact
            // 
            this.btn_EditContact.Location = new System.Drawing.Point(434, 208);
            this.btn_EditContact.Name = "btn_EditContact";
            this.btn_EditContact.Size = new System.Drawing.Size(244, 23);
            this.btn_EditContact.TabIndex = 45;
            this.btn_EditContact.Text = "Edit Contact";
            this.btn_EditContact.UseVisualStyleBackColor = true;
            this.btn_EditContact.Click += new System.EventHandler(this.btn_EditContact_Click);
            // 
            // btn_Back
            // 
            this.btn_Back.Location = new System.Drawing.Point(340, 542);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(176, 23);
            this.btn_Back.TabIndex = 46;
            this.btn_Back.Text = "Back";
            this.btn_Back.UseVisualStyleBackColor = true;
            // 
            // dgv_ContactInfo
            // 
            this.dgv_ContactInfo.AllowUserToAddRows = false;
            this.dgv_ContactInfo.AllowUserToDeleteRows = false;
            this.dgv_ContactInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_ContactInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_ContactInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ContactInfo.Location = new System.Drawing.Point(12, 291);
            this.dgv_ContactInfo.Name = "dgv_ContactInfo";
            this.dgv_ContactInfo.ReadOnly = true;
            this.dgv_ContactInfo.Size = new System.Drawing.Size(809, 43);
            this.dgv_ContactInfo.TabIndex = 47;
            // 
            // lbl_DisplayBy
            // 
            this.lbl_DisplayBy.AutoSize = true;
            this.lbl_DisplayBy.Location = new System.Drawing.Point(100, 106);
            this.lbl_DisplayBy.Name = "lbl_DisplayBy";
            this.lbl_DisplayBy.Size = new System.Drawing.Size(55, 13);
            this.lbl_DisplayBy.TabIndex = 48;
            this.lbl_DisplayBy.Text = "Display by";
            // 
            // cmb_DisplayBy
            // 
            this.cmb_DisplayBy.FormattingEnabled = true;
            this.cmb_DisplayBy.Location = new System.Drawing.Point(161, 98);
            this.cmb_DisplayBy.Name = "cmb_DisplayBy";
            this.cmb_DisplayBy.Size = new System.Drawing.Size(122, 21);
            this.cmb_DisplayBy.TabIndex = 49;
            this.cmb_DisplayBy.SelectedIndexChanged += new System.EventHandler(this.cmb_DisplayBy_SelectedIndexChanged);
            // 
            // frmAddressBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 588);
            this.Controls.Add(this.cmb_DisplayBy);
            this.Controls.Add(this.lbl_DisplayBy);
            this.Controls.Add(this.dgv_ContactInfo);
            this.Controls.Add(this.btn_Back);
            this.Controls.Add(this.btn_EditContact);
            this.Controls.Add(this.btn_DeleteContact);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_CreateContact);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.lbl_ContactInfo);
            this.Controls.Add(this.lbl_Contacts);
            this.Controls.Add(this.lbl_SearchBy);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.cmb_SearchTerms);
            this.Controls.Add(this.txt_SearchTerm);
            this.Controls.Add(this.lst_Contacts);
            this.Name = "frmAddressBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Address Book";
            this.Load += new System.EventHandler(this.frmAddressBook_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ContactInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lbl_ContactInfo;
        private System.Windows.Forms.Label lbl_Contacts;
        private System.Windows.Forms.Label lbl_SearchBy;
        private System.Windows.Forms.Label Search;
        private System.Windows.Forms.ComboBox cmb_SearchTerms;
        private System.Windows.Forms.TextBox txt_SearchTerm;
        private System.Windows.Forms.ListBox lst_Contacts;
        private System.Windows.Forms.Button btn_CreateContact;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_DeleteContact;
        private System.Windows.Forms.Button btn_EditContact;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.DataGridView dgv_ContactInfo;
        private System.Windows.Forms.Label lbl_DisplayBy;
        private System.Windows.Forms.ComboBox cmb_DisplayBy;
    }
}