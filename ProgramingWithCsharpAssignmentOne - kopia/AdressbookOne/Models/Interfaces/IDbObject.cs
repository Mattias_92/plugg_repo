﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdressbookOne.Models.Interfaces
{
    public interface IDbObject : ICloneable
    {
        int Id { get; set; }
    }
}
