﻿using AdressbookOne.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdressbookOne.Models
{
    public class Adress : IDbObject
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName  => $"{FirstName} {LastName}";
        public string StreetAdress { get; set; }
        public string PostArea { get; set; }
        public string Email { get; set; }
        public int ZipCode { get; set; }
        public string PhoneNumber { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
