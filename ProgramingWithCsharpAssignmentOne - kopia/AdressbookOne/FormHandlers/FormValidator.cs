﻿using AdressbookOne.Models;
using AdressbookOne.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdressbookOne.FormHandlers
{
    public static class FormValidator
    {
        internal static bool IsNameInputedCorrectly(out int errorIndex, string txt_FirstName = "placeHolder", string txt_LastName = "placeHolder")
        {
            errorIndex = 1;
            bool isValid = true;
            if (string.IsNullOrWhiteSpace(txt_FirstName))
            {
                errorIndex = 0;
                isValid = false;
            }
            if (string.IsNullOrWhiteSpace(txt_LastName))
            {
                isValid = false;
                if (errorIndex == 0)
                {
                    errorIndex = -2;
                }
                else
                {
                    errorIndex = -1;
                }
            }
            return isValid;
        }

        internal static bool IsEmailCorrect(string text)
        {
            int lastIndexOfDot = text.LastIndexOf(".");
            if (text.Contains("@"))
            {
                if (lastIndexOfDot == text.Length - 4 ||
                    lastIndexOfDot == text.Length - 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        internal static bool IsScoreInputedCorrect(string text, out int result)
        {
            if (!int.TryParse(text, out result))
            {
                return false;
            }
            return true;
        }

        internal static bool EmailAlreadyInUse(string email, List<IDbObject> fileContent)
        {           
            foreach (Adress item in fileContent)
            {
                if (item.Email == email)
                {
                    return true;
                }
            }
            return false;
        }

        internal static bool IsPhoneCorrect(string text)
        {
            if (!int.TryParse(text, out int number))
            {
                return false;
            }
            if (text[0] != '0')
            {
                return false;
            }
            if (text.Length > 10 || text.Length < 8)
            {
                return false;
            }
            return true;
        }

        internal static bool IsZipeCodeCorrect(string txt_ZipCode)
        {
            if (string.IsNullOrEmpty(txt_ZipCode) || txt_ZipCode.Length != 5 || !int.TryParse(txt_ZipCode, out int result))
            {
                return false;
            }
            return true;
        }
    }
}
