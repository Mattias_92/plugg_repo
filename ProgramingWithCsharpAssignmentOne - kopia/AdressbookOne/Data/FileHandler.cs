﻿using AdressbookOne.Models;
using AdressbookOne.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdressbookOne.Data
{
    public class FileHandler<T> : IFileHandler<T> where T : IDbObject
    {
        private string FilePath;
        private List<T> FileContent;
        private int _currentHighestId = 0;
        public FileHandler(string fileName)
        {
            FilePath = Constants.Constants.FileRoot + fileName;
            FileContent = ReadFile(FilePath);
            _currentHighestId = GetHighestId(_currentHighestId);
        }
        private List<T> ReadFile(string filePath)
        {
            try
            {
                using (StreamReader file = File.OpenText(filePath))
                {
                    JsonSerializer seroalizer = new JsonSerializer();
                    var ts = (List<T>)seroalizer.Deserialize(file, typeof(List<T>));
                    if (ts != null)
                    {
                        return ts;
                    }
                    else
                    {
                        return new List<T>();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Something went wrong with reading the file.");
                Console.WriteLine("Initialize an empty list.");
                Console.WriteLine(ex.Message);
                return new List<T>();
            }
            
        }
        public T Get(int id)
        {
            return FileContent.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            List<T> test = FileContent.ConvertAll(x => (T)x.Clone());
            return test;
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            var retObject = FileContent.Where(predicate);
            return retObject;
        }     
        public void Add(T entity)
        {
            AssignId(entity);
            FileContent.Add(entity);
        }
        private void AssignId(T entity)
        {
            try
            {
                int newId = ++_currentHighestId;
                entity.Id = newId;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Someting went wrong when assining Id to entity. " + ex.Message);
            }
        }

        private int GetHighestId(int currentHighestId)
        {
            if (FileContent.Count != 0)
            {
                currentHighestId = FileContent.Where(x => x.Id != 0)
                .Max(x => x.Id);
            }

            return currentHighestId;
        }

        public void AddRange(IEnumerable<T> entities)
        {
            List<T> IdLessEntities = entities.Where(x => x.Id == 0).ToList();
            List<T> duplicates = entities.Where(x => x.Id == FileContent.FirstOrDefault(y => y.Id == x.Id).Id).ToList();
            if (IdLessEntities.Count > 0)
            {
                AssingIdToRangeOfEntities(IdLessEntities);
            }          
            if (duplicates.Count > 0)
            {
                ReplaceDuplicates(duplicates);
            }
            FileContent.AddRange(IdLessEntities);
            FileContent.AddRange(duplicates);
        }

        private void ReplaceDuplicates(List<T> duplicates)
        {
            foreach (var item in duplicates)
            {
                var replaceObject = Get(item.Id);
                replaceObject = item;
            }
        }

        private void AssingIdToRangeOfEntities(List<T> IdLessEntities)
        {
                foreach (var item in IdLessEntities)
                {
                    AssignId(item);
                }
        }


        public void Remove(T entity)
        {
            var deleteItem = Get(entity.Id);
            FileContent.Remove(deleteItem);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                Remove(item);
            }
        }

        public void Save()
        {
            string message = string.Empty;
            try
            {
                using (StreamWriter file = File.CreateText(FilePath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, FileContent);
                }
                message = "Save successfull";
            }
            catch (Exception ex)
            {
                message = "Something went wrong with saving the file";
                Console.WriteLine(message);
                Console.WriteLine(ex.Message);
            }          
        }

        
    }
}
