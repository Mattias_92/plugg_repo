﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdressbookOne.Constants
{
    public class Constants
    {
        public const string FileRoot = @"..\..\Files\";

        public const string PhoneErrorMsg = "Enter a valid phone number.";
        public const string NameErrorMsg = "Enter a name.";
        public const string EmailErrorMsg = "Enter a valid email.";
        public const string StreetAddErrorMsg = "Enter a streetaddress.";
        public const string ZipCodeErrorMsg = "Enter a valid Zip Code";
        public const string PostAreaErrorMsg = "Enter a postal area";
    }
}
