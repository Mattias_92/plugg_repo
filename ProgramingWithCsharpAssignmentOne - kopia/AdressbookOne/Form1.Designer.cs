﻿
namespace AdressbookOne
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_Contacts = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.lbl_FName = new System.Windows.Forms.Label();
            this.lbl_StreetAdd = new System.Windows.Forms.Label();
            this.lbl_LstName = new System.Windows.Forms.Label();
            this.lbl_PostA = new System.Windows.Forms.Label();
            this.lbl_Email = new System.Windows.Forms.Label();
            this.lbl_ZipCode = new System.Windows.Forms.Label();
            this.lbl_PhoneN = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.lbl_ErrorFirstName = new System.Windows.Forms.Label();
            this.lbl_ErrorLastName = new System.Windows.Forms.Label();
            this.lbl_ErrorStreetAdd = new System.Windows.Forms.Label();
            this.lbl_ErrorPostArea = new System.Windows.Forms.Label();
            this.lbl_ErrorEmail = new System.Windows.Forms.Label();
            this.lbl_ErrorZipCode = new System.Windows.Forms.Label();
            this.lbl_ErrorPhoneN = new System.Windows.Forms.Label();
            this.Search = new System.Windows.Forms.Label();
            this.lbl_SearchBy = new System.Windows.Forms.Label();
            this.lbl_Contacts = new System.Windows.Forms.Label();
            this.lbl_ContactInfo = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lst_Contacts
            // 
            this.lst_Contacts.FormattingEnabled = true;
            this.lst_Contacts.Location = new System.Drawing.Point(48, 198);
            this.lst_Contacts.Name = "lst_Contacts";
            this.lst_Contacts.Size = new System.Drawing.Size(301, 355);
            this.lst_Contacts.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(376, 198);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(364, 107);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(48, 575);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(301, 20);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "Search";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(106, 603);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(171, 15);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 4;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(171, 107);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 5;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(171, 59);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(376, 15);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 7;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(376, 59);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 8;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(376, 107);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 9;
            // 
            // lbl_FName
            // 
            this.lbl_FName.AutoSize = true;
            this.lbl_FName.Location = new System.Drawing.Point(79, 22);
            this.lbl_FName.Name = "lbl_FName";
            this.lbl_FName.Size = new System.Drawing.Size(57, 13);
            this.lbl_FName.TabIndex = 10;
            this.lbl_FName.Text = "First Name";
            // 
            // lbl_StreetAdd
            // 
            this.lbl_StreetAdd.AutoSize = true;
            this.lbl_StreetAdd.Location = new System.Drawing.Point(79, 114);
            this.lbl_StreetAdd.Name = "lbl_StreetAdd";
            this.lbl_StreetAdd.Size = new System.Drawing.Size(70, 13);
            this.lbl_StreetAdd.TabIndex = 11;
            this.lbl_StreetAdd.Text = "Street Adress";
            // 
            // lbl_LstName
            // 
            this.lbl_LstName.AutoSize = true;
            this.lbl_LstName.Location = new System.Drawing.Point(79, 66);
            this.lbl_LstName.Name = "lbl_LstName";
            this.lbl_LstName.Size = new System.Drawing.Size(58, 13);
            this.lbl_LstName.TabIndex = 12;
            this.lbl_LstName.Text = "Last Name";
            // 
            // lbl_PostA
            // 
            this.lbl_PostA.AutoSize = true;
            this.lbl_PostA.Location = new System.Drawing.Point(322, 22);
            this.lbl_PostA.Name = "lbl_PostA";
            this.lbl_PostA.Size = new System.Drawing.Size(53, 13);
            this.lbl_PostA.TabIndex = 13;
            this.lbl_PostA.Text = "Post Area";
            // 
            // lbl_Email
            // 
            this.lbl_Email.AutoSize = true;
            this.lbl_Email.Location = new System.Drawing.Point(322, 66);
            this.lbl_Email.Name = "lbl_Email";
            this.lbl_Email.Size = new System.Drawing.Size(32, 13);
            this.lbl_Email.TabIndex = 14;
            this.lbl_Email.Text = "Email";
            // 
            // lbl_ZipCode
            // 
            this.lbl_ZipCode.AutoSize = true;
            this.lbl_ZipCode.Location = new System.Drawing.Point(322, 114);
            this.lbl_ZipCode.Name = "lbl_ZipCode";
            this.lbl_ZipCode.Size = new System.Drawing.Size(50, 13);
            this.lbl_ZipCode.TabIndex = 15;
            this.lbl_ZipCode.Text = "Zip Code";
            // 
            // lbl_PhoneN
            // 
            this.lbl_PhoneN.AutoSize = true;
            this.lbl_PhoneN.Location = new System.Drawing.Point(532, 19);
            this.lbl_PhoneN.Name = "lbl_PhoneN";
            this.lbl_PhoneN.Size = new System.Drawing.Size(78, 13);
            this.lbl_PhoneN.TabIndex = 17;
            this.lbl_PhoneN.Text = "Phone Number";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(611, 15);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 16;
            // 
            // lbl_ErrorFirstName
            // 
            this.lbl_ErrorFirstName.AutoSize = true;
            this.lbl_ErrorFirstName.Location = new System.Drawing.Point(168, 38);
            this.lbl_ErrorFirstName.Name = "lbl_ErrorFirstName";
            this.lbl_ErrorFirstName.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorFirstName.TabIndex = 18;
            this.lbl_ErrorFirstName.Text = "Error";
            this.lbl_ErrorFirstName.Visible = false;
            // 
            // lbl_ErrorLastName
            // 
            this.lbl_ErrorLastName.AutoSize = true;
            this.lbl_ErrorLastName.Location = new System.Drawing.Point(168, 82);
            this.lbl_ErrorLastName.Name = "lbl_ErrorLastName";
            this.lbl_ErrorLastName.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorLastName.TabIndex = 19;
            this.lbl_ErrorLastName.Text = "Error";
            this.lbl_ErrorLastName.Visible = false;
            // 
            // lbl_ErrorStreetAdd
            // 
            this.lbl_ErrorStreetAdd.AutoSize = true;
            this.lbl_ErrorStreetAdd.Location = new System.Drawing.Point(168, 130);
            this.lbl_ErrorStreetAdd.Name = "lbl_ErrorStreetAdd";
            this.lbl_ErrorStreetAdd.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorStreetAdd.TabIndex = 20;
            this.lbl_ErrorStreetAdd.Text = "Error";
            this.lbl_ErrorStreetAdd.Visible = false;
            // 
            // lbl_ErrorPostArea
            // 
            this.lbl_ErrorPostArea.AutoSize = true;
            this.lbl_ErrorPostArea.Location = new System.Drawing.Point(373, 38);
            this.lbl_ErrorPostArea.Name = "lbl_ErrorPostArea";
            this.lbl_ErrorPostArea.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorPostArea.TabIndex = 21;
            this.lbl_ErrorPostArea.Text = "Error";
            this.lbl_ErrorPostArea.Visible = false;
            // 
            // lbl_ErrorEmail
            // 
            this.lbl_ErrorEmail.AutoSize = true;
            this.lbl_ErrorEmail.Location = new System.Drawing.Point(373, 82);
            this.lbl_ErrorEmail.Name = "lbl_ErrorEmail";
            this.lbl_ErrorEmail.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorEmail.TabIndex = 22;
            this.lbl_ErrorEmail.Text = "Error";
            this.lbl_ErrorEmail.Visible = false;
            // 
            // lbl_ErrorZipCode
            // 
            this.lbl_ErrorZipCode.AutoSize = true;
            this.lbl_ErrorZipCode.Location = new System.Drawing.Point(373, 130);
            this.lbl_ErrorZipCode.Name = "lbl_ErrorZipCode";
            this.lbl_ErrorZipCode.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorZipCode.TabIndex = 23;
            this.lbl_ErrorZipCode.Text = "Error";
            this.lbl_ErrorZipCode.Visible = false;
            // 
            // lbl_ErrorPhoneN
            // 
            this.lbl_ErrorPhoneN.AutoSize = true;
            this.lbl_ErrorPhoneN.Location = new System.Drawing.Point(608, 38);
            this.lbl_ErrorPhoneN.Name = "lbl_ErrorPhoneN";
            this.lbl_ErrorPhoneN.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorPhoneN.TabIndex = 24;
            this.lbl_ErrorPhoneN.Text = "Error";
            this.lbl_ErrorPhoneN.Visible = false;
            // 
            // Search
            // 
            this.Search.AutoSize = true;
            this.Search.Location = new System.Drawing.Point(45, 559);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(41, 13);
            this.Search.TabIndex = 25;
            this.Search.Text = "Search";
            // 
            // lbl_SearchBy
            // 
            this.lbl_SearchBy.AutoSize = true;
            this.lbl_SearchBy.Location = new System.Drawing.Point(45, 609);
            this.lbl_SearchBy.Name = "lbl_SearchBy";
            this.lbl_SearchBy.Size = new System.Drawing.Size(55, 13);
            this.lbl_SearchBy.TabIndex = 26;
            this.lbl_SearchBy.Text = "Search by";
            // 
            // lbl_Contacts
            // 
            this.lbl_Contacts.AutoSize = true;
            this.lbl_Contacts.Location = new System.Drawing.Point(45, 182);
            this.lbl_Contacts.Name = "lbl_Contacts";
            this.lbl_Contacts.Size = new System.Drawing.Size(49, 13);
            this.lbl_Contacts.TabIndex = 27;
            this.lbl_Contacts.Text = "Contacts";
            // 
            // lbl_ContactInfo
            // 
            this.lbl_ContactInfo.AutoSize = true;
            this.lbl_ContactInfo.Location = new System.Drawing.Point(373, 182);
            this.lbl_ContactInfo.Name = "lbl_ContactInfo";
            this.lbl_ContactInfo.Size = new System.Drawing.Size(65, 13);
            this.lbl_ContactInfo.TabIndex = 28;
            this.lbl_ContactInfo.Text = "Contact Info";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(255, 603);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(94, 23);
            this.btn_Search.TabIndex = 29;
            this.btn_Search.Text = "button1";
            this.btn_Search.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(535, 105);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(176, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "Create Contact";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(611, 609);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 640);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.lbl_ContactInfo);
            this.Controls.Add(this.lbl_Contacts);
            this.Controls.Add(this.lbl_SearchBy);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.lbl_ErrorPhoneN);
            this.Controls.Add(this.lbl_ErrorZipCode);
            this.Controls.Add(this.lbl_ErrorEmail);
            this.Controls.Add(this.lbl_ErrorPostArea);
            this.Controls.Add(this.lbl_ErrorStreetAdd);
            this.Controls.Add(this.lbl_ErrorLastName);
            this.Controls.Add(this.lbl_ErrorFirstName);
            this.Controls.Add(this.lbl_PhoneN);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.lbl_ZipCode);
            this.Controls.Add(this.lbl_Email);
            this.Controls.Add(this.lbl_PostA);
            this.Controls.Add(this.lbl_LstName);
            this.Controls.Add(this.lbl_StreetAdd);
            this.Controls.Add(this.lbl_FName);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lst_Contacts);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lst_Contacts;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label lbl_FName;
        private System.Windows.Forms.Label lbl_StreetAdd;
        private System.Windows.Forms.Label lbl_LstName;
        private System.Windows.Forms.Label lbl_PostA;
        private System.Windows.Forms.Label lbl_Email;
        private System.Windows.Forms.Label lbl_ZipCode;
        private System.Windows.Forms.Label lbl_PhoneN;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label lbl_ErrorFirstName;
        private System.Windows.Forms.Label lbl_ErrorLastName;
        private System.Windows.Forms.Label lbl_ErrorStreetAdd;
        private System.Windows.Forms.Label lbl_ErrorPostArea;
        private System.Windows.Forms.Label lbl_ErrorEmail;
        private System.Windows.Forms.Label lbl_ErrorZipCode;
        private System.Windows.Forms.Label lbl_ErrorPhoneN;
        private System.Windows.Forms.Label Search;
        private System.Windows.Forms.Label lbl_SearchBy;
        private System.Windows.Forms.Label lbl_Contacts;
        private System.Windows.Forms.Label lbl_ContactInfo;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

