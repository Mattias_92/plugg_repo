﻿
namespace AdressbookOne
{
    partial class frmCreateAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_CreateContact = new System.Windows.Forms.Button();
            this.lbl_ErrorPhoneN = new System.Windows.Forms.Label();
            this.lbl_ErrorZipCode = new System.Windows.Forms.Label();
            this.lbl_ErrorEmail = new System.Windows.Forms.Label();
            this.lbl_ErrorPostArea = new System.Windows.Forms.Label();
            this.lbl_ErrorStreetAdd = new System.Windows.Forms.Label();
            this.lbl_ErrorLastName = new System.Windows.Forms.Label();
            this.lbl_ErrorFirstName = new System.Windows.Forms.Label();
            this.lbl_PhoneN = new System.Windows.Forms.Label();
            this.txt_PhoneNumber = new System.Windows.Forms.TextBox();
            this.lbl_ZipCode = new System.Windows.Forms.Label();
            this.lbl_Email = new System.Windows.Forms.Label();
            this.lbl_PostA = new System.Windows.Forms.Label();
            this.lbl_LstName = new System.Windows.Forms.Label();
            this.lbl_StreetAdd = new System.Windows.Forms.Label();
            this.lbl_FName = new System.Windows.Forms.Label();
            this.txt_ZipCode = new System.Windows.Forms.TextBox();
            this.txt_Email = new System.Windows.Forms.TextBox();
            this.txt_PostArea = new System.Windows.Forms.TextBox();
            this.txt_LastName = new System.Windows.Forms.TextBox();
            this.txt_StreetAddress = new System.Windows.Forms.TextBox();
            this.txt_FirstName = new System.Windows.Forms.TextBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.lbl_TitleHeader = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_CreateContact
            // 
            this.btn_CreateContact.Location = new System.Drawing.Point(261, 246);
            this.btn_CreateContact.Name = "btn_CreateContact";
            this.btn_CreateContact.Size = new System.Drawing.Size(160, 23);
            this.btn_CreateContact.TabIndex = 52;
            this.btn_CreateContact.Text = "Create Contact";
            this.btn_CreateContact.UseVisualStyleBackColor = true;
            this.btn_CreateContact.Click += new System.EventHandler(this.btn_CreateContact_Click);
            // 
            // lbl_ErrorPhoneN
            // 
            this.lbl_ErrorPhoneN.AutoSize = true;
            this.lbl_ErrorPhoneN.Location = new System.Drawing.Point(318, 173);
            this.lbl_ErrorPhoneN.Name = "lbl_ErrorPhoneN";
            this.lbl_ErrorPhoneN.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorPhoneN.TabIndex = 51;
            this.lbl_ErrorPhoneN.Text = "Error";
            this.lbl_ErrorPhoneN.Visible = false;
            // 
            // lbl_ErrorZipCode
            // 
            this.lbl_ErrorZipCode.AutoSize = true;
            this.lbl_ErrorZipCode.Location = new System.Drawing.Point(318, 217);
            this.lbl_ErrorZipCode.Name = "lbl_ErrorZipCode";
            this.lbl_ErrorZipCode.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorZipCode.TabIndex = 50;
            this.lbl_ErrorZipCode.Text = "Error";
            this.lbl_ErrorZipCode.Visible = false;
            // 
            // lbl_ErrorEmail
            // 
            this.lbl_ErrorEmail.AutoSize = true;
            this.lbl_ErrorEmail.Location = new System.Drawing.Point(113, 256);
            this.lbl_ErrorEmail.Name = "lbl_ErrorEmail";
            this.lbl_ErrorEmail.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorEmail.TabIndex = 49;
            this.lbl_ErrorEmail.Text = "Error";
            this.lbl_ErrorEmail.Visible = false;
            // 
            // lbl_ErrorPostArea
            // 
            this.lbl_ErrorPostArea.AutoSize = true;
            this.lbl_ErrorPostArea.Location = new System.Drawing.Point(318, 125);
            this.lbl_ErrorPostArea.Name = "lbl_ErrorPostArea";
            this.lbl_ErrorPostArea.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorPostArea.TabIndex = 48;
            this.lbl_ErrorPostArea.Text = "Error";
            this.lbl_ErrorPostArea.Visible = false;
            // 
            // lbl_ErrorStreetAdd
            // 
            this.lbl_ErrorStreetAdd.AutoSize = true;
            this.lbl_ErrorStreetAdd.Location = new System.Drawing.Point(113, 217);
            this.lbl_ErrorStreetAdd.Name = "lbl_ErrorStreetAdd";
            this.lbl_ErrorStreetAdd.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorStreetAdd.TabIndex = 47;
            this.lbl_ErrorStreetAdd.Text = "Error";
            this.lbl_ErrorStreetAdd.Visible = false;
            // 
            // lbl_ErrorLastName
            // 
            this.lbl_ErrorLastName.AutoSize = true;
            this.lbl_ErrorLastName.Location = new System.Drawing.Point(113, 169);
            this.lbl_ErrorLastName.Name = "lbl_ErrorLastName";
            this.lbl_ErrorLastName.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorLastName.TabIndex = 46;
            this.lbl_ErrorLastName.Text = "Error";
            this.lbl_ErrorLastName.Visible = false;
            // 
            // lbl_ErrorFirstName
            // 
            this.lbl_ErrorFirstName.AutoSize = true;
            this.lbl_ErrorFirstName.Location = new System.Drawing.Point(113, 125);
            this.lbl_ErrorFirstName.Name = "lbl_ErrorFirstName";
            this.lbl_ErrorFirstName.Size = new System.Drawing.Size(29, 13);
            this.lbl_ErrorFirstName.TabIndex = 45;
            this.lbl_ErrorFirstName.Text = "Error";
            this.lbl_ErrorFirstName.Visible = false;
            // 
            // lbl_PhoneN
            // 
            this.lbl_PhoneN.AutoSize = true;
            this.lbl_PhoneN.Location = new System.Drawing.Point(229, 153);
            this.lbl_PhoneN.Name = "lbl_PhoneN";
            this.lbl_PhoneN.Size = new System.Drawing.Size(78, 13);
            this.lbl_PhoneN.TabIndex = 44;
            this.lbl_PhoneN.Text = "Phone Number";
            // 
            // txt_PhoneNumber
            // 
            this.txt_PhoneNumber.Location = new System.Drawing.Point(321, 150);
            this.txt_PhoneNumber.Name = "txt_PhoneNumber";
            this.txt_PhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.txt_PhoneNumber.TabIndex = 43;
            // 
            // lbl_ZipCode
            // 
            this.lbl_ZipCode.AutoSize = true;
            this.lbl_ZipCode.Location = new System.Drawing.Point(267, 201);
            this.lbl_ZipCode.Name = "lbl_ZipCode";
            this.lbl_ZipCode.Size = new System.Drawing.Size(50, 13);
            this.lbl_ZipCode.TabIndex = 42;
            this.lbl_ZipCode.Text = "Zip Code";
            // 
            // lbl_Email
            // 
            this.lbl_Email.AutoSize = true;
            this.lbl_Email.Location = new System.Drawing.Point(24, 240);
            this.lbl_Email.Name = "lbl_Email";
            this.lbl_Email.Size = new System.Drawing.Size(32, 13);
            this.lbl_Email.TabIndex = 41;
            this.lbl_Email.Text = "Email";
            // 
            // lbl_PostA
            // 
            this.lbl_PostA.AutoSize = true;
            this.lbl_PostA.Location = new System.Drawing.Point(267, 109);
            this.lbl_PostA.Name = "lbl_PostA";
            this.lbl_PostA.Size = new System.Drawing.Size(53, 13);
            this.lbl_PostA.TabIndex = 40;
            this.lbl_PostA.Text = "Post Area";
            // 
            // lbl_LstName
            // 
            this.lbl_LstName.AutoSize = true;
            this.lbl_LstName.Location = new System.Drawing.Point(24, 153);
            this.lbl_LstName.Name = "lbl_LstName";
            this.lbl_LstName.Size = new System.Drawing.Size(58, 13);
            this.lbl_LstName.TabIndex = 39;
            this.lbl_LstName.Text = "Last Name";
            // 
            // lbl_StreetAdd
            // 
            this.lbl_StreetAdd.AutoSize = true;
            this.lbl_StreetAdd.Location = new System.Drawing.Point(24, 201);
            this.lbl_StreetAdd.Name = "lbl_StreetAdd";
            this.lbl_StreetAdd.Size = new System.Drawing.Size(70, 13);
            this.lbl_StreetAdd.TabIndex = 38;
            this.lbl_StreetAdd.Text = "Street Adress";
            // 
            // lbl_FName
            // 
            this.lbl_FName.AutoSize = true;
            this.lbl_FName.Location = new System.Drawing.Point(24, 109);
            this.lbl_FName.Name = "lbl_FName";
            this.lbl_FName.Size = new System.Drawing.Size(57, 13);
            this.lbl_FName.TabIndex = 37;
            this.lbl_FName.Text = "First Name";
            // 
            // txt_ZipCode
            // 
            this.txt_ZipCode.Location = new System.Drawing.Point(321, 194);
            this.txt_ZipCode.Name = "txt_ZipCode";
            this.txt_ZipCode.Size = new System.Drawing.Size(100, 20);
            this.txt_ZipCode.TabIndex = 36;
            // 
            // txt_Email
            // 
            this.txt_Email.Location = new System.Drawing.Point(116, 233);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(100, 20);
            this.txt_Email.TabIndex = 35;
            // 
            // txt_PostArea
            // 
            this.txt_PostArea.Location = new System.Drawing.Point(321, 102);
            this.txt_PostArea.Name = "txt_PostArea";
            this.txt_PostArea.Size = new System.Drawing.Size(100, 20);
            this.txt_PostArea.TabIndex = 34;
            // 
            // txt_LastName
            // 
            this.txt_LastName.Location = new System.Drawing.Point(116, 146);
            this.txt_LastName.Name = "txt_LastName";
            this.txt_LastName.Size = new System.Drawing.Size(100, 20);
            this.txt_LastName.TabIndex = 33;
            // 
            // txt_StreetAddress
            // 
            this.txt_StreetAddress.Location = new System.Drawing.Point(116, 194);
            this.txt_StreetAddress.Name = "txt_StreetAddress";
            this.txt_StreetAddress.Size = new System.Drawing.Size(100, 20);
            this.txt_StreetAddress.TabIndex = 32;
            // 
            // txt_FirstName
            // 
            this.txt_FirstName.Location = new System.Drawing.Point(116, 102);
            this.txt_FirstName.Name = "txt_FirstName";
            this.txt_FirstName.Size = new System.Drawing.Size(100, 20);
            this.txt_FirstName.TabIndex = 31;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(261, 275);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(160, 23);
            this.btn_Cancel.TabIndex = 53;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // lbl_TitleHeader
            // 
            this.lbl_TitleHeader.AutoSize = true;
            this.lbl_TitleHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TitleHeader.Location = new System.Drawing.Point(128, 9);
            this.lbl_TitleHeader.Name = "lbl_TitleHeader";
            this.lbl_TitleHeader.Size = new System.Drawing.Size(243, 37);
            this.lbl_TitleHeader.TabIndex = 54;
            this.lbl_TitleHeader.Text = "Add an address";
            this.lbl_TitleHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmCreateAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 342);
            this.Controls.Add(this.lbl_TitleHeader);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_CreateContact);
            this.Controls.Add(this.lbl_ErrorPhoneN);
            this.Controls.Add(this.lbl_ErrorZipCode);
            this.Controls.Add(this.lbl_ErrorEmail);
            this.Controls.Add(this.lbl_ErrorPostArea);
            this.Controls.Add(this.lbl_ErrorStreetAdd);
            this.Controls.Add(this.lbl_ErrorLastName);
            this.Controls.Add(this.lbl_ErrorFirstName);
            this.Controls.Add(this.lbl_PhoneN);
            this.Controls.Add(this.txt_PhoneNumber);
            this.Controls.Add(this.lbl_ZipCode);
            this.Controls.Add(this.lbl_Email);
            this.Controls.Add(this.lbl_PostA);
            this.Controls.Add(this.lbl_LstName);
            this.Controls.Add(this.lbl_StreetAdd);
            this.Controls.Add(this.lbl_FName);
            this.Controls.Add(this.txt_ZipCode);
            this.Controls.Add(this.txt_Email);
            this.Controls.Add(this.txt_PostArea);
            this.Controls.Add(this.txt_LastName);
            this.Controls.Add(this.txt_StreetAddress);
            this.Controls.Add(this.txt_FirstName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreateAddress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreateAddress";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCreateAddress_FormClosing);
            this.Load += new System.EventHandler(this.frmCreateAddress_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_CreateContact;
        private System.Windows.Forms.Label lbl_ErrorPhoneN;
        private System.Windows.Forms.Label lbl_ErrorZipCode;
        private System.Windows.Forms.Label lbl_ErrorEmail;
        private System.Windows.Forms.Label lbl_ErrorPostArea;
        private System.Windows.Forms.Label lbl_ErrorStreetAdd;
        private System.Windows.Forms.Label lbl_ErrorLastName;
        private System.Windows.Forms.Label lbl_ErrorFirstName;
        private System.Windows.Forms.Label lbl_PhoneN;
        private System.Windows.Forms.TextBox txt_PhoneNumber;
        private System.Windows.Forms.Label lbl_ZipCode;
        private System.Windows.Forms.Label lbl_Email;
        private System.Windows.Forms.Label lbl_PostA;
        private System.Windows.Forms.Label lbl_LstName;
        private System.Windows.Forms.Label lbl_StreetAdd;
        private System.Windows.Forms.Label lbl_FName;
        private System.Windows.Forms.TextBox txt_ZipCode;
        private System.Windows.Forms.TextBox txt_Email;
        private System.Windows.Forms.TextBox txt_PostArea;
        private System.Windows.Forms.TextBox txt_LastName;
        private System.Windows.Forms.TextBox txt_StreetAddress;
        private System.Windows.Forms.TextBox txt_FirstName;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lbl_TitleHeader;
    }
}